#******************************************************************************
# E.S.O. - VLT project
#
# "@(#) $Id: ESO-VLT-DIC.XSHOO_OS,v 1.1 2010-03-10 13:49:42 amodigli Exp $"
#
# XSHOO_OS dictionary
#
# who       when      what
# --------  --------  ----------------------------------------------
# alongino  01/09/00  created (copied from shodic)
# psantin   05/02/05  X-shooter kw added
# psantin   22/05/08  cleaned
#

#******************************************************************************
#
# This dictionary contains all keywords used by XSHOO OS at runtime
# to execute and understand commands with -function option, such
# as SETUP and STATUS.
#
# IMPORTANT NOTE: The "Value Format" entry defines the precision of
#                 the keyword value, following the ANSI-C printf
#                 rules. Specially for float or double numbers,
#                 roundings are possible and affects all commands,
#                 in particular SETUP and STATUS.
#                 In order to avoid undesired roundings, the
#                 appropriate precision must be defined in this field.
#
#******************************************************************************

#
# Notes
# 1) The letters within () indicate the Class that keyword belongs to:
#    c = config
#    h = header
#    l = conf-log
#    p = private
#    s = setup
#    o = ops-log
#

Dictionary Name:   ESO-VLT-DIC.XSHOO_OS
Scope:             XSHOO
Source:            ESO VLT
Version Control:   @(#) $Id: ESO-VLT-DIC.XSHOO_OS,v 1.1 2010-03-10 13:49:42 amodigli Exp $
Revision:          $Revision: 1.1 $
Date:              2008-10-22
Status:            Development
Description:       Template Instrument OS keywords

# Max. comment length ---------------------|
#                                          V
# -----------------------------------------+------------------
# General


Parameter Name:    OCS SYNC
Class:             header|ops-log|setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    Synchronous exposures flag
Description:       Synchronous exposures flag

Parameter Name:    SEQ NEXPO UVB
Class:             ops-log|setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    No. of exposures for UVB arm
Description:       No. of exposures for UVB arm

Parameter Name:    SEQ NEXPO VIS
Class:             ops-log|setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    No. of exposures for VIS arm
Description:       No. of exposures for VIS arm

Parameter Name:    SEQ NEXPO NIR
Class:             ops-log|setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    No. of exposures for NIR arm
Description:       No. of exposures for NIR arm

Parameter Name:    SEQ CAL MODE
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %s
Unit:              
Comment Format:    keyword: Calibration mode (SLIT/IFU)
Description:       keyword: Calibration mode (SLIT/IFU)

Parameter Name:    SEQ AGSNAPSHOT
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    take a snapshot with A&G sub-system (T/F)
Description:       take a snapshot with A&G sub-system (T/F)

Parameter Name:    SEQ NOFFSET
Class:             ops-log|setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    No. of offsets
Description:       No. of offsets

Parameter Name:    SEQ OFFSET COORDS
Class:             setup
Context:           Instrument
Type:              string
Value Format:      %s
Unit:              
Comment Format:    Type of offset : X/RA - Y/DEC (DETECTOR/SKY)
Description:       Type of offset : X/RA - Y/DEC (DETECTOR/SKY)

Parameter Name:    SEQ RELOFF1
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    List of X/RA offsets
Description:       List of X/RA offsets

Parameter Name:    SEQ RELOFF2
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    List of Y/DEC offsets
Description:       List of Y/DEC offsets

Parameter Name:    SEQ RELOFF RA
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    List of RA offsets
Description:       List of RA offsets

Parameter Name:    SEQ RELOFF DEC
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    List of DEC offsets
Description:       List of DEC offsets

Parameter Name:    SEQ OBS TYPE
Class:             header|setup
Context:           Instrument
Type:              string
Value Format:      %s
Unit:              
Comment Format:    List of observation types (Object, Sky)
Description:       List of observation types (Object, Sky)

Parameter Name:    SEQ CUMOFF RA
Class:             header
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    Cumulative RA offset
Description:       Cumulative RA offset

Parameter Name:    SEQ CUMOFF DEC
Class:             header
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    Cumulative DEC offset
Description:       Cumulative DEC offset

Parameter Name:    SEQ FIXOFF RA
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    RA offset
Description:       RA offset

Parameter Name:    SEQ FIXOFF DEC
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    offset
Description:       offset

Parameter Name:    SEQ NCYCLES
Class:             header|setup
Context:           Instrument
Type:              integer
Value Format:      %d
Unit:              
Comment Format:    No. of cycles to observe (OS = 1 cycle)
Description:       No. of cycles to observe (OS = 1 cycle)

Parameter Name:    SEQ NOD THROW
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    Nodding throw length in arcsec
Description:       Nodding throw length in arcsec

Parameter Name:    SEQ JITTER WIDTH
Class:             header|setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    Width of the Jitter box (arcsec)
Description:       Width of the Jitter box (arcsec)

Parameter Name:    SEQ OFFSET ZERO
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    Flag: return to zero offset position
Description:       Flag: return to zero offset position

Parameter Name:    SEQ AFC WSIZE
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    window size for clip cross-correlation
Description:       window size for clip cross-correlation

Parameter Name:    SEQ AFC MAXD
Class:             setup
Context:           Instrument
Type:              double
Value Format:      %f
Unit:              
Comment Format:    max distance for clip cross-correlation
Description:       max distance for clip cross-correlation

Parameter Name:    SEQ AFC CORRECT
Class:             setup
Context:           Instrument
Type:              logical
Value Format:      %c
Unit:              
Comment Format:    AFC correction flag (T/F)
Description:       AFC correction flag (T/F)

Parameter Name:    SEQ AFC CORRTIME
Class:             header
Context:           Instrument
Type:              string
Value Format:      %s
Unit:              
Comment Format:    AFC correction time (HH:MM:SS)
Description:       AFC correction time (HH:MM:SS)

Parameter Name:    SEQ ARM
Class:             header
Context:           Instrument
Type:              string
Value Format:      %3s
Unit:              
Comment Format:    Instrument Arm
Description:       Instrument Arm

# --- oOo ---
