from reflex import RecipeParameter
class Parameters(RecipeParameter):

    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setDecodeBpParameters(self,paramList,rec_id,group):

      help1="""Integer representation of the bits to be considered bad when decoding the bad pixel mask pixel values.
                    Most frequent codes relevant for the user:
                    0: good pixel, 
                    8: pick-up noise, 
                    16: cosmic-ray removed, 
                    32: cosmic-ray unremoved, 
                    128: calibration file defect, 
                    256: hot pixel, 
                    512: dark pixel, 
                    4096: A/D converted saturation, 
                    32768: non linear pixel, 
                    1048576: extrapolated flux in NIR, 
                    4194304: Interpolated flux during extraction."""
      
      
      paramList.append(RecipeParameter(rec_id,displayName="decode-bp",group=group,description=help1))


    def setPreOverscanParameters(self,paramList,rec_id,group):

      help2="""pre-overscan correction.0: no correction1: mean overscan correction2: mean prescan correction3: (mean pre+mean overscan)/2 correction."""
      paramList.append(RecipeParameter(rec_id,displayName="pre-overscan-corr",group=group,description=help2))


    def setGeneralCalibParameters(self,paramList,rec_id):
      group="general"

      self.setPreOverscanParameters(paramList,rec_id,group)


    def setGeneralObjectParameters(self,paramList,rec_id):

      group="general"
      print "rec_id=",rec_id

      self.setDecodeBpParameters(paramList,rec_id,group)
      self.setPreOverscanParameters(paramList,rec_id,group)

      help5="""if TRUE recompute (wave and slit) maps from the dispersion solution. Use FALSE only if you are sure to have wave and slit maps better than what this recipe may compute.""" 


      help6="""if TRUE compute object position trace via Gaussian fit."""
      help7="""Method used for extraction (LOCALIZATION, NOD)."""
      help8="""if TRUE a 2D sky frame, a 2D rectified, a 2D merged sky are generated."""
      help9="""Lower Slit Limit (localize and extract."""
      help10="""Upper Slit Limit (localize and extract."""


      if "stare" in rec_id:
        print "stare"
        help5=help5+"If sky-subtract is set to TRUE this must be set to TRUE."
        paramList.append(RecipeParameter(rec_id,displayName="compute-map",group=group,description=help5))
        paramList.append(RecipeParameter(rec_id,displayName="trace-obj",group=group,description=help6))

      if "nod" in rec_id :
        print "extract method"
        paramList.append(RecipeParameter(rec_id,displayName="extract-method",group=group,description=help7))

      if "slit_offset" in rec_id :
        paramList.append(RecipeParameter(rec_id,displayName="gen-sky",group=group,description=help8))

      if "slit_nod" in rec_id :
        paramList.append(RecipeParameter(rec_id,displayName="max-slit",group=group,description=help9))
        paramList.append(RecipeParameter(rec_id,displayName="min-slit",group=group,description=help10))

        help14="""Combination method for nodded frames (MEDIAN, MEAN)."""
        paramList.append(RecipeParameter(rec_id,displayName="combinenod-method",group=group,description=help14))

        help15="""TRUE if the resampled spectrum at each wavelength is median subtracted to remove sky lines."""
        paramList.append(RecipeParameter(rec_id,displayName="correct-sky-by-median",group=group,description=help15))

      if "slit" in rec_id :

         help12="""Half size of mask used to define object cross orderprofile. [7]. [FALSE]."""
         paramList.append(RecipeParameter(rec_id,displayName="stdextract-interp-hsize",group=group,description=help12))

      if rec_id[0:10] == "xsh_respon":
         help13="""TRUE if during response computation we apply telluric correction. [FALSE]."""
         paramList.append(RecipeParameter(rec_id,displayName="correct-tellurics",group=group,description=help13))

      if rec_id[0:10] == "xsh_scired":
         help16="""TRUE if recipe cuts the UVB spectrum at 556 nm (dichroich). [TRUE]."""
         paramList.append(RecipeParameter(rec_id,displayName="cut-uvb-spectrum",group=group,description=help16))

      if "slit" in rec_id :
         help17="""TRUE if additional files should be generated in Science Data Product (SDP) format. [FALSE]."""
         paramList.append(RecipeParameter(rec_id,displayName="generate-SDP-format",group=group,description=help17))

      return paramList







    def setDetectArclinesParameters(self,paramList,rec_id):
      group="detectarclines"
      help1="""Half window size (HWS) in pixels for the line 2D fitting window (total window size = 2*HWS+1)."""
      paramList.append(RecipeParameter(rec_id,displayName="detectarclines-fit-win-hsize",group=group,description=help1))

      help2="""Half window size (HWS) in pixels for the line search box around the expected position (total window size = 2*HWS+1) [bin units]."""

      paramList.append(RecipeParameter(rec_id,displayName="detectarclines-search-win-hsize",group=group,description=help2))


      if rec_id[4:11] != "predict" :
         help7="""Degree in Y in the polynomial order tracing X=f(Y)."""
         paramList.append(RecipeParameter(rec_id,displayName="detectarclines-ordertab-deg-y",group=group,description=help7))


      help8="""Minimum signal-to-noise ratio to filter lines"""
      paramList.append(RecipeParameter(rec_id,displayName="detectarclines-min-sn",group=group,description=help8))


    def setDetectContinuumParameters(self,paramList,rec_id):
      group="detectcontinuum"

      help1="""Half window size for the running median box during the search for the maximum in the cross-dispersion profile."""
      paramList.append(RecipeParameter(rec_id,displayName="detectcontinuum-search-win-hsize",group=group,description=help1))

      help2="""Half window size for the fit of the cross-dispersion profile."""
      paramList.append(RecipeParameter(rec_id,displayName="detectcontinuum-running-win-hsize",group=group,description=help2))

      help3="""Threshold factor applied to check that the flux at the fitted peak is higher than error."""
      paramList.append(RecipeParameter(rec_id,displayName="detectcontinuum-fit-win-hsize",group=group,description=help3))

      help4="""Degree in Y in the polynomial order tracing X=f(Y)."""
      paramList.append(RecipeParameter(rec_id,displayName="detectcontinuum-ordertab-step-y",group=group,description=help4))

      help5="""Maximum allowed residual (before kappa-sigma clip)."""
      paramList.append(RecipeParameter(rec_id,displayName="detectcontinuum-ordertab-deg-y",group=group,description=help5))


    def setDetectOrderParameters(self,paramList,rec_id):
      group="detectorder"

      help1="""Half window size in pixels for the search for order edges."""
      paramList.append(RecipeParameter(rec_id,displayName="detectorder-edges-search-win-hsize",group=group,description=help1))

      help2="""Threshold in relative flux (compared to the central flux) below which the order edges are defined."""
      paramList.append(RecipeParameter(rec_id,displayName="detectorder-edges-flux-thresh",group=group,description=help2))

      help3="""Minimum signal-to-noise ratio at the centroid of the orders (35 for SLIT-UVB,VIS, 20 for IFU-UVB,VIS, 25 for SLIT-NIR, 4 for IFU-NIR."""
      paramList.append(RecipeParameter(rec_id,displayName="detectorder-min-sn",group=group,description=help3))

      help11="""minimum signal noise ratio in order."""
      paramList.append(RecipeParameter(rec_id,displayName="detectorder-d2-min-sn",group=group,description=help11))


    def setBackgroundParameters(self,paramList,rec_id):
      group="background"


      help5="""Poly mode fit deg along X."""
      help6="""Poly mode fit deg along Y."""
      help7="""Poly mode kappa value of kappa-sigma-clip outliers removal."""
      help8="""X margin to order edge to define background sampling points."""

      paramList.append(RecipeParameter(rec_id,displayName="background-poly-deg-x",group=group,description=help5))
      paramList.append(RecipeParameter(rec_id,displayName="background-poly-deg-y",group=group,description=help6))
      paramList.append(RecipeParameter(rec_id,displayName="background-poly-kappa",group=group,description=help7))
      paramList.append(RecipeParameter(rec_id,displayName="background-edges-margin",group=group,description=help8))



    def setModelParameters(self,paramList,rec_id):
      group="model"

      help1="""Number/10 of annealing iterations if in physical model mode."""
      paramList.append(RecipeParameter(rec_id,displayName="model-maxit",group=group,description=help1))

      help2="""Multiplier applied to the automatic parameter ranges (i.e. when scenario!=0). For routine operations should be 1.0. (physical model mode)."""
      paramList.append(RecipeParameter(rec_id,displayName="model-anneal-factor",group=group,description=help2))

      help3="""selects preset flag and range combinations appropriate to common scenarios: 0 - No scenario, input cfg flags and limits used.1 - scenario appropriate for the startup recipe (large ranges for parameters affecting single ph exposures, dist coeff fixed).  2 - Like 1, but includes parameters affecting all ph positions.  3 - Scenario for use in fine tuning cfg to match routine single pinhole exposures. All parameters affecting 1ph exposures except dist coeffs are included and parameter ranges are small. (For use by predict in 1ph case).  4 - Like 3 but includes parameters  affecting all ph positions (Standard for use by predict in 9ph case and 2dmap)."""


    def setStackParameters(self,paramList,rec_id):
      group="stack"
      help1="""Method used to build master frame. <median | mean>"""
      paramList.append(RecipeParameter(rec_id,displayName="stack-method",group=group,description=help1))

      help2="""Kappa used to clip low level values, when method is set to 'mean'."""
      paramList.append(RecipeParameter(rec_id,displayName="klow",group=group,description=help2))

      help3="""Kappa used to clip high level values, when method is set to 'mean'."""
      paramList.append(RecipeParameter(rec_id,displayName="khigh",group=group,description=help3))

      help4="""Number of kappa sigma iterations, when method is set to 'mean'."""


    def setCrhSingleParameters(self,paramList,rec_id):
      group="removecrhsingle"
      help1="""Max fraction of bad pixels allowed."""
      help2="""Poisson fluctuation threshold to flag CRHs (see van Dokkum, PASP,113,2001,p1420-27)."""
      help3="""Minimum contrast between the Laplacian image and the fine structure image that a point must have to be flagged as CRH. (see van Dokkum, PASP, 113, 2001, p1420-27)."""
      help4="""Max number of iterations."""
      paramList.append(RecipeParameter(rec_id,displayName="removecrhsingle-frac-max",group=group,description=help1))
      paramList.append(RecipeParameter(rec_id,displayName="removecrhsingle-sigmalim",group=group,description=help2))
      paramList.append(RecipeParameter(rec_id,displayName="removecrhsingle-flim",group=group,description=help3))
      paramList.append(RecipeParameter(rec_id,displayName="removecrhsingle-niter",group=group,description=help4))


    def setLocalizeParameters(self,paramList,rec_id):
      group="localize"
      help1="""Localization method (MANUAL, MAXIMUM, GAUSSIAN) used to detect the object centroid and height on the slit."""
      help2="""Number of chunks in the full spectrum to localize the object."""
      help3="""Threshold relative to the peak intensity below which the edges of the object are detected for MAXIMUM localization."""
      help4="""Degree in lambda in the localization polynomial expression slit=f(lambda), used only for MAXIMUM and GAUSSIAN."""
      help5="""Object position on the slit for MANUAL localization [arcsec]."""
      help6="""Object half height on the slit for MANUAL localization [arcsec]."""
      help7="""Kappa value for sigma clipping in the localization polynomial fit."""
      help8="""Number of iterations for sigma clipping in the localization polynomial fit."""
      help9="""TRUE if we want to mask sky lines using SKY_LINE_LIST file."""
      help10="""Step (arcsec) between A and B images in nodding mode."""

      paramList.append(RecipeParameter(rec_id,displayName="localize-method",group=group,description=help1))
      paramList.append(RecipeParameter(rec_id,displayName="localize-chunk-nb",group=group,description=help2))
      paramList.append(RecipeParameter(rec_id,displayName="localize-thresh",group=group,description=help3))
      paramList.append(RecipeParameter(rec_id,displayName="localize-deg-lambda",group=group,description=help4))
      paramList.append(RecipeParameter(rec_id,displayName="localize-slit-position",group=group,description=help5))
      paramList.append(RecipeParameter(rec_id,displayName="localize-slit-hheight",group=group,description=help6))
      paramList.append(RecipeParameter(rec_id,displayName="localize-kappa",group=group,description=help7))
      paramList.append(RecipeParameter(rec_id,displayName="localize-niter",group=group,description=help8))
      paramList.append(RecipeParameter(rec_id,displayName="localize-use-skymask",group=group,description=help9))
      if rec_id == "xsh_scired_slit_nod" or rec_id == "xsh_respon_slit_nod":
         paramList.append(RecipeParameter(rec_id,displayName="localize-nod-throw",group=group,description=help10))

    def setRectifyParameters(self,paramList,rec_id):
      group="rectify"
      help1="""Name of the Interpolation Kernel Used. Possible values are tanh, tanh, sinc, sinc2, lanczos, hamming, hann."""
      help2="""Rectify Interpolation radius [bin units]."""
      help3="""Wavelength step in the output spectrum [nm]."""
      help4="""Spatial step along the slit in the output spectrum [arcsec]."""

      paramList.append(RecipeParameter(rec_id,displayName="rectify-kernel",group=group,description=help1))
      paramList.append(RecipeParameter(rec_id,displayName="rectify-radius",group=group,description=help2))
      paramList.append(RecipeParameter(rec_id,displayName="rectify-bin-lambda",group=group,description=help3))
      paramList.append(RecipeParameter(rec_id,displayName="rectify-bin-slit",group=group,description=help4))


    def setOptExtractParameters(self,paramList,rec_id):
      group="optextract"
      help1="""TRUE if we do the optimal extraction."""
      help2="""Oversample factor for the science image."""
      help3="""Extraction box [pixel]."""
      help4="""Chunk size [bin]."""
      help5="""Lambda step [nm]."""
      help6="""Kappa for cosmics ray hits rejection."""
      help7="""Maximum bad pixels fraction for cosmics ray hits rejection."""
      help8="""Maximum number of iterations for cosmics ray hits rejection."""
      help9="""Number of iterations."""
      help10="""Extraction method GAUSSIAN | GENERAL."""


      paramList.append(RecipeParameter(rec_id,displayName="do-optextract",group=group,description=help1))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-oversample",group=group,description=help2))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-box-half-size",group=group,description=help3))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-chunk-size",group=group,description=help4))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-step-lambda",group=group,description=help5))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-clip-kappa",group=group,description=help6))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-clip-frac",group=group,description=help7))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-clip-niter",group=group,description=help8))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-niter",group=group,description=help9))
      paramList.append(RecipeParameter(rec_id,displayName="optextract-method",group=group,description=help10))


    def setSkyParameters(self,paramList,rec_id):
      group="sky"
      help1="""TRUE to use subtract sky single."""
      help2="""Sky subtract Method (BSPLINE, MEDIAN)."""
      help3="""Nb of break points for Bezier curve fitting (without localization)."""
      help4="""Nb of break points for Bezier curve fitting (with localization).""" 
      help5="""Bezier spline order."""
      help6="""Nb of iterations. [20]"""
      help7="""Size of edges mask in arcsec."""
      help8="""Half size of the running median. If sky-method=MEDIAN."""
      help9="""Size of edges mask in arcsec."""
      help10="""Central position of the sky window #1 [arcsec]."""
      help11="""Half size of sky window #1 [arcsec]."""
      help12="""Central position of the sky window #2 [arcsec]."""
      help13="""Half size of the sky window #2 [arcsec]."""
      help14="""BSPLINE sampling. UNIFORM-uses the user defined nbkpts value, corrected for binning, for all orders. FINE: multiplies the user defined nbkpts value, corrected for binning, by a hard coded coefficient optimized on each arm-order)."""
        
      paramList.append(RecipeParameter(rec_id,displayName="sky-subtract",group=group,description=help1))
      paramList.append(RecipeParameter(rec_id,displayName="sky-method",group=group,description=help2))

      paramList.append(RecipeParameter(rec_id,displayName="sky-bspline-nbkpts-first",group=group,description=help3))
      paramList.append(RecipeParameter(rec_id,displayName="sky-bspline-nbkpts-second",group=group,description=help4))
      paramList.append(RecipeParameter(rec_id,displayName="sky-bspline-order",group=group,description=help5))
      paramList.append(RecipeParameter(rec_id,displayName="sky-bspline-niter",group=group,description=help6))
      paramList.append(RecipeParameter(rec_id,displayName="sky-bspline-kappa",group=group,description=help7))
      paramList.append(RecipeParameter(rec_id,displayName="sky-median-hsize",group=group,description=help8))
      paramList.append(RecipeParameter(rec_id,displayName="sky-slit-edges-mask",group=group,description=help9))
      paramList.append(RecipeParameter(rec_id,displayName="sky-position1",group=group,description=help10))
      paramList.append(RecipeParameter(rec_id,displayName="sky-hheight1",group=group,description=help11))
      paramList.append(RecipeParameter(rec_id,displayName="sky-position2",group=group,description=help12))
      paramList.append(RecipeParameter(rec_id,displayName="sky-hheight2",group=group,description=help13))
      paramList.append(RecipeParameter(rec_id,displayName="bspline-sampling",group=group,description=help14))
