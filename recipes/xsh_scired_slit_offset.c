/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2013-08-29 10:49:42 $
 * $Revision: 1.158 $
 *
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup xsh_scired_slit_offset   xsh_scired_slit_offset
 * @ingroup recipes
 *
 * This recipe ...
 * See man-page for details.
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*-----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/

/* DRL steps */

/* Error handling */
#include <xsh_error.h>
#include <stdio.h>
/* Utility fonctions */
#include <xsh_utils.h>
#include <xsh_utils_scired_slit.h>
#include <xsh_data_spectrum.h>

#include <xsh_msg.h>
/* DFS functions */
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <xsh_data_spectrum1D.h>
#include <xsh_model_arm_constants.h>
#include <xsh_utils_image.h>

/* DRL functions */
#include <xsh_drl.h>
#include <xsh_drl_check.h>
/* Library */
#include <cpl.h>

/*-----------------------------------------------------------------------------
  Defines
  ---------------------------------------------------------------------------*/

#define RECIPE_ID "xsh_scired_slit_offset"
#define RECIPE_AUTHOR "P.Goldoni, L.Guglielmi, R. Haigron, F. Royer, D. Bramich, A. Modigliani"
#define RECIPE_CONTACT "amodigli@eso.org"

/*-----------------------------------------------------------------------------
  Functions prototypes
  ----------------------------------------------------------------------------*/

/*
 *   Plugin initalization, execute and cleanup handlers
 */

static int xsh_scired_slit_offset_create( cpl_plugin *);
static int xsh_scired_slit_offset_exec( cpl_plugin *);
static int xsh_scired_slit_offset_destroy( cpl_plugin *);

/* The actual executor function */
static void xsh_scired_slit_offset( cpl_parameterlist *, cpl_frameset *);

/*-----------------------------------------------------------------------------
  Static variables
  ----------------------------------------------------------------------------*/
static char xsh_scired_slit_offset_description_short[] =
"Reduce science exposure in SLIT configuration and on/off mode";

static char xsh_scired_slit_offset_description[] =
"This recipe reduces science exposure in SLIT configuration and on/off mode\n\
Input Frames : \n\
  - A set of n Science frames ( n even ), \
      Tag = STD_FLUX_SLIT_OFFSET_arm, SKY_SLIT_arm\n\
  - Spectral format table (Tag = SPECTRAL_FORMAT_TAB_arm)\n\
  - A master flat frame (Tag = MASTER_FLAT_SLIT_arm)\n\
  - An order table frame(Tag = ORDER_TABLE_EDGES_SLIT_arm)\n\
  - A wavelength calibration solution frame(Tag = WAVE_TAB_2D_arm)\n\
  - [OPTIONAL] Table with dispersion coefficients (Tag = DISP_TAB_arm)\n\
    It is required if -rectify-conserve-flux=TRUE\n\
  - [OPTIONAL] A non-linear badpixel map (Tag = BP_MAP_NL_arm)\n\
  - [OPTIONAL] A reference badpixel map (Tag = BP_MAP_RP_arm)\n\
  - [OPTIONAL] The instrument response table (Tag = RESPONSE_MERGE1D_SLIT_arm)\n\
  - [OPTIONAL] An atmospheric extinction table (Tag = ATMOS_EXT_arm)\n\
  - [OPTIONAL] A telluric mask (Tag = TELL_MASK_arm)\n\
  - [OPTIONAL] The instrument master response table (Tag = MRESPONSE_MERGE1D_SLIT_arm).\n\
    If both master and individual response are provided the individual response is preferred.\n\
Products : \n\
  - PREFIX_ORDER2D_arm extracted spectrum, order-by-order, 2D\n\
  - PREFIX_ORDER1D_arm extracted spectrum, order-by-order, 1D\n\
  - PREFIX_MERGE2D_arm merged spectrum, 2D\n\
  - PREFIX_MERGE1D_arm merged spectrum, 1D\n\
  - PREFIX_SKY_arm, 2D sky frame\n\
  - SKY_SLIT_ORDER2D_arm, 2D sky image (order-by-order)\n\
  - SKY_SLIT_MERGE2D_arm, 2D sky image (merged)\n\
  - PREFIX_WAVE_MAP_arm, wave map image\n\
  - PREFIX_SLIT_MAP_arm, slit map image\n\
 - where PREFIX is SCI, FLUX, TELL if input raw DPR.TYPE contains OBJECT or FLUX or TELLURIC\n\
  - [OPTIONAL, if response and atm ext are provided]  PREFIX_FLUX_ORDER2D_arm (2 dimension)\n\
  - [OPTIONAL, if response and atm ext are provided]  PREFIX_FLUX_ORDER1D_arm (1 dimension)\n\
  - [OPTIONAL, if response and atm ext are provided]  PREFIX_FLUX_MERGE2D_arm (2 dimension)\n\
  - [OPTIONAL, if response and atm ext are provided]  PREFIX_FLUX_MERGE1D_arm (1 dimension)\n";


/*-----------------------------------------------------------------------------
  Functions code
  ----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
   @brief    Build the list of available plugins, for this module.
   @param    list    the plugin list
   @return   0 if everything is ok, -1 otherwise

   Create the recipe instance and make it available to the application using the
   interface. This function is exported.
*/
/*----------------------------------------------------------------------------*/

int cpl_plugin_get_info(cpl_pluginlist *list) {
  cpl_recipe *recipe = NULL;
  cpl_plugin *plugin = NULL;

  recipe = cpl_calloc(1, sizeof(*recipe));
  if ( recipe == NULL ){
    return -1;
  }

  plugin = &recipe->interface ;

  cpl_plugin_init(plugin,
                  CPL_PLUGIN_API,                    /* Plugin API */
                  XSH_BINARY_VERSION,             /* Plugin version */
                  CPL_PLUGIN_TYPE_RECIPE,            /* Plugin type */
                  RECIPE_ID,                         /* Plugin name */
                  xsh_scired_slit_offset_description_short,   /* Short help */
                  xsh_scired_slit_offset_description,         /* Detailed help */
                  RECIPE_AUTHOR,                     /* Author name */
                  RECIPE_CONTACT,                    /* Contact address */
                  xsh_get_license(),                 /* Copyright */
                  xsh_scired_slit_offset_create,
                  xsh_scired_slit_offset_exec,
                  xsh_scired_slit_offset_destroy);

  cpl_pluginlist_append(list, plugin);

  return (cpl_error_get_code() != CPL_ERROR_NONE);
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Setup the recipe options
   @param    plugin  the plugin
   @return   0 if everything is ok

   Create the recipe instance and make it available to the application using the
   interface.

*/
/*----------------------------------------------------------------------------*/

static int xsh_scired_slit_offset_create(cpl_plugin *plugin){
  cpl_recipe *recipe = NULL;

  /* First param (conv_kernel) should be initialized correctly ! */
  xsh_remove_crh_single_param crh_single = { 0.1, 20, 2.0, 4} ;
  xsh_rectify_param rectify = { "tanh",
                                CPL_KERNEL_DEFAULT, 
                                2,
                                -1.0, 
                                -1.0,
                                1,0,0.};
  /* 2nd and 3rd params should be initialized correctly */
  xsh_localize_obj_param loc_obj = 
     {10, 0.1, 0, 0, LOC_MANUAL_METHOD, 0, 2.0,3,3,FALSE};
  //xsh_extract_param extract_par = { LOCALIZATION_METHOD};
  xsh_combine_nod_param nod_param = { 5, TRUE, 5, 2, 0.1, "throwlist.asc", COMBINE_MEAN_METHOD } ;
  xsh_interpolate_bp_param ipol_par = {30 };
  /* Reset library state */
  xsh_init();

  /* Check input */
  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin");
  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
          CPL_ERROR_TYPE_MISMATCH,
          "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;

  /* Create the parameter list in the cpl_recipe object */
  recipe->parameters = cpl_parameterlist_new();
  assure( recipe->parameters != NULL,
          CPL_ERROR_ILLEGAL_OUTPUT,
          "Memory allocation failed!");

  /* Set generic parameters (common to all recipes) */
  check( xsh_parameters_generic( RECIPE_ID, recipe->parameters ) ) ;
  xsh_parameters_decode_bp(RECIPE_ID,recipe->parameters,-1);
  check( xsh_parameters_pre_overscan( RECIPE_ID, recipe->parameters ) ) ;

  /* remove_crh_single */
  check(xsh_parameters_remove_crh_single_create(RECIPE_ID,recipe->parameters,
						crh_single )) ;
  /* xsh_rectify */
  check(xsh_parameters_rectify_create(RECIPE_ID,recipe->parameters,
					    rectify )) ;
  /* xsh_localize_object */
  check(xsh_parameters_localize_obj_create(RECIPE_ID,recipe->parameters,
					   loc_obj )) ;
  /* xsh_optimal_extract
  check(xsh_parameters_optimal_extract_create(RECIPE_ID,
					      recipe->parameters,-1. )) ;
					      */
  /* trivial extract (Just temporary)
  check(xsh_parameters_extract_create(RECIPE_ID,
				      recipe->parameters,
				      extract_par,LOCALIZATION_METHOD )) ;
  */

  check(xsh_parameters_interpolate_bp_create(RECIPE_ID,
                recipe->parameters,ipol_par)) ;

  check(xsh_parameters_combine_nod_create(RECIPE_ID,
					  recipe->parameters,
					  nod_param )) ;
  /*
  check( xsh_parameters_new_boolean( recipe->parameters, RECIPE_ID,
    "do-flatfield", TRUE, 
    "TRUE if we do the flatfielding"));
*/

  check( xsh_parameters_new_boolean( recipe->parameters, RECIPE_ID,
    "gen-sky", TRUE, 
    "if TRUE a 2D sky frame, a 2D rectified, a 2D merged sky are generated"));

  check( xsh_parameters_new_boolean( recipe->parameters, RECIPE_ID,
      "cut-uvb-spectrum", TRUE,
      "TRUE if recipe cuts the UVB spectrum at 556 nm (dichroich)"));

  /*
  check( xsh_parameters_new_boolean( recipe->parameters, RECIPE_ID,
    "compute-map", TRUE,
     "if TRUE recompute (wave and slit) maps from the dispersion solution. Use FALSE only if you are sure to have wave and slit maps better than what this recipe may compute."));
  */
  
  /* Flag for generation of data in SDP format. */
  check( xsh_parameters_new_boolean( recipe->parameters, RECIPE_ID,
    "generate-SDP-format", FALSE,
    "TRUE if additional files should be generated in Science Data Product"
    " (SDP) format."));

  /* Flag for adding dummy ASSO[NCM]i keywords. */
  check( xsh_parameters_new_int( recipe->parameters, RECIPE_ID,
    "dummy-association-keys", 0,
    "Sets the number of dummy (empty) ASSONi, ASSOCi and ASSOMi keywords to"
    " create."));
  
  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE ){
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else {
      return 0;
    }
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Execute the plugin instance given by the interface
   @param    plugin  the plugin
   @return   0 if everything is ok
*/
/*----------------------------------------------------------------------------*/

static int xsh_scired_slit_offset_exec(cpl_plugin *plugin) {
  cpl_recipe *recipe = NULL;

  /* Check parameter */

  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin");

  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
          CPL_ERROR_TYPE_MISMATCH, "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;

  /* Check recipe */
  xsh_scired_slit_offset(recipe->parameters, recipe->frames);

  cleanup:
    if ( cpl_error_get_code() != CPL_ERROR_NONE ) {
      xsh_error_dump(CPL_MSG_ERROR);
      xsh_error_reset();
      return 1;
    }
    else {
      return 0;
    }
}

/*----------------------------------------------------------------------------*/
/**
   @brief    Destroy what has been created by the 'create' function
   @param    plugin  the plugin
   @return   0 if everything is ok
*/
/*----------------------------------------------------------------------------*/
static int xsh_scired_slit_offset_destroy(cpl_plugin *plugin)
{
  cpl_recipe *recipe = NULL;

  /* reset error state before detroying recipe */
  xsh_error_reset(); 
  /* Check parameter */
  assure( plugin != NULL, CPL_ERROR_NULL_INPUT, "Null plugin" );

  /* Get the recipe out of the plugin */
  assure( cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE,
    CPL_ERROR_TYPE_MISMATCH, "Plugin is not a recipe");

  recipe = (cpl_recipe *)plugin;

  xsh_free_parameterlist(&recipe->parameters);

 cleanup:
  if (cpl_error_get_code() != CPL_ERROR_NONE)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}


static cpl_error_code 
xsh_params_monitor(xsh_rectify_param * rectify_par,
                   xsh_localize_obj_param * loc_obj_par)
{


  xsh_msg_dbg_low("rectify params: radius=%g bin_lambda=%g bin_space=%g",
	  rectify_par->rectif_radius,rectify_par->rectif_bin_lambda,
	  rectify_par->rectif_bin_space);

  xsh_msg_dbg_low("localize params: chunk_nb=%d nod_step=%g",
	  loc_obj_par->loc_chunk_nb,loc_obj_par->nod_step);

  return cpl_error_get_code();

}

/*----------------------------------------------------------------------------*/
/**
   @brief    Interpret the command line options and execute the data processing
   @param    parameters     the parameters list
   @param    frameset   the frames list

   In case of failure the cpl_error_code is set.
*/
/*----------------------------------------------------------------------------*/
static void xsh_scired_slit_offset( cpl_parameterlist* parameters, 
  cpl_frameset* frameset)
{
   const char* recipe_tags[3] = {XSH_OBJECT_SLIT_OFFSET, XSH_STD_FLUX_SLIT_OFFSET, XSH_SKY_SLIT};
  int recipe_tags_size = 3;

  /* Input frames */
  cpl_frameset* raws = NULL;

  cpl_frameset * raw_object = NULL;
  cpl_frameset * raw_sky = NULL;
  cpl_frameset * calib = NULL;
  cpl_frameset * usedframes = NULL;
  int nobj=0;
  int nsky=0;

  int nb_sub_frames = 0;
  /* Beware, do not "free" the following input frames, they are part
     of the input frameset */
  cpl_frame* bpmap = NULL;
  cpl_frame* master_bias = NULL;
  cpl_frame* master_dark = NULL;
  cpl_frame* master_flat = NULL;
  cpl_frame* order_tab_edges = NULL;
  cpl_frame * wave_tab = NULL ;
  cpl_frame * model_config_frame = NULL ;
  cpl_frame * wavemap = NULL ;
  cpl_frame * slitmap = NULL ;
  cpl_frame *disp_tab_frame = NULL;
  cpl_frame * spectral_format = NULL ;
  cpl_frame *tellmask_frame = NULL;
  /* Parameters */
  xsh_remove_crh_single_param * crh_single_par = NULL ;
  xsh_rectify_param * rectify_par = NULL ;
  xsh_localize_obj_param * loc_obj_par = NULL ;

  xsh_extract_param * extract_par = NULL ;
  xsh_combine_nod_param * combine_nod_param = NULL ;

  xsh_instrument* instrument = NULL;

  /* Intermediate frames */

  cpl_frame * loc_table_frame = NULL ; /**< Output of xsh_localize_object */
  cpl_frame * clean_frame = NULL ; /**< Output of remove_crh_single */
  cpl_frameset * sub_frameset = NULL ;	/**< frameset of subtracted offset frames */
  cpl_frameset * clean_frameset = NULL ;	/**< frameset of clean offset frames */
  cpl_frameset * rect_frameset = NULL ; /**< frameset of rectified frames */
  cpl_frameset * rect2_frameset_tables = NULL ; /**< frameset of rectified frames */

  /* Output Frames (results)*/
  cpl_frame * res1D_frame = NULL ;	/**< Final (result) frame */
  cpl_frame * res2D_frame = NULL ;	/**< Final (result) frame */
  cpl_frame * ext_frame = NULL ;
  cpl_frame * ext_frame_eso = NULL ;
  cpl_frame * comb_frame = NULL ;
  cpl_frame * comb_frame_eso = NULL ;
  cpl_frame* response_ord_frame=NULL;

  char  file_name[256];
  char  arm_str[16] ;
  char  file_tag[40];

  int i ;
  cpl_frame* grid_back=NULL;
  cpl_frame* frame_backg=NULL;
  int do_flatfield=1;
  int gen_sky=0;
  int generate_sdp_format=0;
  char* rec_prefix=NULL;
  char sky_prefix[256];
  char sky_tag[256];
  cpl_frame* avg_sky=NULL;
  cpl_frame* rec_sky=NULL;
  cpl_frame* rec_sky_eso=NULL;
  cpl_frame* mer_sky=NULL;
  cpl_frame * rectif_tab = NULL ;
  cpl_frame * sky_divided = NULL ;
  cpl_frameset* sky_bias=NULL;
  cpl_frameset* sky_dark=NULL;
  cpl_frame * fluxcal_rect_1D_frame = NULL ;	
  cpl_frame * fluxcal_rect_2D_frame = NULL ;	
  cpl_frame * fluxcal_1D_frame = NULL ;	
  cpl_frame * fluxcal_2D_frame = NULL ;	
  cpl_frame* frm_atmext=NULL;
  cpl_frameset* crh_clean_obj = NULL;
  cpl_frameset* crh_clean_sky = NULL;
  cpl_frame* ext_sky_frame=NULL;
  cpl_frame* ext_sky_frame_eso=NULL;
  cpl_frame* res_1D_sky_frame=NULL;
  cpl_frame * fluxframe = NULL;
  cpl_frame * uncalframe = NULL;
  cpl_frame* sky_map_frm = NULL;
  int pre_overscan_corr=0;
  int merge_par=0;
  xsh_interpolate_bp_param *ipol_bp=NULL;
  int do_computemap=1;
  int use_model=0;
  int cut_uvb_spectrum=0;
  int scale_nod=0;
  /**************************************************************************/
  /* DFS management */
  /**************************************************************************/
  check( xsh_begin( frameset, parameters, &instrument, &raws, &calib,
                    recipe_tags, recipe_tags_size,
                    RECIPE_ID, XSH_BINARY_VERSION,
                    xsh_scired_slit_offset_description_short));

  check(xsh_ensure_raws_input_offset_recipe_is_proper(&raws,instrument));
  assure( instrument->mode == XSH_MODE_SLIT, CPL_ERROR_ILLEGAL_INPUT,
	  "Instrument NOT in Slit Mode" ) ;
  xsh_recipe_params_check(parameters,instrument,RECIPE_ID);
  if(instrument->arm == XSH_ARM_NIR) {
    xsh_instrument_nir_corr_if_JH(raws,instrument);
  }
  /**************************************************************************/
  /* Recipe frames */
  /**************************************************************************/
  check(xsh_slit_offset_get_calibs(calib,instrument,&bpmap,&master_bias,
                                   &master_dark,&order_tab_edges,
                                   &model_config_frame,&wave_tab,&master_flat,
                                   &wavemap,&slitmap,&spectral_format,RECIPE_ID));
  /* scired specifig input */
  if((response_ord_frame=xsh_find_frame_with_tag(calib,XSH_RESPONSE_MERGE1D_SLIT,
                                                 instrument)) == NULL ) {
  check( response_ord_frame = xsh_find_frame_with_tag(calib,
                                                      XSH_MRESPONSE_MERGE1D_SLIT,
                                                      instrument));
  }

   if(response_ord_frame != NULL) { 
      frm_atmext=xsh_find_frame_with_tag(calib,XSH_ATMOS_EXT,instrument);
      if(frm_atmext==NULL) {
         xsh_msg_error("Provide atmospheric extinction frame");
      }
   }

  tellmask_frame = xsh_find_frame_with_tag(calib,XSH_TELL_MASK, instrument);
  /**************************************************************************/
  /* Recipe parameters */
  /**************************************************************************/
  cut_uvb_spectrum=xsh_parameters_cut_uvb_spectrum_get(RECIPE_ID,parameters);
  check(xsh_slit_offset_get_params(parameters,RECIPE_ID,
                                  &loc_obj_par,&rectify_par,&crh_single_par,
                                  &extract_par,
                                  &combine_nod_param,&do_flatfield,&gen_sky,
                                  &generate_sdp_format));

  check(ipol_bp = xsh_parameters_interpolate_bp_get(RECIPE_ID,parameters));

  check(xsh_rectify_params_set_defaults(parameters,RECIPE_ID,instrument,rectify_par));
  rectify_par->conserve_flux=FALSE;

  check(xsh_params_monitor(rectify_par,loc_obj_par));

  if ( rectify_par->conserve_flux || model_config_frame == NULL){
     check_msg( disp_tab_frame = xsh_find_disp_tab( calib, instrument),
                "if rectify-conserve-flux=TRUE DISP_TAB_ARM is required input");
  }
  check( pre_overscan_corr = xsh_parameters_get_int( parameters, RECIPE_ID,
						     "pre-overscan-corr"));
  /**************************************************************************/
  /* Recipe code */
  /**************************************************************************/
  /* prepare RAW frames in PRE format */
  check(xsh_prepare(raws, bpmap, master_bias, XSH_OBJECT_SLIT_OFFSET,
              instrument,pre_overscan_corr,CPL_TRUE));

  /* make sure each input raw frame has the same exp time */
  check(xsh_frameset_check_uniform_exptime(raws,instrument));

  /* Separate OBJECT and SKY frames */
  check( nb_sub_frames = xsh_dfs_split_offset( raws, XSH_OBJECT_SLIT_OFFSET,
              XSH_SKY_SLIT, &raw_object,
              &raw_sky ) );

  nobj=cpl_frameset_get_size(raw_object);
  nsky=cpl_frameset_get_size(raw_sky);
  rec_prefix=xsh_set_recipe_file_prefix(raw_object,"xsh_scired_slit_offset");
  sprintf(sky_prefix,xsh_set_recipe_sky_file_prefix(rec_prefix));

  xsh_msg("cmap=%d wavemap=%p slitmap=%p",do_computemap,wavemap,slitmap);
  if( (do_computemap == 1) &&
      (wavemap ==NULL || slitmap ==NULL )
      ) {
        if (model_config_frame != NULL) {
          use_model=1;
        }

        check( xsh_check_get_map( disp_tab_frame, order_tab_edges,
                master_flat, model_config_frame, calib, instrument,
                do_computemap, use_model, rec_prefix,
                &wavemap, &slitmap));

  }


  if( nb_sub_frames==0 ) {
        xsh_msg_error("nb_sub_frames=%d something wrong check your input raws",nb_sub_frames);
        goto cleanup;

  }


  /* remove crh on each obj or sky frame */
  sky_map_frm = xsh_find_frame_with_tag(calib,XSH_SKY_MAP, instrument);
  crh_clean_obj = xsh_frameset_crh_single(raw_object, crh_single_par,
                  sky_map_frm,instrument,rec_prefix,"OBJ");
  crh_clean_sky = xsh_frameset_crh_single(raw_sky, crh_single_par,
                  sky_map_frm, instrument,rec_prefix,"SKY");

  check(
      sub_frameset = xsh_subtract_sky_offset( crh_clean_obj, crh_clean_sky, nb_sub_frames,instrument));

  xsh_msg_dbg_low(
      "Nb of Subtracted Frames: %" CPL_SIZE_FORMAT "", cpl_frameset_get_size( sub_frameset ));

  sprintf(arm_str, "%s", xsh_instrument_arm_tostring(instrument));

  if (gen_sky) {
    /* Not yet working.. */
    if (master_bias != NULL && pre_overscan_corr == 0) {
      check(
          sky_bias=xsh_pre_frameset_subtract_frame(crh_clean_sky,master_bias,"MBIAS_",instrument));
    } else {
      sky_bias = cpl_frameset_duplicate(crh_clean_sky);
    }
    if (master_dark != NULL) {
      check(
          sky_dark=xsh_pre_frameset_subtract_frame(sky_bias,master_dark,"MDARK",instrument));
    } else {
      sky_dark = cpl_frameset_duplicate(sky_bias);
    }
    sprintf(sky_tag, "%s_SKY_%s", rec_prefix, arm_str);
    check(avg_sky=xsh_frameset_average_pre(sky_dark,instrument,sky_tag));
    if (do_flatfield == 1) {
      sprintf(sky_tag, "%s_FF_SKY_%s", rec_prefix, arm_str);
      check(
          sky_divided = xsh_divide_flat( avg_sky,master_flat, sky_tag, instrument ));
    } else {
      sky_divided = cpl_frame_duplicate(avg_sky);
    }
    xsh_add_temporary_file(cpl_frame_get_filename(sky_divided));
  }

  if (do_flatfield == 1) {
    clean_frameset=xsh_frameset_mflat_divide(sub_frameset,master_flat,instrument);
  } else {
    clean_frameset = cpl_frameset_duplicate(sub_frameset);
  }
  /* now we have duplicate sub_frameset we do not need it anymore */
  xsh_free_frameset(&sub_frameset);

  {
    int clean_size ;

    check( clean_size = cpl_frameset_get_size( clean_frameset ) ) ;
    xsh_msg( "Nb of clean Frames: %d", clean_size ) ;
  }


  /* Now rectify each clean frame */
  check( rect_frameset = cpl_frameset_new() ) ;
  for( i = 0 ; i < nb_sub_frames ; i++ ) {
    cpl_frame * rectif = NULL ;
    cpl_frame * rectif_eso = NULL ;
    cpl_frame * clean = NULL ;
    char str[16] ;

    sprintf( str, "%d", i ) ;
    check( clean = cpl_frameset_get_frame( clean_frameset, i ) ) ;
    xsh_msg( "Rectifying Frame '%s'", cpl_frame_get_filename( clean ) ) ;
    sprintf(file_name,"RECTIFIED_SLIT_OFFSET_%s_%s.fits",arm_str,str) ;
    check( rectif = xsh_rectify( clean, order_tab_edges, wave_tab,
				 model_config_frame, instrument,
				 rectify_par, spectral_format,
                                 disp_tab_frame,
				 file_name,&rectif_eso,&rectif_tab,
				 rec_prefix) ) ;
    xsh_add_temporary_file(file_name);
    check( cpl_frameset_insert( rect_frameset, cpl_frame_duplicate(rectif) ) ) ;
    check( cpl_frameset_insert( rect_frameset, cpl_frame_duplicate(rectif_eso) ) ) ;
    xsh_free_frame(&rectif_tab);
    xsh_free_frame(&rectif_eso);
    xsh_free_frame(&rectif);
  }


  /* Combine rectified Frames */
  sprintf(file_tag,"%s_%s_%s",rec_prefix,XSH_ORDER2D, arm_str) ;
  check(rect2_frameset_tables=xsh_frameset_ext_table_frames(rect_frameset));
  check( comb_frame = xsh_combine_nod(rect2_frameset_tables, combine_nod_param,
				       file_tag,instrument,&comb_frame_eso,scale_nod));
  xsh_free_frameset(&rect2_frameset_tables);


  if ( extract_par->method == LOCALIZATION_METHOD || 
       extract_par->method == CLEAN_METHOD) {
    xsh_msg( "Localize before extraction" ) ;
    sprintf(file_name,"LOCALIZE_%s_ALL.fits",arm_str) ;
    check( loc_table_frame = xsh_localize_obj( comb_frame, NULL,instrument,
					       loc_obj_par, NULL,
					       file_name) ) ;
    xsh_add_temporary_file(file_name);
  } 



  if(gen_sky) {
     xsh_msg("rectify sky frame");
     sprintf(file_name,"%s_RECTIFIED_SKY_%s.fits",sky_prefix,arm_str) ;
     xsh_msg("file_name=%s",file_name);
     check( rec_sky = xsh_rectify(sky_divided, order_tab_edges, wave_tab,
                                  model_config_frame, instrument,
                                  rectify_par, spectral_format,
                                  disp_tab_frame,
                                  file_name,&rec_sky_eso,&rectif_tab,
                                  sky_prefix) ) ;

     xsh_add_temporary_file(file_name);

     xsh_msg("Merge 2D sky frame");
     check( mer_sky = xsh_merge_ord( rec_sky, instrument,
                                     merge_par,sky_prefix ));

     check(ext_sky_frame = xsh_extract_clean(rec_sky, loc_table_frame,
					     instrument, extract_par,ipol_bp,
					     &ext_sky_frame_eso,rec_prefix )) ;
    
     check( res_1D_sky_frame = xsh_merge_ord( ext_sky_frame, instrument,
					      merge_par,sky_prefix));
     xsh_free_frame(&rectif_tab);
  }


  xsh_msg( "Calling xsh_extract" ) ;
  check(ext_frame = xsh_extract_clean(comb_frame, loc_table_frame,
				instrument, extract_par,ipol_bp, &ext_frame_eso,
				rec_prefix )) ;
  xsh_msg( "Calling xsh_merge_ord with 1D frame" ) ;
  check( res1D_frame = xsh_merge_ord( ext_frame, instrument,
				      merge_par,rec_prefix ));
  check( xsh_mark_tell( res1D_frame, tellmask_frame));
  /* More todo here
      mark_telluric
     calibrate_flux
  */


  xsh_msg( "Calling xsh_merge_ord with 2D frame" ) ;
  check( res2D_frame = xsh_merge_ord( comb_frame, instrument, merge_par,
				      rec_prefix) ) ;

  if(response_ord_frame != NULL && frm_atmext != NULL) {
   check(xsh_flux_calibrate(comb_frame_eso,ext_frame_eso,frm_atmext,
			    response_ord_frame,merge_par,instrument,rec_prefix,
                            &fluxcal_rect_2D_frame,&fluxcal_rect_1D_frame,
			    &fluxcal_2D_frame,&fluxcal_1D_frame));

  }


  if(model_config_frame!=NULL && wavemap != NULL&& slitmap != NULL) {

     check(xsh_compute_resampling_accuracy(wavemap,slitmap,order_tab_edges,model_config_frame,comb_frame_eso,instrument));
     check(xsh_compute_resampling_accuracy(wavemap,slitmap,order_tab_edges,model_config_frame,res2D_frame,instrument));

     check(xsh_compute_wavelength_resampling_accuracy(wavemap,order_tab_edges,model_config_frame,res1D_frame,instrument));
     check(xsh_compute_wavelength_resampling_accuracy(wavemap,order_tab_edges,model_config_frame,ext_frame_eso,instrument));

     xsh_add_afc_info(model_config_frame,wavemap);
     xsh_add_afc_info(model_config_frame,slitmap);

     if(fluxcal_rect_2D_frame != NULL) {

        check(xsh_compute_resampling_accuracy(wavemap,slitmap,order_tab_edges,model_config_frame,fluxcal_rect_2D_frame,instrument));
        check(xsh_compute_resampling_accuracy(wavemap,slitmap,order_tab_edges,model_config_frame,fluxcal_2D_frame,instrument));

        check(xsh_compute_wavelength_resampling_accuracy(wavemap,order_tab_edges,model_config_frame,fluxcal_rect_1D_frame,instrument));
        check(xsh_compute_wavelength_resampling_accuracy(wavemap,order_tab_edges,model_config_frame,fluxcal_1D_frame,instrument));

     }

     if(gen_sky) {
       check(xsh_compute_resampling_accuracy(wavemap,slitmap,order_tab_edges,model_config_frame,rec_sky_eso,instrument));
       check(xsh_compute_resampling_accuracy(wavemap,slitmap,order_tab_edges,model_config_frame,mer_sky,instrument));
     }

  }

  if(cut_uvb_spectrum) {
       if(instrument->arm == XSH_ARM_UVB) {
           xsh_image_cut_dichroic_uvb(comb_frame_eso);
           xsh_spectrum_orders_cut_dichroic_uvb(ext_frame_eso,instrument);
           xsh_spectrum_cut_dichroic_uvb(res2D_frame);
           xsh_spectrum_cut_dichroic_uvb(res1D_frame);

           if(gen_sky) {

               xsh_image_cut_dichroic_uvb(rec_sky_eso);
               xsh_image_cut_dichroic_uvb(mer_sky);
               xsh_spectrum_cut_dichroic_uvb(res_1D_sky_frame);

           }
           if(fluxcal_rect_2D_frame != NULL) {

               xsh_image_cut_dichroic_uvb(fluxcal_rect_2D_frame);
               xsh_spectrum_orders_cut_dichroic_uvb(fluxcal_rect_1D_frame,instrument);
               xsh_spectrum_cut_dichroic_uvb(fluxcal_2D_frame);
               xsh_spectrum_cut_dichroic_uvb(fluxcal_1D_frame);

           }
       }
   }


  /* Save products */
  xsh_msg( "Saving products" ) ;
  
  usedframes = cpl_frameset_duplicate(frameset);
  
  /* Removed as format not easy to read
   check( xsh_add_product_table( comb_frame, frameset,
  			      parameters, RECIPE_ID, instrument));
   *
   */

  check( xsh_add_product_image( comb_frame_eso, frameset, 
				parameters, RECIPE_ID, instrument,NULL));

  /* Removed as format not easy to read
  check( xsh_add_product_table( ext_frame, frameset,
				      parameters, RECIPE_ID, instrument));
  */

  check( xsh_add_product_orders_spectrum( ext_frame_eso, frameset,
				parameters, RECIPE_ID, instrument,NULL));

 
  check( xsh_add_product_spectrum( res2D_frame, frameset, parameters,
				RECIPE_ID, instrument, NULL));

  check(xsh_monitor_spectrum1D_flux(res1D_frame,instrument));
  check( xsh_add_product_spectrum( res1D_frame, frameset, parameters,
			      RECIPE_ID, instrument, &uncalframe));


  if(gen_sky) {
     check( xsh_add_product_image( avg_sky, frameset, parameters,
                                   RECIPE_ID, instrument,NULL));


     sprintf(file_tag,"%s_%s_%s",sky_prefix,XSH_ORDER2D, arm_str) ;
     check( xsh_add_product_image( rec_sky_eso, frameset, parameters,
                                   RECIPE_ID, instrument,NULL));


     sprintf(file_tag,"%s_%s_%s",sky_prefix,XSH_MERGE2D, arm_str) ;
     check( xsh_add_product_image( mer_sky, frameset, parameters,
                                   RECIPE_ID, instrument,NULL));
     check( xsh_add_product_spectrum( res_1D_sky_frame, frameset, parameters,
				      RECIPE_ID, instrument, NULL));

  }

  if(fluxcal_rect_2D_frame != NULL) {
     check( xsh_add_product_image(fluxcal_rect_2D_frame,frameset,parameters, 
                                  RECIPE_ID, instrument,NULL));
     check( xsh_add_product_orders_spectrum(fluxcal_rect_1D_frame,frameset,parameters,
                                  RECIPE_ID, instrument,NULL));
     check( xsh_add_product_spectrum( fluxcal_2D_frame, frameset, parameters, 
                                      RECIPE_ID, instrument, NULL));
     check( xsh_add_product_spectrum( fluxcal_1D_frame, frameset, parameters, 
                                      RECIPE_ID, instrument, &fluxframe));
  }
  if (do_computemap){
     //sprintf(prefix,"%s_WAVE_MAP",rec_prefix);


    check(xsh_add_product_image( wavemap, frameset,
      parameters, RECIPE_ID, instrument, NULL));

    //check(sprintf(prefix,"%s_SLIT_MAP",rec_prefix));

    check(xsh_add_product_image( slitmap, frameset,
      parameters, RECIPE_ID, instrument,NULL));
  }
  
  if (generate_sdp_format) {
    cpl_frame * fluxframe_copy = fluxframe;
    cpl_frame * uncalframe_copy = uncalframe;
    if (fluxframe != NULL) {
      check( cpl_frameset_insert(usedframes, fluxframe) );
      fluxframe = NULL;  /* prevent direct deletion in cleanup section,
                            will be deleted together with usedframes. */
    }
    if (uncalframe != NULL) {
      check( cpl_frameset_insert(usedframes, uncalframe) );
      uncalframe = NULL;  /* prevent direct deletion in cleanup section,
                             will be deleted together with usedframes. */
    }
    check( xsh_add_sdp_product_spectrum(fluxframe_copy, uncalframe_copy,
                    frameset, usedframes, parameters, RECIPE_ID, instrument) );
    /* Restore usedframes to what it was before in case we want to reuse it */
    if (fluxframe_copy != NULL) {
      check( cpl_frameset_erase_frame(usedframes, fluxframe_copy) );
    }
    if (uncalframe_copy != NULL) {
      check( cpl_frameset_erase_frame(usedframes, uncalframe_copy) );
    }
  }

  cleanup:
    xsh_end( RECIPE_ID, frameset, parameters );
    XSH_FREE( rec_prefix);
    XSH_FREE( crh_single_par ) ;
    XSH_FREE( rectify_par ) ;
    XSH_FREE( loc_obj_par ) ;
    XSH_FREE(extract_par);
    XSH_FREE(ipol_bp);
    XSH_FREE(combine_nod_param);

    xsh_instrument_free(&instrument );
    xsh_free_frameset(&rect2_frameset_tables);
    xsh_free_frameset(&raws);

    /* the following two frees are dangerous in case of failure (e.g. nsky=0) */
    if(nobj>0) {
    xsh_free_frameset( &raw_object);
    }
    if(nsky>0) {
    xsh_free_frameset( &raw_sky);
    }
    xsh_free_frameset(&usedframes);
    xsh_free_frameset(&calib);
    xsh_free_frameset(&clean_frameset);
    xsh_free_frameset(&crh_clean_obj);
    xsh_free_frameset(&crh_clean_sky);

    //this create seg fault
    xsh_free_frameset(&rect_frameset);

    xsh_free_frameset(&sky_bias);
    xsh_free_frameset(&sky_dark);
    xsh_free_frame(&bpmap);
    xsh_free_frame(&wavemap) ;
    xsh_free_frame(&slitmap) ;
    xsh_free_frame(&loc_table_frame) ;
    xsh_free_frame(&clean_frame) ;
    xsh_free_frame(&res1D_frame) ;
    xsh_free_frame(&res2D_frame) ;
    xsh_free_frame( &comb_frame ) ;
    xsh_free_frame( &comb_frame_eso ) ;
    xsh_free_frame( &ext_frame ) ;
    xsh_free_frame( &ext_frame_eso ) ;
    xsh_free_frame( &ext_sky_frame);
    xsh_free_frame( &ext_sky_frame_eso);

    xsh_free_frame( &avg_sky ) ;
    xsh_free_frame( &sky_divided ) ;
    xsh_free_frame( &rec_sky ) ;
    xsh_free_frame( &rec_sky_eso ) ;
    xsh_free_frame( &mer_sky ) ;

    xsh_free_frame( &frame_backg ) ;
    xsh_free_frame( &grid_back ) ;
    xsh_free_frame(&fluxcal_rect_1D_frame) ;
    xsh_free_frame(&fluxcal_rect_2D_frame) ;
    xsh_free_frame(&fluxcal_1D_frame) ;
    xsh_free_frame(&fluxcal_2D_frame) ;
    xsh_free_frame(&res_1D_sky_frame) ;

    xsh_free_frame(&fluxframe);
    xsh_free_frame(&uncalframe);

    return;
}

/**@}*/
