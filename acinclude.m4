# XSH_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([XSH_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# XSH_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([XSH_SET_VERSION_INFO],
[
    xsh_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    xsh_major_version=`echo "$xsh_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    xsh_minor_version=`echo "$xsh_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    xsh_micro_version=`echo "$xsh_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$xsh_major_version"; then xsh_major_version=0
    fi

    if test -z "$xsh_minor_version"; then xsh_minor_version=0
    fi

    if test -z "$xsh_micro_version"; then xsh_micro_version=0
    fi

    XSH_VERSION="$xsh_version"
    XSH_MAJOR_VERSION=$xsh_major_version
    XSH_MINOR_VERSION=$xsh_minor_version
    XSH_MICRO_VERSION=$xsh_micro_version

    if test -z "$4"; then XSH_INTERFACE_AGE=0
    else XSH_INTERFACE_AGE="$4"
    fi

    XSH_BINARY_AGE=`expr 100 '*' $XSH_MINOR_VERSION + $XSH_MICRO_VERSION`
    XSH_BINARY_VERSION=`expr 10000 '*' $XSH_MAJOR_VERSION + \
                          $XSH_BINARY_AGE`

    AC_SUBST(XSH_VERSION)
    AC_SUBST(XSH_MAJOR_VERSION)
    AC_SUBST(XSH_MINOR_VERSION)
    AC_SUBST(XSH_MICRO_VERSION)
    AC_SUBST(XSH_INTERFACE_AGE)
    AC_SUBST(XSH_BINARY_VERSION)
    AC_SUBST(XSH_BINARY_AGE)

    AC_DEFINE_UNQUOTED(XSH_MAJOR_VERSION, $XSH_MAJOR_VERSION,
                       [XSH major version number])
    AC_DEFINE_UNQUOTED(XSH_MINOR_VERSION, $XSH_MINOR_VERSION,
                       [XSH minor version number])
    AC_DEFINE_UNQUOTED(XSH_MICRO_VERSION, $XSH_MICRO_VERSION,
                       [XSH micro version number])
    AC_DEFINE_UNQUOTED(XSH_INTERFACE_AGE, $XSH_INTERFACE_AGE,
                       [XSH interface age])
    AC_DEFINE_UNQUOTED(XSH_BINARY_VERSION, $XSH_BINARY_VERSION,
                       [XSH binary version number])
    AC_DEFINE_UNQUOTED(XSH_BINARY_AGE, $XSH_BINARY_AGE,
                       [XSH binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# XSH_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([XSH_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi
 
    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi

    htmldir='${pipedocsdir}/html'

    if test -z "$configdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    testpath=`cd $srcdir/xsh && pwd`/tests/data

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(htmldir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)
    AC_SUBST(testsdir)

    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(XSH_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(XSH_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

    AC_DEFINE_UNQUOTED(XSH_TEST_DATA_PATH,"$testpath",
                       [Absolute path to the tests data directory tree])
])


# XSH_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([XSH_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    XSH_INCLUDES='-I$(top_srcdir)/xsh -I$(top_srcdir)/irplib -I$(top_srcdir)/hdrl'
    XSH_LDFLAGS='-L$(top_builddir)/xsh'

    # Library aliases
    LIBXSH='$(top_builddir)/xsh/libxsh.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'
    LIBHDRL='$(top_builddir)/hdrl/libhdrl.la'

    # Using irplib_fits_update_checksums function:
    IRPLIB_CPPFLAGS='-DIRPLIB_USE_FITS_UPDATE_CHECKSUM'

    # Substitute the defined symbols

    AC_SUBST(XSH_INCLUDES)
    AC_SUBST(XSH_LDFLAGS)
    AC_SUBST(LIBXSH)
    AC_SUBST(LIBIRPLIB)
    AC_SUBST(IRPLIB_CPPFLAGS)
    AC_SUBST(HDRL_INCLUDES)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(XSH_INCLUDES) $(CPL_INCLUDES)  $(GSL_INCLUDES) $(EXTRA_INCLUDES) $(CFITSIO_INCLUDES)'
    all_ldflags='$(XSH_LDFLAGS) $(CPL_LDFLAGS) $(GSL_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])
