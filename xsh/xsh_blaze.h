/* $Id: xsh_blaze.h,v 1.3 2011-12-02 14:15:28 amodigli Exp $
 *
 * This file is part of the X-shooter Pipeline
 * Copyright (C) 2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_BLAZE_H
#define XSH_BLAZE_H

/*-----------------------------------------------------------------------------
                                    Includes
 -----------------------------------------------------------------------------*/
#include <cpl.h>
#include <xsh_error.h>
#include <xsh_dfs.h>
#include <xsh_utils.h>
#include <xsh_data_pre.h>
#include <xsh_data_rec.h>
#include <xsh_data_instrument.h>
#include <xsh_data_order.h>
#include <xsh_data_dispersol.h>
#include <xsh_parameters.h>
#include <xsh_qc_handling.h>
/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/
 

/*-----------------------------------------------------------------------------
                             Typedefs
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                    Prototypes
 -----------------------------------------------------------------------------*/
#define MSE_LIMIT 1

cpl_frame *xsh_blaze_image( cpl_frame* masterflat_frame,
   cpl_frame *order_frame, xsh_instrument *instrument);

cpl_frame *xsh_divide_by_blaze( cpl_frame *pre_frame, cpl_frame *blaze_frame,
  xsh_instrument *instrument);

cpl_image* xsh_create_blaze( cpl_frame* masterflat_frame, 
  xsh_order_list* order_list, xsh_instrument *instrument);

#endif
