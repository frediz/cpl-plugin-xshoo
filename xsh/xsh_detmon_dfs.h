/* $Id: detmon_dfs.h,v 1.5 2013-01-29 08:56:40 jtaylor Exp $
 *
 * This file is part of the IIINSTRUMENT Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-01-29 08:56:40 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef DETMON_DFS_H
#define DETMON_DFS_H

/*-----------------------------------------------------------------------------
   								Define
 -----------------------------------------------------------------------------*/

/* Define here the PRO.CATG keywords */
#define DETMON_IR_LG            "DETMON_IR_LG"

/* Define here the DO.CATG keywords */
#define DETMON_IR_LG_ON_RAW                    "ON_RAW"
#define DETMON_IR_LG_OFF_RAW			"OFF_RAW"

#define DETMON_OPT_LG_ON_RAW                    "ON_RAW"
#define DETMON_OPT_LG_OFF_RAW			"OFF_RAW"

#define NIR TRUE
#define OPT FALSE

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

#endif
