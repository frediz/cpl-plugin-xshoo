/* $Id: xsh_irplib_mkmaster.h,v 1.5 2012-06-13 09:25:20 amodigli Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-06-13 09:25:20 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_IRPLIB_MKMASTER_H
#define XSH_IRPLIB_MKMASTER_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_detmon.h>

cpl_image*
xsh_irplib_mkmaster_mean(cpl_imagelist* images,const double kappa, const int nclip,
    const double tolerance,const double klow,const double khigh);

cpl_image* 
xsh_irplib_mkmaster_median(cpl_imagelist* images,const double kappa, const int nclip,
    const double tolerance);

cpl_imagelist*
xsh_irplib_mkmaster_dark_fill_imagelist(const cpl_imagelist* raw_images,
    cpl_propertylist** raw_headers, const cpl_image* master_bias,
    double* mean_exptime);
cpl_vector *
xsh_irplib_imagelist_get_clean_mean_levels(const cpl_imagelist* iml,
                                       const double kappa,
                                       const int nclip,
                                       const double tolerance);
#endif
