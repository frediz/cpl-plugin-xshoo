/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-08-06 16:25:39 $
 * $Revision: 1.29 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_UTILS_IMAGE_H
#define XSH_UTILS_IMAGE_H

/*----------------------------------------------------------------------------
                                    Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_error.h>
#include <xsh_data_order.h>

typedef float    pixelvalue ;

/*---------------------------------------------------------------------------
                                  Defines
 ---------------------------------------------------------------------------*/

typedef struct _STATS_
{
    float cleanmean ;          /* mean of pixel values without considering 
                                  the extreme values */
    float cleanstdev ;         /* standard deviation of pixel values without 
                                  considering the extreme values */
    int   npix ;               /* number of clean pixel values */
} Stats ;


#define TRANSFO_AFFINE          0
#define TRANSFO_DEG2            1
#define TRANSFO_HOMOGRAPHIC     2

/* Number of pixels set to 0 by the shift resampling */
#define    SHIFT_REJECT_L            2
#define    SHIFT_REJECT_R            2
#define    SHIFT_REJECT_T            2
#define    SHIFT_REJECT_B            2

/*
 * Kernel definition in terms of sampling
 */


/* Number of tabulations in kernel  */
#define TABSPERPIX      (1000)
#define KERNEL_WIDTH    (2.0)
#define KERNEL_SAMPLES  (1+(int)(TABSPERPIX * KERNEL_WIDTH))

#define TANH_STEEPNESS    (5.0)



cpl_image*
xsh_image_compute_geom_corr(cpl_image* in);

double   *
xsh_generate_interpolation_kernel(const char * kernel_type) ;

cpl_image *
xsh_warp_image_generic(
    cpl_image       *    image_in,
    char        *    kernel_type,
    cpl_polynomial  *    poly_u,
    cpl_polynomial  *    poly_v
) ;


cpl_image * 
xsh_image_search_bad_pixels_via_noise(cpl_imagelist *  darks,
                                      float      thresh_sigma_factor,
                                      float      low_threshold,
                                      float      high_threshold,
                                      int llx,
                                      int lly,
                                      int urx,
                                      int ury);

void xsh_pixel_qsort(pixelvalue *pix_arr, int npix) ;
void xsh_show_interpolation_kernel(char * kernel_name) ;
double * xsh_generate_tanh_kernel(double steep) ;

double
xsh_fixed_pattern_noise(const cpl_image *master,
                         double convert_ADU,
                        double master_noise);
double
xsh_fixed_pattern_noise_bias(const cpl_image *first_raw,
                              const cpl_image *second_raw,
                             double ron);

double xsh_image_get_stdev_clean(const cpl_image *image, 
				 double *dstdev);

double xsh_image_get_stdev_robust(const cpl_image *image, 
				   double cut,
                                  double *dstdev);

cpl_error_code xsh_image_warp_polynomial_scale(cpl_image *,
                                               const cpl_polynomial *poly_x,
                                               const cpl_polynomial *poly_y);
cpl_image* xsh_sobel_lx(cpl_image* in);
cpl_image* xsh_sobel_ly(cpl_image* in);
cpl_image* xsh_scharr_x(cpl_image* in);
cpl_image* xsh_scharr_y(cpl_image* in);
cpl_error_code
xsh_compute_ron(cpl_frameset* frames,
                int llx, 
                int lly, 
                int urx, 
                int ury,
                int nsampl, 
                int hsize,
		const int reg_id,
                double* ron, 
                double* ron_err);

cpl_image * xsh_image_smooth_fft(cpl_image * inp, const int fx, const int fy);
cpl_image * xsh_image_smooth_median_x(cpl_image * inp, const int r);
cpl_image * xsh_image_smooth_mean_x(cpl_image * inp, const int r);
cpl_image * xsh_image_smooth_median_y(cpl_image * inp, const int r);
cpl_image * xsh_image_smooth_mean_y(cpl_image * inp, const int r);
cpl_image * xsh_image_smooth_median_xy(cpl_image * inp, const int r);
double
xsh_image_fit_gaussian_max_pos_x_window(const cpl_image* ima,
					const int llx,
					const int urx,
                                        const int ypos);

cpl_frame* 
xsh_cube_qc_trace_window(cpl_frame* frm_cube,xsh_instrument* instrument,
                         const char* prefix,const char* rec_prefix,
                         const int win_min, const int win_max,
                         const int hsize, 
                         const int method,const int compute_qc);

cpl_frame* 
xsh_frame_image_qc_trace_window(cpl_frame* frm_ima,xsh_instrument* instrument,
                          const char* suffix,
                          const int hsize, const int method);
cpl_frame* 
xsh_frame_image_ext_qc_trace_window(cpl_frame* frm_ima,
                                    xsh_instrument* instrument,
                                    const char* suffix,
                                    const int hsize, 
                                    const int method);

cpl_error_code
xsh_iml_merge_avg(cpl_imagelist** data,
                  cpl_imagelist** mask,
                  const cpl_image* data_ima,
                  const cpl_image* mask_ima,
                  const int mk);
cpl_error_code
xsh_iml_merge_wgt(cpl_imagelist** data,
                  cpl_imagelist** errs,
                  cpl_imagelist** qual,
                  const cpl_image* flux_b,
                  const cpl_image* errs_b,
                  const cpl_image* qual_b,
                  const int mk,const int decode_bp);
cpl_error_code
xsh_image_mflat_detect_blemishes(cpl_frame* flat_frame,
			       xsh_instrument* instrument);

cpl_error_code
xsh_collapse_errs(cpl_image * errs, cpl_imagelist * list, const int mode);

cpl_image*
xsh_combine_flats(
    cpl_image* ima1_in,
    cpl_image* ima2_in,
    xsh_order_list* qth_list,
    xsh_order_list* d2_list,
    const int xrad,
    const int yrad);

cpl_error_code xsh_frame_image_save2ext(cpl_frame* frm,
					const char* name_o, const int ext_i,
					const int ext_o);
cpl_error_code xsh_frame_image_add_double(cpl_frame* frm, const double value);
cpl_frame* xsh_frame_image_mult_by_fct(cpl_frame* frm,const int fctx, const int fcty);

cpl_frame* 
xsh_frame_image_div_by_fct(cpl_frame* frm,const int fctx, const int fcty);
cpl_error_code xsh_image_cut_dichroic_uvb(cpl_frame* frame1d);

cpl_image*
xsh_compute_scale(cpl_imagelist* iml_data, cpl_mask* bpm,
                  const int mode, const int win_hsz);
cpl_image*
xsh_compute_scale_tab(cpl_imagelist* iml_data, cpl_mask* bpm, cpl_table* tab_bpm,
                  const int mode, const int win_hsz);
cpl_image*
xsh_compute_scale_tab2(cpl_imagelist* iml_data, cpl_imagelist* iml_qual,
		               cpl_mask* bpm, cpl_table* tab_bpm, const int mode,
					   const int win_hsz, const int decode_bp);

cpl_image*
xsh_compute_scale_tab3(cpl_imagelist* iml_data, cpl_imagelist* iml_qual,
		               cpl_mask* bpm, cpl_table* tab_bpm, const int mode,
					   const int win_hsz, const int decode_bp);
cpl_table*
xsh_qual2tab(cpl_image* bpm,const int code);
#endif
