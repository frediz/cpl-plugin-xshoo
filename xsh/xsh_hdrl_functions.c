/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or F1ITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
/*----------------------------------------------------------------------------*/
/**
 * @defgroup xsh_rectify  Rectify Image
 * @ingroup drl_functions
 *
 * Function ...
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*-----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/
#include <xsh_hdrl_functions.h>

#include <cpl.h>
#include <hdrl.h>

#include <xsh_msg.h>
#include <xsh_dfs.h>

#include <xsh_error.h>
#include <xsh_qc_handling.h>

/* This should be defined in a more clever way, a parameter for example */
#define REGDEBUG_FULL 0

cpl_frame*
xsh_hdrl_remove_crh_single( cpl_frame* frm,
                       xsh_instrument* instrument,
                       xsh_remove_crh_single_param *  crh_single_par,
                       const char* ftag ) {

    double sigma_lim, f_lim;
    int max_iter;
    xsh_pre* inp_pre = NULL; /**< Input PRE */
    xsh_pre* out_pre = NULL; /**< Result PRE */
    cpl_frame* res_frame=NULL;
    cpl_mask* result_mask=NULL;
    cpl_image* img_data;
    cpl_image* img_errs;
    cpl_image* img_mask;

    /* Check parameters */
    XSH_ASSURE_NOT_NULL_MSG( frm,"Null input science frame" );
    XSH_ASSURE_NOT_NULL_MSG( instrument,"Null instrument setting" );
    XSH_ASSURE_NOT_NULL_MSG( crh_single_par,"Null input parameters" );
    XSH_ASSURE_NOT_NULL_MSG( ftag,"Null input parameters" );

    sigma_lim = crh_single_par->sigma_lim;
    f_lim = crh_single_par->f_lim;
    max_iter = crh_single_par->nb_iter;
    xsh_msg_dbg_high("  Params: sigma_lim %.2f f_lim %.2f, iter %d",
                     sigma_lim, f_lim, max_iter);

    check( inp_pre = xsh_pre_load( frm, instrument));
    check( out_pre = xsh_pre_duplicate( inp_pre ) );
    check( img_data = xsh_pre_get_data( out_pre));
    check( img_errs = xsh_pre_get_errs( out_pre));
    check( img_mask = xsh_pre_get_qual( out_pre));


    // XSH has image data in FLOAT!!
    cpl_image* d_data=cpl_image_cast(img_data,CPL_TYPE_DOUBLE);
    cpl_image* d_errs=cpl_image_cast(img_errs,CPL_TYPE_DOUBLE);


    cpl_image_set_bpm(img_data,img_mask);
    hdrl_image * image = hdrl_image_create(d_data, d_errs);
    hdrl_parameter * params = hdrl_lacosmic_parameter_create(sigma_lim, f_lim, max_iter);

    result_mask = hdrl_lacosmic_edgedetect(image, params);
    int nb_crh=cpl_mask_count(result_mask);

    /* we need now to convert back to XSH format
     * Use images instead than mask
     * Use proper bad pixel code
     * Combine found CRH to original bad pixels
     */


    img_mask=cpl_image_new_from_mask(result_mask);
    cpl_image_multiply_scalar(img_mask,QFLAG_COSMIC_RAY_UNREMOVED);
    xsh_badpixelmap_image_coadd(&(out_pre->qual), img_mask,1);

#if REGDEBUG_FULL
    cpl_image_save( img_mask, "CRH_SINGLE.fits", CPL_BPP_32_SIGNED, NULL,
                    CPL_IO_DEFAULT);
    cpl_image_save( out_pre->qual, "CRH_SINGLE2.fits", CPL_BPP_32_SIGNED, NULL,
                       CPL_IO_DEFAULT);
#endif


    char* res_name = xsh_stringcat_any(ftag, ".fits", (void*)NULL);

    xsh_msg_dbg_high( "Saving Result Frame '%s'", res_name);
    check( xsh_add_qc_crh( out_pre, nb_crh, 1) );
    check( res_frame = xsh_pre_save( out_pre, res_name, ftag,0 ) );
    check( cpl_frame_set_tag( res_frame, ftag ) );

    XSH_FREE( res_name);

    cleanup:
    xsh_print_rec_status(16);
    cpl_image_delete(d_data);
    cpl_image_delete(d_errs);
    hdrl_parameter_delete(params) ;
    hdrl_image_delete(image);
    cpl_mask_delete(result_mask);

    return res_frame;
}


/*----------------------------------------------------------------------------*/

/**@}*/
