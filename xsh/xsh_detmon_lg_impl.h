/* $Id: detmon_lg_impl.h,v 1.1 2011-11-02 13:14:23 amodigli Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002, 2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02111-1307 USA
 */


/*
 * $Author: amodigli $
 * $Date: 2011-11-02 13:14:23 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.2  2009/08/17 15:10:16  kmirny
 *
 * DFS07454 DFS07437
 *
 * Revision 1.1  2009/08/12 09:26:43  kmirny
 * DFS07454
 *
*/
#ifndef XSH_IRPLIB_DETMON_LG_IMPL_H_
#define XSH_IRPLIB_DETMON_LG_IMPL_H_

cpl_error_code
xsh_detmon_pair_extract(const cpl_frameset *,
                           int *, int,
                           int ,
                           int *,
                           int,
                           cpl_frameset **);
cpl_error_code
xsh_detmon_check_order(const double *exptime, int sz, double tolerance, int order);


#endif /* XSH_IRPLIB_DETMON_LG_IMPL_H_ */
