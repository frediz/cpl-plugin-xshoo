
#ifndef XSH_EQWIDTH_LIB_H
#define XSH_EQWIDTH_LIB_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_sf_bessel.h>
#include "xsh_ngaussfdf.h"
//#include "espda_util_das.h"

/*-----------------------------------------------------------------------------
                                   Macros -----------------------------------------------------------------------------*/
#define max(a,b) (((a)>(b))?(a):(b))

/*-----------------------------------------------------------------------------
                                   Functions prototypes
 -----------------------------------------------------------------------------*/
cpl_error_code select_local_spec(cpl_table* spec_total,
                                 double ew_space,
                                 double lambda,
                                 cpl_table** spec_region);

//cpl_size get_index_from_spec(cpl_table* spec_total, double lambda);
//cpl_size get_index_from_spec_alt(cpl_table* spec_total, double lambda);

cpl_size get_nm_to_index_scale(cpl_table* spec_total, double nm);

cpl_error_code esp_fit_lcont(cpl_table* spec_region, 
                             double cont_rejt, 
                             int cont_iter);

cpl_error_code esp_det_line(cpl_table* spec_region, 
                             double det_line_thres,
                             double det_line_resol,
                             int det_line_smwidth,
                             cpl_table** line_table);


//cpl_table* esp_spec_deriv(cpl_table* spec_region);

//void esp_spec_smooth(cpl_table* spec_region, int smwidth);


void find_left_right_continuum_pos(int* xind1, int* xind2, 
                                   cpl_table* spec_region,
                                   double cont_rejt, double line);


void smooth(double vec[], long n, int w, double svec[]);

/*
void zeroscenterfind(double iy[], double y[], 
                     double dy[], double ddy[], 
                     long n, long center[], long *ncenter,double det_line_thres);
*/

double maxele_vec(double vec[], long nvec);

cpl_error_code esp_fit_ngauss(cpl_table* spec_cont_region, 
                              cpl_table* line_table,
                              double fit_ngauss_width);

double check_ew(cpl_table* line_table,
                double line,
                double det_line_resol,
                int* index_line,
                int* n_lines,
                double* ew_error);
/*
void poly_fitn(double xvec[], double yvec[], double err[], 
               long n, long ord, double coefs[]);
               */

void deriv(double x[], double y[], double dy[], long n);

void fitngauss(double t[], double y[], double sigma[], long nvec, 
               double acoef[], double acoef_er[], int para, int *status2);

cpl_error_code espda_create_line_table(cpl_table **tab,
                                       cpl_size    size);


#endif
