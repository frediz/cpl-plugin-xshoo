/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

#ifndef XSH_UTILS_WRAPPERS_H
#define XSH_UTILS_WRAPPERS_H

#include <cpl.h>
#include <xsh_cpl_size.h>
cpl_image*
xsh_image_filter_mode(const cpl_image* b,
                      const cpl_matrix * ker,
                      cpl_filter_mode filter);

cpl_polynomial * 
xsh_polynomial_fit_1d_create(
			       const cpl_vector    *   x_pos,
			       const cpl_vector    *   values,
			       int                     degree,
			       double              *   mse
			       );


cpl_polynomial * xsh_polynomial_fit_2d_create(cpl_bivector     *  xy_pos,
                                              cpl_vector       *  values,
                                              cpl_size*                 degree,
                                              double           *  mse);

cpl_image * xsh_image_filter_median(const cpl_image *, const cpl_matrix *);
cpl_image * xsh_image_filter_linear(const cpl_image *, const cpl_matrix *);

#endif
