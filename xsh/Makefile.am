## Process this file with automake to produce Makefile.in

##   This file is part of the X-shooter Pipeline
##   Copyright (C) 2006 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~


if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif


SUBDIRS=tests

AM_CPPFLAGS = -DCX_LOG_DOMAIN=\"XshLib\" $(all_includes) $(GSL_INCLUDES) $(IRPLIB_CPPFLAGS)

noinst_HEADERS =    xsh_utils.h               \
                    xsh_globals.h         \
                    xsh_utils_image.h         \
                    xsh_utils_imagelist.h         \
                    xsh_utils_response.h         \
                    xsh_utils_table.h         \
                    xsh_utils_vector.h         \
                    xsh_utils_scired_slit.h         \
                    xsh_utils_ifu.h         \
                    xsh_utils_wrappers.h         \
                    xsh_star_index.h      \
                    xsh_utils_efficiency.h      \
                    xsh_efficiency_response.h      \
                    irplib_error.h            \
                    xsh_error.h               \
			xsh_hist.h \
			xsh_spectrum.h \
                    xsh_msg.h                 \
                    xsh_dump.h                \
                    xsh_pfits.h               \
                    xsh_pfits_qc.h            \
                    xsh_dfs.h                 \
                    xsh_drl.h                 \
                    xsh_drl_check.h           \
                    xsh_paf_save.h            \
                    xsh_qc_handling.h         \
                    xsh_qc_definition.h       \
                    xsh_fit.h                 \
                    xsh_fit_body.h            \
                    xsh_parameters.h          \
                    xsh_compute_linearity.h   \
                    xsh_data_pre.h            \
                    xsh_data_instrument.h     \
                    xsh_data_order.h          \
                    xsh_data_the_map.h        \
                    xsh_data_resid_tab.h      \
                    xsh_data_arclist.h        \
                    xsh_data_wavesol.h        \
                    xsh_data_grid.h           \
                    xsh_data_shift_tab.h      \
                    xsh_badpixelmap.h         \
                    xsh_model_arm_constants.h \
		    xsh_model_kernel.h	      \
		    xsh_model_utils.h	      \
		    xsh_model_anneal.h	      \
		    xsh_model_io.h	      \
		    xsh_model_r250.h	      \
		    xsh_model_randlcg.h	      \
		    xsh_model_cputime.h	      \
		    xsh_model_sa.h	      \
		    xsh_model_metric.h	      \
                    xsh_model_ref_ind.h       \
		    xsh_data_linetilt.h       \
		    xsh_data_localization.h   \
		    xsh_data_rec.h            \
		    xsh_data_wavemap.h        \
                    xsh_data_spectrum.h       \
                       xsh_data_spectrum1D.h       \
                    xsh_data_spectralformat.h \
			xsh_data_order_resid_tab.h \
                        xsh_data_dispersol.h \
			xsh_ifu_defs.h \
			xsh_data_slice_offset.h \
			xsh_data_image_3d.h \
			xsh_data_pre_3d.h \
			xsh_data_star_flux.h \
			xsh_time.h \
			xsh_ksigma_clip.h \
			xsh_detmon.h \
			xsh_detmon_dfs.h \
			xsh_detmon_utils.h \
			xsh_detmon_body.h \
			xsh_detmon_lg_impl.h \
			xsh_detmon_lg.h \
			xsh_utils_polynomial.h \
			xsh_data_atmos_ext.h \
                        xsh_rectify.h \
                        xsh_baryvel.h \
                       xsh_irplib_mkmaster.h \
                       xsh_cpl_size.h \
                        xsh_blaze.h \
                        xsh_hdrl_functions.h \
                        xsh_eqwidth_lib.h \
                       xsh_ngaussfdf.h  
#                       xsh_struct.h 



pkginclude_HEADERS =

privatelib_LTLIBRARIES = libxsh.la

libxsh_la_SOURCES =    xsh_utils.c               \
                    xsh_utils_image.c         \
                    xsh_utils_imagelist.c         \
                    xsh_utils_response.c         \
                       xsh_utils_table.c         \
                    xsh_utils_vector.c         \
                    xsh_utils_scired_slit.c         \
                    xsh_utils_ifu.c         \
                    xsh_utils_wrappers.c         \
                       xsh_parameters.c          \
                       irplib_error.c            \
                    xsh_star_index.c      \
			xsh_hist.c \
			xsh_spectrum.c \
                       xsh_msg.c                 \
                       xsh_dump.c                \
                       xsh_pfits.c               \
                       xsh_pfits_qc.c            \
                       xsh_dfs.c                 \
                       xsh_drl_check.c           \
                       xsh_paf_save.c            \
                       xsh_qc_handling.c         \
                       xsh_fit.c                 \
                       xsh_data_pre.c            \
                       xsh_data_instrument.c     \
                       xsh_data_order.c          \
                       xsh_data_the_map.c        \
                       xsh_data_resid_tab.c      \
                       xsh_data_arclist.c        \
                       xsh_data_wavesol.c        \
		       xsh_data_linetilt.c       \
                       xsh_data_grid.c           \
                       xsh_data_shift_tab.c      \
                       xsh_detect_order.c        \
                       xsh_prepare.c             \
                       xsh_remove_crh_multi.c    \
                       xsh_badpixelmap.c         \
                       xsh_create_master.c       \
                       xsh_compute_noise_map.c   \
                       xsh_compute_linearity.c   \
                       xsh_subtract.c            \
                       xsh_divide.c              \
                       xsh_multiply.c              \
		       xsh_model_kernel.c        \
		    xsh_model_utils.c	      \
		       xsh_model_anneal.c        \
		       xsh_model_io.c            \
		       xsh_model_r250.c          \
		       xsh_model_randlcg.c       \
		       xsh_model_cputime.c       \
		       xsh_model_sa.c            \
		       xsh_model_metric.c        \
                       xsh_detect_continuum.c    \
                       xsh_detect_arclines.c     \
                       xsh_follow_arclines.c     \
                       xsh_rectify.c             \
                       xsh_localize_obj.c        \
                       xsh_extract.c             \
	       	       xsh_data_localization.c   \
                       xsh_remove_crh_single.c   \
                       xsh_merge_ord.c           \
                       xsh_data_rec.c            \
                       xsh_data_wavemap.c        \
                       xsh_data_spectrum.c       \
                       xsh_data_spectrum1D.c       \
                       xsh_data_spectralformat.c \
                       xsh_data_dispersol.c \
                       xsh_create_wavemap.c      \
                       xsh_create_order_table.c  \
                       xsh_flat_merge.c \
                       xsh_opt_extract.c \
                        xsh_subtract_sky_nod.c \
			xsh_subtract_sky_offset.c \
			xsh_combine_nod.c \
			xsh_data_order_resid_tab.c \
			xsh_data_slice_offset.c \
			xsh_compute_slice_dist.c \
			xsh_data_image_3d.c \
			xsh_data_pre_3d.c \
                        xsh_subtract_sky_single.c \
			xsh_format.c \
			xsh_compute_response.c \
			xsh_data_star_flux.c \
			xsh_time.c \
			xsh_ksigma_clip.c \
		       xsh_detmon.c \
		       xsh_detmon_utils.c \
                       xsh_detmon_lg.c \
                       xsh_data_check.c \
			xsh_utils_polynomial.c \
                    xsh_utils_efficiency.c      \
                    xsh_efficiency_response.c      \
			xsh_data_atmos_ext.c \
                       xsh_flexcor.c \
                       xsh_ifu.c \
			xsh_calibrate_flux.c \
                        xsh_baryvel.c \
                       xsh_irplib_mkmaster.c \
			xsh_compute_absorp.c \
                        xsh_blaze.c \
                        xsh_hdrl_functions.c \
                        xsh_eqwidth_lib.c 
#                      xsh_struct.c 



libxsh_la_LDFLAGS =  $(GSL_LDFLAGS) $(CPL_LDFLAGS) $(HDRL_LDFLAGS) -version-info $(LT_CURRENT):$(LT_REVISION):$(LT_AGE)
libxsh_la_LIBADD = $(LIBCFITSIO) $(LIBIRPLIB) $(LIBCPLDFS) $(LIBCPLUI) $(LIBCPLDRS) $(LIBCPLCORE) $(HDRL_LIBS) $(GSL_LIBS) 
libxsh_la_DEPENDENCIES = $(LIBIRPLIB)
