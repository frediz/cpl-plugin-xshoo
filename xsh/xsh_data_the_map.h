/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.15 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_THE_MAP_H
#define XSH_DATA_THE_MAP_H

#include <cpl.h>

#define XSH_THE_MAP_TABLE_NB_COL 6
#define XSH_THE_MAP_TABLE_COLNAME_WAVELENGTH "Wavelength"
#define XSH_THE_MAP_TABLE_UNIT_WAVELENGTH "nm"
#define XSH_THE_MAP_TABLE_COLNAME_ORDER "Order"
#define XSH_THE_MAP_TABLE_UNIT_ORDER "none"
#define XSH_THE_MAP_TABLE_COLNAME_SLITPOSITION "slit_position"
#define XSH_THE_MAP_TABLE_UNIT_SLITPOSITION "arcsec"
#define XSH_THE_MAP_TABLE_COLNAME_SLITINDEX "slit_index"
#define XSH_THE_MAP_TABLE_UNIT_SLITINDEX "none"
#define XSH_THE_MAP_TABLE_COLNAME_DETECTORX "detector_x"
#define XSH_THE_MAP_TABLE_UNIT_DETECTORX "pixel"
#define XSH_THE_MAP_TABLE_COLNAME_DETECTORY "detector_y"
#define XSH_THE_MAP_TABLE_UNIT_DETECTORY "pixel"

typedef struct{
  /* wavelength of arc lines */
  float wavelength;
  /* order */
  int order;
  /* slit position */
  float slit_position;
  /* slit index */
  int slit_index;
  /* given pixel position along X axis */
  double detector_x;
  /* given pixel position along Y axis */
  double detector_y;
}xsh_the_arcline;


typedef struct{
  int size;
  xsh_the_arcline** list;
  cpl_propertylist * header ;
} xsh_the_map;

xsh_the_map* xsh_the_map_create( int size);
xsh_the_map* xsh_the_map_load(cpl_frame* frame);
void xsh_the_arcline_free(xsh_the_arcline** arc);
void xsh_the_map_free(xsh_the_map** list);

void xsh_the_map_set_arcline( xsh_the_map* list, int idx, float wavelength,
  int order, int slit_index, float slit_position, double detx, double dety);

int xsh_the_map_get_size(xsh_the_map* list);
double xsh_the_map_get_detx(xsh_the_map* list, int idx);
double xsh_the_map_get_dety(xsh_the_map* list, int idx);
int xsh_the_map_get_slit_index( xsh_the_map* list, int idx);
float xsh_the_map_get_wavelength(xsh_the_map* list, int idx);
int xsh_the_map_get_order(xsh_the_map* list, int idx);
float xsh_the_map_get_slit_position( xsh_the_map* list, int idx);

cpl_frame* xsh_the_map_save(xsh_the_map* list,const char* filename);
void xsh_the_map_lambda_sort(xsh_the_map* list);
void xsh_the_map_lambda_order_slit_sort(xsh_the_map* list);
void xsh_dump_the_map( xsh_the_map* list ) ;

#endif  /* XSH_THE_MAP_H */
