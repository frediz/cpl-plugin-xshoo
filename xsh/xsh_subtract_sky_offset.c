/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-03-19 13:43:18 $
 * $Revision: 1.9 $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------*/
/**
 * @defgroup xsh_subtract_sky_offset  Subtract Sky OFFSET Frames
 * @ingroup drl_functions
 *
 * Function ...
 */
/*----------------------------------------------------------------------------*/
/**@{*/

/*-----------------------------------------------------------------------------
  Includes
  -----------------------------------------------------------------------------*/

#include <math.h>
#include <xsh_drl.h>

#include <xsh_badpixelmap.h>
#include <xsh_data_pre.h>
#include <xsh_data_order.h>
#include <xsh_data_wavemap.h>
#include <xsh_data_localization.h>
#include <xsh_data_rec.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_fit.h>
#include <xsh_badpixelmap.h>

#include <cpl.h>

/*-----------------------------------------------------------------------------
  Functions prototypes
  -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  Implementation
  -----------------------------------------------------------------------------*/

/** 
 * Subtract SKY frames from OBJECT frames. OBJECT and SKY frames are first
 * ordered by increasing observation date (Key Word DATE-OBS).
 * 
 * @param object_raws Raw OBJECT frames frameset
 * @param sky_raws Raw SKY frames frameset
 * @param nraws Number of OBJECT frames (equal nb of SKY frames)
 * @param instrument Pointer to instrument structure
 * 
 * @return Frameset of OBJECT-SKY frames
 */
cpl_frameset * xsh_subtract_sky_offset( cpl_frameset * object_raws,
					cpl_frameset * sky_raws,
					int nraws,
					xsh_instrument * instrument)
{
  cpl_frameset *result = NULL;
  char arm_name[16] ;
  cpl_frameset * objects = NULL, * skys = NULL ;
  int i ;

  XSH_ASSURE_NOT_NULL( object_raws);
  XSH_ASSURE_NOT_NULL( sky_raws);
  XSH_ASSURE_NOT_NULL( instrument);

  /*Before anything else, order the frameset by date=obs */
  check( objects = xsh_order_frameset_by_date( object_raws));
  check( skys = xsh_order_frameset_by_date( sky_raws));

  check( result = cpl_frameset_new());
  sprintf( arm_name, "%s", xsh_instrument_arm_tostring( instrument));

  /* For each pair OBJECT/SKY, subtract ==> OBJECT-SKY */
  for( i = 0 ; i<nraws ; i ++ ) {
    char a_b_name[256];
    cpl_frame *a = NULL, *b = NULL;
    cpl_frame *a_b = NULL ;

    check(a = cpl_frameset_get_frame( objects, i));
    check(b = cpl_frameset_get_frame( skys, i));

    xsh_msg( "1-st pair: OBJECT='%s'", cpl_frame_get_filename( a));
    xsh_msg( "           SKY   ='%s'", cpl_frame_get_filename( b));

    sprintf( a_b_name ,"SKY_SUBTRACTED_OFFSET_%s_%d.fits", 
      arm_name, i);
    /* The following function creates a new frame: as that is to be added
     * on the newly created result frameset, there is no need to duplicate it
     * when it will be inserted in it.
     */
    check( a_b = xsh_pre_frame_subtract( a, b, a_b_name, instrument,1));

    // Why this ?
    //   save_pre_frame( a_b, instrument ) ;

    check( cpl_frameset_insert( result, a_b));

  }

  xsh_msg_dbg_high( "Done OK" ) ;

  cleanup:
    xsh_free_frameset( &objects);
    xsh_free_frameset( &skys);
    return result ;

}


/**@}*/
