/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: rhaigron $
 * $Date: 2010-04-07 16:33:39 $
 * $Revision: 1.13 $
 */
#ifndef XSH_DATA_SPECTRALFORMAT_H
#define XSH_DATA_SPECTRALFORMAT_H

#include <cpl.h>
#include <xsh_data_instrument.h>
#include <xsh_data_order.h>
#include <xsh_data_wavesol.h>
#include <xsh_model_kernel.h>

#define XSH_SPECTRALFORMAT_TABLE_NB_UVB_ROWS 11
#define XSH_SPECTRALFORMAT_TABLE_NB_VIS_ROWS 14
#define XSH_SPECTRALFORMAT_TABLE_NB_NIR_ROWS 16

#define XSH_SPECTRALFORMAT_TABLE_COLNAME_ORDER "ORDER"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_LAMP "LAMP"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_WLMINFUL "WLMINFUL"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_WLMIN "WLMIN"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_WLMAX "WLMAX"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_WLMAXFUL "WLMAXFUL"

#define XSH_SPECTRALFORMAT_TABLE_COLNAME_DISP_MIN "DISP_MIN"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_DISP_MAX "DISP_MAX"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_FLSR "LFSR"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_UFSR "UFSR"

#define XSH_SPECTRALFORMAT_TABLE_COLNAME_XMIN "XMIN"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_XMAX "XMAX"

#define XSH_SPECTRALFORMAT_TABLE_COLNAME_YMIN "YMIN"
#define XSH_SPECTRALFORMAT_TABLE_COLNAME_YMAX "YMAX"

typedef struct{
  int absorder;
  char lamp[8];
  double lambda_min_full, lambda_max_full;
  double lambda_min, lambda_max;
  double flsr, ufsr;
  double xmin, xmax;
  double ymin, ymax;
} xsh_spectralformat ;

typedef struct{
  int size;
  int dist_order ;
  xsh_spectralformat * list;
  xsh_instrument * instrument; 
  cpl_propertylist * header;
} xsh_spectralformat_list;


/*****************************************************************************/
xsh_spectralformat_list* xsh_spectralformat_list_create( int size, 
  xsh_instrument *instr);

xsh_spectralformat_list* xsh_spectralformat_list_load( cpl_frame *frame,
  xsh_instrument *instr);

void xsh_spectralformat_list_free( xsh_spectralformat_list **list);

void xsh_spectralformat_list_dump( xsh_spectralformat_list *list,
  const char* fname);

float xsh_spectralformat_list_get_lambda_min( xsh_spectralformat_list *list,
  int absorder);

float xsh_spectralformat_list_get_lambda_max( xsh_spectralformat_list *list,
  int absorder);

const char* xsh_spectralformat_list_get_lamp( xsh_spectralformat_list * list,
  int absorder);

cpl_propertylist* xsh_spectralformat_list_get_header(
  xsh_spectralformat_list* list) ;

cpl_vector* xsh_spectralformat_list_get_orders( 
  xsh_spectralformat_list* list, float lambda);

int xsh_spectralformat_list_get_dist_order(xsh_spectralformat_list *list);

void xsh_spectralformat_check_wlimit( xsh_spectralformat_list *list, 
  xsh_order_list* order, xsh_wavesol *w, xsh_xs_3* model, 
  xsh_instrument *instr);

#endif  /* XSH_DATA_SPECTRALFORMAT_H */
