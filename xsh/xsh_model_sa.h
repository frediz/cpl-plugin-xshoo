/* $Id: xsh_model_sa.h,v 1.4 2008-05-23 09:08:18 amodigli Exp $
 *
 * This file is part of the ESO X-shooter Pipeline                          
 * Copyright (C) 2006 European Southern Observatory 
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* sa.h	  	Prototypes for Simulated Annealing library */

/* rcsid: @(#)sa.h	1.2 15:54:42 3/30/93   EFC   */

#ifndef XSH_MODEL_SA_H
#define XSH_MODEL_SA_H 1.2

#include "xsh_model_kernel.h" 

typedef float (*CostFunction)(double*); 
 

/* the value that causes the "set/query" functions to just query */
#define NO_VALUE	-1
#define NO_VALUE_INT   -1
#define NO_VALUE_FLOAT -1.0

int xsh_SAInit(CostFunction f, int d);
void xsh_SAfree(void);
int xsh_SAiterations(int it);
int xsh_SAdwell(int d);
float xsh_SABoltzmann(float k);
float xsh_SAlearning_rate(float r);
float xsh_SAtemperature(float t);
float xsh_SAjump(float j);
float xsh_SArange(float r);
void xsh_SAinitial(double* xi);
void xsh_SAcurrent(double* xc);
void xsh_SAoptimum(double* xb);
float xsh_SAmelt(int iters);
float xsh_SAanneal(int iters);

#endif


