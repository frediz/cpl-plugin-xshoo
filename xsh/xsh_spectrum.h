/* $Id: xsh_spectrum.h,v 1.3 2011-12-02 14:15:28 amodigli Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_SPECTRUM_H
#define XSH_SPECTRUM_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                New types
 -----------------------------------------------------------------------------*/

typedef enum SPEC_SHADOWS {
    /* 2 shadows above and below true spectrum */
    TWO_SHADOWS,
    /* 1 shadow at specified distance from spectrum */
    ONE_SHADOW,
    /* Do not search for shadow */
    NO_SHADOW
} spec_shadows ;

/*-----------------------------------------------------------------------------
                                Prototypes
 -----------------------------------------------------------------------------*/

int xsh_spectrum_find_brightest(const cpl_image *, int, spec_shadows, 
        double, int, double *) ;
cpl_vector * xsh_spectrum_detect_peaks(const cpl_vector *, int,
        double, int) ;

#endif
