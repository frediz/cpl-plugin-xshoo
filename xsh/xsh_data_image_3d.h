/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:37:36 $
 * $Revision: 1.2 $
*/

#if !defined(XSH_DATA_IMAGE_3D_H)
#define XSH_DATA_IMAGE_3D_H

#include <xsh_data_instrument.h>

#include <cpl.h>
#include <xsh_error.h>


typedef struct {
  int nx, ny, nz ;

  cpl_type type ;

  void * pixels ;

} xsh_image_3d ;


xsh_image_3d * xsh_image_3d_new( int nx, int ny, int nz, cpl_type type ) ;
xsh_image_3d * xsh_image_3d_load( const char * filename, cpl_type type,
				  int xtnum ) ;

cpl_error_code xsh_image_3d_insert( xsh_image_3d * img_3d, cpl_image * img,
				    int iz ) ;
cpl_error_code xsh_image_3d_save( xsh_image_3d * img_3d,
				  const char * fname,
				  cpl_propertylist * header,
				  unsigned mode ) ;
void xsh_image_3d_free( xsh_image_3d ** img_3d ) ;

void * xsh_image_3d_get_data( xsh_image_3d * img_3d ) ;
int xsh_image_3d_get_size_x( xsh_image_3d * img_3d ) ;
int xsh_image_3d_get_size_y( xsh_image_3d * img_3d ) ;
int xsh_image_3d_get_size_z( xsh_image_3d * img_3d ) ;
cpl_type xsh_image_3d_get_type( xsh_image_3d * img_3d ) ;

#endif
