/* $Id: xsh_badpixelmap.h,v 1.49 2012-11-21 15:47:34 amodigli Exp $
 *
 * This file is part of the X-shooter Pipeline
 * Copyright (C) 2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-11-21 15:47:34 $
 * $Revision: 1.49 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_BADPIXELMAP_H
#define XSH_BADPIXELMAP_H

/*------------------------------------------------------------------------
   				   Includes
 -------------------------------------------------------------------------*/
#include <xsh_cpl_size.h>
#include <xsh_data_instrument.h>
#include <xsh_data_pre.h>
#include <string.h>
#include <cpl.h>

/*-------------------------------------------------------------------------
   				   Define
 -------------------------------------------------------------------------*/

/*
  Quality Flags in the bad pixel maps

  Bad pixel maps are 32 bits int images

*/
#define QFLAG_GOOD_PIXEL 0
#define QFLAG_TELLURIC_CORRECTED 1
#define QFLAG_TELLURIC_UNCORRECTED 0x2
#define QFLAG_GHOST_STRAY_LIGHT 0x4
#define QFLAG_ELECTRONIC_PICKUP 0x8
#define QFLAG_COSMIC_RAY_REMOVED 0x10

/* the last good pixel level */
#define XSH_GOOD_PIXEL_LEVEL QFLAG_COSMIC_RAY_REMOVED
#define XSH_BAD_PIXEL XSH_GOOD_PIXEL_LEVEL+1

//#define XSH_GOOD_PIXEL_LEVEL QFLAG_GHOST_STRAY_LIGHT
/* first really pixel not used */
#define QFLAG_COSMIC_RAY_UNREMOVED 0x20
#define QFLAG_LOW_QE_PIXEL 0x40
#define QFLAG_CALIB_FILE_DEFECT 0x80
#define QFLAG_HOT_PIXEL 0x100
#define QFLAG_DARK_PIXEL 0x200
#define QFLAG_QUESTIONABLE_PIXEL 0x400
#define QFLAG_WELL_SATURATION 0x800
#define QFLAG_ADC_SATURATION 0x1000
#define QFLAG_CAMERA_DEFECT 0x2000
#define QFLAG_OTHER_BAD_PIXEL 0x4000
  /* Room for user defined bad pixel codes */
#define QFLAG_NON_LINEAR_PIXEL 0x8000         /* 2e16 =>  32 768 */
#define QFLAG_NON_SPATIAL_UNIFORMITY 0x10000  /* 2e17     65 536 */
#define QFLAG_DIVISOR_ZERO 0x20000            /* 2e18    131 072 */
#define QFLAG_OUT_OF_NOD   0x40000            /* 2e19    262 144 */
/*Not in rectified shifted frame */
/* ... */
#define QFLAG_MISSING_DATA 0x80000          /* 2e19       524 288 */
#define QFLAG_SATURATED_DATA 0x100000       /* 2e20     1 048 576 */
#define QFLAG_NEGATIVE_DATA 0x200000        /* 2e21     2 097 152 */
#define QFLAG_INTERPOL_FLUX 0x400000        /* 2e22     4 194 304 */
#define QFLAG_SKY_MODEL_BAD_FIT 0x800000    /* 2e23     8 388 608 */
#define QFLAG_SKY_MODEL_BAD_PIX 0x1000000   /* 2e24    16 777 216 */
#define QFLAG_ALL_PIX_BAD 0x2000000         /* 2e25    33 554 432 */
#define QFLAG_INCOMPLETE_DATA 0x4000000     /* 2e26    67 108 864 */
#define QFLAG_INCOMPLETE_NOD 0x8000000      /* 2e27   134 217 728 */
#define QFLAG_SCALED_NOD 0x10000000         /* 2e28   268 435 456 */
#define QFLAG_OUTSIDE_DATA_RANGE 0x40000000 /* 2e30 1 073 741 824 */
    /*} PIXEL_QUALITY_FLAG ; */

/*------------------------------------------------------------------------
                                Functions prototypes
 -------------------------------------------------------------------------*/

double cpl_tools_get_median_double( double *, int ) ;

void xsh_bpmap_set_bad_pixel( cpl_image * bpmap, int ix, int iy,
			      int flag ) ;
void xsh_bpmap_mask_bad_pixel(cpl_image * bpmap, cpl_mask* mask,
  int flag ) ;
cpl_mask* xsh_qual_to_cpl_mask(cpl_image * qual, const int decode_bp);
cpl_mask*
xsh_code_is_in_qual(cpl_image * qual, const int code) ;
cpl_frame*
xsh_badpixelmap_crea_master_from_bpmap(cpl_frame* bpmap, xsh_instrument* inst);

cpl_image * xsh_bpmap_collapse_bpmap_create( cpl_imagelist *list,const int decode_bp ) ;
cpl_mask * xsh_bpm_filter(
        const cpl_mask    *   input_mask,
        cpl_size        kernel_nx,
        cpl_size        kernel_ny,
        cpl_filter_mode filter);

int xsh_bpmap_count( cpl_image *bpmap, int nx, int ny ) ;
void xsh_bpmap_collapse_median(cpl_image* median, cpl_imagelist *list,
			       cpl_mask *mask ) ;
void xsh_bpmap_collapse_mean( cpl_image * mean, cpl_imagelist *list,
			      cpl_mask *mask ) ;
void xsh_set_image_cpl_bpmap( cpl_image * image,
			      cpl_image *bpmap, const int decode_bp ) ;
void xsh_badpixelmap_or( xsh_pre *self, const xsh_pre *right ) ;
void xsh_bpmap_bitwise_to_flag(cpl_image * bpmap,int flag );
cpl_error_code xsh_badpixelmap_coadd(cpl_frame *self, const cpl_frame *right,const int mode ) ;

cpl_error_code
xsh_count_satpix(xsh_pre* pre, xsh_instrument* instr, const int datancom);
cpl_error_code
xsh_count_crh(xsh_pre* pre, xsh_instrument* instr,const int datancom);

cpl_error_code
xsh_image_clean_badpixel(cpl_frame* in);

cpl_error_code
xsh_frame_qual_update(cpl_frame *frame, const cpl_frame *bpmap,xsh_instrument* instrument );

cpl_error_code
xsh_image_get_hot_cold_pixs(cpl_frame* frame_image, 
                            xsh_instrument* instrument,
                            const double ks_low,
                            const int cold_niter,
                            const double ks_high,
                            const int hot_niter,
                            cpl_frame** cpix_frm,
                            cpl_frame** hpix_frm);

cpl_error_code
xsh_badpixel_flag_rejected(cpl_image* qual, cpl_image* image);

cpl_frame*
xsh_image_local_cold_pixs(cpl_image* ima, 
			  const double kappa,
                          const int r,
                          xsh_instrument* instr);

cpl_frame*
xsh_image_local_hot_pixs(cpl_image* ima, 
			 const double kappa,
                         const int r, 
                         xsh_instrument* instr);
void
xsh_image_flag_bp(cpl_image * image,cpl_image * mask, xsh_instrument* inst);

cpl_error_code
xsh_image_clean_mask_pixs(cpl_image** ima,cpl_image* msk,const int r);
cpl_image* xsh_image_flag_bptype_with_crox(cpl_image* ima);
cpl_error_code
xsh_badpixelmap_flag_saturated_pixels(xsh_pre* pre,xsh_instrument* instr,
		const double cor_val, const int flag, const int is_flat, int* nsat);
cpl_error_code
xsh_badpixelmap_fill_bp_pattern_holes(cpl_frame* frm);
cpl_error_code
xsh_badpixelmap_image_coadd(cpl_image **self, const cpl_image *right,const int mode );
cpl_frame* xsh_badpixelmap_extract(cpl_frame* frame, int xmin, int ymin, 
				   int xmax, int ymax);
cpl_error_code
xsh_badpixelmap_count_range_pixels(xsh_pre* pre,
				   const double thresh_min, 
				   const double thresh_max,
                                   const double cor_val, 
                                   int* nrange, 
                                   double* frange);
#endif
