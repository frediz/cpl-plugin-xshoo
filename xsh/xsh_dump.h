/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_DUMP_H
#define XSH_DUMP_H

#include <cpl.h>

cpl_error_code xsh_print_cpl_propertylist(const cpl_propertylist *pl, 
		long low, long high);
cpl_error_code xsh_print_cpl_property(const cpl_property *);

cpl_error_code xsh_print_cpl_frameset( cpl_frameset *);
cpl_error_code xsh_print_cpl_frame(const cpl_frame *);

const char *xsh_tostring_cpl_type(cpl_type t);
const char *xsh_tostring_cpl_frame_type(cpl_frame_type);
const char *xsh_tostring_cpl_frame_group(cpl_frame_group);
const char *xsh_tostring_cpl_frame_level(cpl_frame_level);

#endif /* XSH_DUMP_H */
