/* $Id: xsh_model_arm_constants.h,v 1.10 2008-12-19 10:27:51 bristowp Exp $
 *
 *   This file is part of the ESO X-shooter Pipeline                          
 *   Copyright (C) 2006 European Southern Observatory  
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef XSH_MODEL_ARM_CONSTANTS_H
#define XSH_MODEL_ARM_CONSTANTS_H

#define UVB_BSIZE 3000;
#define UVB_ASIZE 2048;
#define UVB_chipxpix 2048.0;
#define UVB_chipypix 3000.0;
#define UVB_morder 19;
#define UVB_morder_min 13;
#define UVB_morder_max 24;
#define UVB_blaze_pad 0.00001;
#define UVB_SIZE 3000;
#define UVB_xsize_corr 96.0;
#define UVB_ysize_corr 0.0;

#define VIS_BSIZE 4000;
#define VIS_ASIZE 2048;
#define VIS_chipxpix 2048.0;
#define VIS_chipypix 4000.0;
#define VIS_morder 24;
#define VIS_morder_min 16;
#define VIS_morder_max 30;
#define VIS_blaze_pad 0.00003;
#define VIS_SIZE 4000;
#define VIS_xsize_corr 96.0;
#define VIS_ysize_corr 0.0;

#define NIR_BSIZE 2040;
#define NIR_ASIZE 1020;
#define NIR_chipxpix 1020.0;
#define NIR_chipypix 2040.0;
#define NIR_morder 19;
#define NIR_morder_min 11;
#define NIR_morder_max 26;
#define NIR_blaze_pad 0.00005;
#define NIR_SIZE 2048;
#define NIR_xsize_corr 4.0;
#define NIR_ysize_corr 8.0;

#endif


