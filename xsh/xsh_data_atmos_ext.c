/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-01-27 08:04:18 $
 * $Revision: 1.5 $
*/

/**
 * @defgroup data_handling    Data Format Handling functions
 */
/**
 * @defgroup xsh_data_atmos_ext  Atmospheric Extinction Table
 * @ingroup data_handling
 *
 */

/*----------------------------------------------------------------------------
  Includes
  ----------------------------------------------------------------------------*/

#include <xsh_dfs.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <cpl.h>
#include <string.h>
#include <time.h>
#include <xsh_utils_table.h>
#include <xsh_data_atmos_ext.h>

/*----------------------------------------------------------------------------
  Function prototypes
  ----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                              Implementation
 -----------------------------------------------------------------------------*/
xsh_atmos_ext_list * xsh_atmos_ext_list_create( int size )
{
  xsh_atmos_ext_list * result = NULL ;

  /* Create the list */
  XSH_CALLOC( result, xsh_atmos_ext_list, 1 ) ;
  result->size = size ;
  XSH_CALLOC( result->lambda, double, size ) ;
  XSH_CALLOC( result->K, double, size ) ;

 cleanup:
  return result ;
}

cpl_error_code 
xsh_atmos_ext_dump_ascii( xsh_atmos_ext_list * list, const char* filename )
{
   FILE *   fout ;
   int size=0;
   int i=0;
   double * plambda=NULL;
   double * pK=NULL;

   XSH_ASSURE_NOT_NULL_MSG(list,"Null input atmospheric ext frame list!Exit");
   size=list->size;

   plambda = list->lambda ;
   pK = list->K ;

   if((fout=fopen(filename,"w"))==NULL) {

      return CPL_ERROR_FILE_IO ;
   } else {
      for(i=0;i<size;i++) {
	fprintf(fout, "%f %f \n", plambda[i], pK[i]);
      }
   }
   if ( fout ) fclose( fout ) ;

 cleanup:
   return cpl_error_get_code() ;
}


xsh_atmos_ext_list * xsh_atmos_ext_list_load( cpl_frame * ext_frame )
{
  cpl_table *table = NULL ;
  const char * tablename = NULL ;
  xsh_atmos_ext_list * result = NULL ;
  int nentries, i ;
  double * plambda, * pK ;

  /* verify input */
  XSH_ASSURE_NOT_NULL( ext_frame);

  /* get table filename */
  check(tablename = cpl_frame_get_filename( ext_frame ));

  /* Load the table */
  XSH_TABLE_LOAD( table, tablename ) ;
  check( nentries = cpl_table_get_nrow( table ) ) ;

  /* Create the list */
  check( result = xsh_atmos_ext_list_create( nentries ) ) ;

  plambda = result->lambda ;
  pK = result->K ;
  if(!cpl_table_has_column(table,XSH_ATMOS_EXT_LIST_COLNAME_K)){
     xsh_msg_warning("You are using an obsolete atm extinction line table");
     cpl_table_duplicate_column(table,XSH_ATMOS_EXT_LIST_COLNAME_K,
                                table,XSH_ATMOS_EXT_LIST_COLNAME_OLD);
  }
  /* Fill the list */
  for( i = 0 ; i< nentries ; i++, plambda++, pK++ ) {
    float value ;

    check( xsh_get_table_value( table, XSH_ATMOS_EXT_LIST_COLNAME_WAVELENGTH,
				CPL_TYPE_FLOAT, i, &value ) ) ;
    *plambda = value ;
    check( xsh_get_table_value( table, XSH_ATMOS_EXT_LIST_COLNAME_K,
				CPL_TYPE_FLOAT, i, &value ) ) ;
    *pK = value ;
  }

 cleanup:
  if (cpl_error_get_code () != CPL_ERROR_NONE) {
    xsh_error_msg("can't load frame %s",cpl_frame_get_filename(ext_frame));
    xsh_atmos_ext_list_free(&result);
  }
  XSH_TABLE_FREE( table);
  return result ;
}


void xsh_atmos_ext_list_free( xsh_atmos_ext_list ** list )
{
  if ( list != NULL && *list != NULL ) {
    check( cpl_free( (*list)->lambda ) ) ;
    check( cpl_free( (*list)->K ) ) ;
    check( cpl_free( *list ) ) ;
    *list = NULL ;
  }

 cleanup:
  return ;
}

double * xsh_atmos_ext_list_get_lambda( xsh_atmos_ext_list * list )
{
  XSH_ASSURE_NOT_NULL( list ) ;

 cleanup:
  return list->lambda ;
}

double * xsh_atmos_ext_list_get_K( xsh_atmos_ext_list * list )
{
  XSH_ASSURE_NOT_NULL( list ) ;

 cleanup:
  return list->K ;
}
