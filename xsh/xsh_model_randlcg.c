/* $Id: xsh_model_randlcg.c,v 1.6 2011-12-02 14:15:28 amodigli Exp $
 *
 * This file is part of the ESO X-shooter Pipeline                          
 * Copyright (C) 2006 European Southern Observatory 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */
/* rndlcg            Linear Congruential Method, the "minimal standard generator"
                     Park & Miller, 1988, Comm of the ACM, 31(10), pp. 1192-1201

*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
/*---------------------------------------------------------------------------*/
/**
 * @addtogroup xsh_model    xsh_model_xsh_randlcg
 *
 * Physical model random generator
 * See man-page for details.
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "xsh_model_kernel.h"

/*----------------------------------------------------------------------------*/

//static char rcsid[] = "@(#)xsh_randlcg.c	1.1 15:48:15 11/21/94   EFC";

#include <math.h>
#include <limits.h>
#include <xsh_model_randlcg.h>

#define ALL_BITS     0xffffffff

static long int the_quotient  = LONG_MAX / 16807L;
static long int the_remainder = LONG_MAX % 16807L;

static long int seed_val = 1L;

long xsh_set_seed(long int sd)
{
        return seed_val = sd;
}

long get_seed(void)
{
        return seed_val;
}


unsigned long int xsh_randlcg(void)       /* returns a random unsigned integer */
{
        if ( seed_val <= the_quotient )
                seed_val = (seed_val * 16807L) % LONG_MAX;
        else
        {
                long int high_part = seed_val / the_quotient;
                long int low_part  = seed_val % the_quotient;

                long int test = 16807L * low_part - the_remainder * high_part;

                if ( test > 0 )
                        seed_val = test;
                else
                        seed_val = test + LONG_MAX;

        }

        return seed_val;
}

/**@}*/



