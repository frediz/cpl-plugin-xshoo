/*
 * This file is part of the ESO X-shooter Pipeline
 * Copyright (C) 2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-04-26 10:43:06 $
 * $Revision: 1.60 $
 */

#ifndef XSH_QC_DEFINITION_H

#define XSH_QC_DEFINITION_H



#define XSH_LAMRMS "LAMRMS"
#define XSH_LAMRMS_C "RMS of wavelength solution [CUNIT1]"
#define XSH_LAMNLIN "LAMNLIN"
#define XSH_LAMNLIN_C "No. of lines used in wavelength solution"
#define XSH_CRDER1 "CRDER1"
#define XSH_CRDER1_C "Wavelength uncertainty [CUNIT1]"
#define XSH_CSYER1 "CSYER1"
#define XSH_CSYER1_C "Typical systematic wavelength error [CUNIT1]"


#define XSH_CRDER2 "CRDER2"
#define XSH_CRDER2_C "Spatial coordinate uncertainty [CUNIT2]"
#define XSH_CSYER2 "CSYER2"
#define XSH_CSYER2_C "Spatial coordinate systematic uncertainty [CUNIT2]"
#define XSH_SPATRMS "SPATRMS"
#define XSH_SPATRMS_C "RMS on spatial coordinate uncertainty [CUNIT2]"

#define XSH_CUNIT1_C "Wavelength units"
#define XSH_CUNIT2_C "Spatial units"

#define XSH_QC_MASTER_MEAN "ESO QC MASTER MEAN"
#define XSH_QC_MASTER_MEAN_C "Estimated master (clean) mean (adu)"

#define XSH_QC_MASTER_RMS "ESO QC MASTER RMS"
#define XSH_QC_MASTER_RMS_C  "Estimated master (clean) rms (adu)"
#define XSH_QC_COLD_PIX_NUM  "ESO QC CPIXNUM"
#define XSH_QC_COLD_PIX_NUM_C  "Measured cold pixels"
#define XSH_QC_HOT_PIX_NUM  "ESO QC HPIXNUM"
#define XSH_QC_HOT_PIX_NUM_C  "Measured hot pixels"

#define XSH_QC_MASTER_BIAS_MEAN "ESO QC MBIASAVG"
#define XSH_QC_MASTER_BIAS_MEAN_C "Average value of the master BIAS (excluding bad pixels)."
#define XSH_QC_MASTER_BIAS_MEDIAN "ESO QC MBIASMED"
#define XSH_QC_MASTER_BIAS_MEDIAN_C "Median value of the master BIAS."
#define XSH_QC_MASTER_BIAS_RMS "ESO QC MBIASRMS"
#define XSH_QC_MASTER_BIAS_RMS_C "RMS of the master BIAS frame (excluding bad pixels)."

#define XSH_QC_STRUCT_X_REG1  "ESO QC STRUCTX1"
#define XSH_QC_STRUCT_X_REG1_C  "Slope in BIAS frame in the X direction on region 1. The frame is collapsed in the Y direction (excluding bad pixels) and fitted by a linear expression."

#define XSH_QC_STRUCT_Y_REG1  "ESO QC STRUCTY1"
#define XSH_QC_STRUCT_Y_REG1_C  "Slope in BIAS frame in the Y direction on region 1. The frame is collapsed in the X direction (excluding bad pixels) and fitted by a linear expression."

#define XSH_QC_STRUCT_X_REG2  "ESO QC STRUCTX2"
#define XSH_QC_STRUCT_X_REG2_C  "Slope in BIAS frame in the X direction on region 2. The frame is collapsed in the Y direction (excluding bad pixels) and fitted by a linear expression."

#define XSH_QC_STRUCT_Y_REG2  "ESO QC STRUCTY2"
#define XSH_QC_STRUCT_Y_REG2_C  "Slope in BIAS frame in the Y direction on region 2. The frame is collapsed in the X direction (excluding bad pixels) and fitted by a linear expression."

#define XSH_QC_RON "ESO QC RON"
#define XSH_QC_RON_C "Read Out Noise value (ADU)"

#define XSH_QC_RON_ERR "ESO QC RON ERR"
#define XSH_QC_RON_ERR_C   "Read Out Noise error (ADU)"

#define XSH_QC_RON_REG1 "ESO QC RON1"
#define XSH_QC_RON_REG1_C "Read Out Noise value on region 1 (ADU)"

#define XSH_QC_RON_REG1_ERR "ESO QC RON1 ERR"
#define XSH_QC_RON_REG1_ERR_C   "Read Out Noise error on region 1 (ADU)"

#define XSH_QC_RON_REG2 "ESO QC RON2"
#define XSH_QC_RON_REG2_C "Read Out Noise value on region 2 (ADU)"

#define XSH_QC_RON_REG2_ERR "ESO QC RON2 ERR"
#define XSH_QC_RON_REG2_ERR_C   "Read Out Noise error on region 2 (ADU)"


#define XSH_QC_RON_MASTER "ESO QC RON MASTER"
#define XSH_QC_RON_MASTER_C   "Read Out Noise value on mdaster frame (ADU)"


#define XSH_QC_CRH_RATE "ESO QC CRRATE"
#define XSH_QC_CRH_RATE_C "Number of detected CRH per cm2 and s"
#define XSH_QC_CRH_NUMBER  "ESO QC NCRH"
#define XSH_QC_CRH_NUMBER_C  "Number of detected cosmic ray hits"
#define XSH_QC_CRH_NUMBER_MEAN "ESO QC NCRH AVG"
#define XSH_QC_CRH_NUMBER_MEAN_C "Average number of cosmic ray hits per frame"

#define XSH_QC_CRH_NUMBER_TOT "ESO QC NCRH TOT"
#define XSH_QC_CRH_NUMBER_TOT_C "Total number of cosmic ray hits on frames"

#define XSH_QC_MASTER_DARK_MEDIAN  "ESO QC MDARKMED"
#define XSH_QC_MASTER_DARK_MEDIAN_C  "Median value of the master DARK."


#define XSH_QC_MASTER_DARK_MEAN  "ESO QC MDARKAVG"
#define XSH_QC_MASTER_DARK_MEAN_C  "Average value of the master DARK (excluding bad pixels)."

#define XSH_QC_MASTER_DARK_RMS  "ESO QC MDARKRMS"
#define XSH_QC_MASTER_DARK_RMS_C  "RMS value of the master DARK (excluding bad pixels)."

#define XSH_QC_NORM_FPN "ESO QC NORMFPN"
#define XSH_QC_NORM_FPN_C "Fixed Pattern Noise value normalized to 1s exposure"

#define XSH_QC_NORM_FPN_ERR "ESO QC NORMFPN ERR"
#define XSH_QC_NORM_FPN_ERR_C "Fixed Pattern Noise error normalized to 1s exposure"

#define XSH_QC_FPN "ESO QC FPN"
#define XSH_QC_FPN_C "Fixed Pattern Noise value"

#define XSH_QC_FPN_ERR "ESO QC FPN ERR"
#define XSH_QC_FPN_ERR_C "Fixed Pattern Noise error"

#define XSH_QC_FPN_MASTER "ESO QC FPN MASTER"
#define XSH_QC_FPN_MASTER_C "Fixed Pattern Noise value on master frame"

#define XSH_QC_FPN_ERR_MASTER "ESO QC FPN ERR MASTER"
#define XSH_QC_FPN_ERR_MASTER_C "Fixed Pattern Noise error on master frame"

#define XSH_QC_FLAT_FPNi  "ESO QC FLAT FPNi"
#define XSH_QC_FLAT_FPNi_C   "Fixed pattern noise."
#define XSH_QC_BP_MAP_NTOTAL "ESO QC BP-MAP NTOTAL"
#define XSH_QC_BP_MAP_NTOTAL_C "Number of flag pixels in the bad pixel map"

#define XSH_QC_BP_MAP_NFLAGi "ESO QC BP-MAP NFLAGi"
#define XSH_QC_BP_MAP_NFLAGi_C "Number of pixels for the given flag"


#define XSH_QC_NLINE_FOUND  "ESO QC NLINE FOUND"
#define XSH_QC_NLINE_FOUND_C  "Number of lines successfully matched with the theoretical table."

#define XSH_QC_NLINE_CAT  "ESO QC NLINE CAT"
#define XSH_QC_NLINE_CAT_C "Number of arc lines in the input catalog (arc line list)."

#define XSH_QC_NLINE_CAT_CLEAN  "ESO QC NLINE CAT CLEAN"
#define XSH_QC_NLINE_CAT_CLEAN_C "Number of arc lines after gaussian fit and sigma clipping."


#define XSH_QC_NLINE_FOUND_CLEAN  "ESO QC NLINE FOUND CLEAN"
#define XSH_QC_NLINE_FOUND_CLEAN_C "Number of lines successfully matched with the theoretical table after gaussian fit and sigma clipping."


#define XSH_QC_NLINE4_FOUND  "ESO QC NLINE4 FOUND"
#define XSH_QC_NLINE4_FOUND_C  "Number of pin-hole 4 lines successfully matched with the theoretical table."

#define XSH_QC_NLINE4_FOUND_CLEAN  "ESO QC NLINE4 FOUND CLEAN"
#define XSH_QC_NLINE4_FOUND_CLEAN_C "Number of pin-hole 4 lines successfully matched with the theoretical table after gaussian fit and sigma clipping."


#define XSH_QC_MODEL_NDAT "ESO QC MODEL NDAT"
#define XSH_QC_MODEL_NDAT_C "Number of data points over which is optimized the physical model."

#define XSH_QC_MODEL_FMTCHK_DATE "ESO QC FMTCHK MODEL DATE"
#define XSH_QC_FMTCHK_POLY_DIFFXAVG "ESO QC FMTCHK POLY DIFFXAVG"
#define XSH_QC_FMTCHK_POLY_DIFFXMED "ESO QC FMTCHK POLY DIFFXMED"
#define XSH_QC_FMTCHK_POLY_DIFFXSTD "ESO QC FMTCHK POLY DIFFXSTD"
#define XSH_QC_FMTCHK_POLY_DIFFYAVG "ESO QC FMTCHK POLY DIFFYAVG"
#define XSH_QC_FMTCHK_POLY_DIFFYMED "ESO QC FMTCHK POLY DIFFYMED"
#define XSH_QC_FMTCHK_POLY_DIFFYSTD "ESO QC FMTCHK POLY DIFFYSTD"


#define XSH_QC_MODEL_PREDICT_RESX_MIN "ESO QC MODEL PREDICT RESX_MIN"
#define XSH_QC_MODEL_PREDICT_RESX_MIN_C "Min X Residual X of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESX_MAX "ESO QC MODEL PREDICT RESX_MAX"
#define XSH_QC_MODEL_PREDICT_RESX_MAX_C "Max X Residual X of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESX_MED "ESO QC MODEL PREDICT RESX_MED"
#define XSH_QC_MODEL_PREDICT_RESX_MED_C "Median X Residual X of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESX_AVG "ESO QC MODEL PREDICT RESX_AVG"
#define XSH_QC_MODEL_PREDICT_RESX_AVG_C "Mean X Residual X of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESX_RMS "ESO QC MODEL PREDICT RESX_RMS"
#define XSH_QC_MODEL_PREDICT_RESX_RMS_C "RMS X Residual X of data points to fit positions before model optimization."

#define XSH_QC_MODEL_PREDICT_RESY_MIN "ESO QC MODEL PREDICT RESY_MIN"
#define XSH_QC_MODEL_PREDICT_RESY_MIN_C "Min Y Residual Y of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESY_MAX "ESO QC MODEL PREDICT RESY_MAX"
#define XSH_QC_MODEL_PREDICT_RESY_MAX_C "Max Y Residual Y of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESY_MED "ESO QC MODEL PREDICT RESY_MED"
#define XSH_QC_MODEL_PREDICT_RESY_MED_C "Median Y Residual Y of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESY_AVG "ESO QC MODEL PREDICT RESY_AVG"
#define XSH_QC_MODEL_PREDICT_RESY_AVG_C "Mean Y Residual Y of data points to fit positions before model optimization."
#define XSH_QC_MODEL_PREDICT_RESY_RMS "ESO QC MODEL PREDICT RESY_RMS"
#define XSH_QC_MODEL_PREDICT_RESY_RMS_C "RMS Y Residual Y of data points to fit positions before model optimization."


#define XSH_QC_MODEL_ANNEAL_RESX_MIN "ESO QC MODEL ANNEAL RESX_MIN"
#define XSH_QC_MODEL_ANNEAL_RESX_MIN_C "Min X Residual X of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESX_MAX "ESO QC MODEL ANNEAL RESX_MAX"
#define XSH_QC_MODEL_ANNEAL_RESX_MAX_C  "Max X Residual X of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESX_MED "ESO QC MODEL ANNEAL RESX_MED"
#define XSH_QC_MODEL_ANNEAL_RESX_MED_C  "Median X Residual X of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESX_AVG "ESO QC MODEL ANNEAL RESX_AVG"
#define XSH_QC_MODEL_ANNEAL_RESX_AVG_C  "Mean X Residual X of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESX_RMS "ESO QC MODEL ANNEAL RESX_RMS"

#define XSH_QC_MODEL_ANNEAL_RESY_MIN "ESO QC MODEL ANNEAL RESY_MIN"
#define XSH_QC_MODEL_ANNEAL_RESY_MIN_C "Min Y Residual Y of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESY_MAX "ESO QC MODEL ANNEAL RESY_MAX"
#define XSH_QC_MODEL_ANNEAL_RESY_MAX_C "Max Y Residual Y of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESY_MED "ESO QC MODEL ANNEAL RESY_MED"
#define XSH_QC_MODEL_ANNEAL_RESY_MED_C "Median Y Residual Y of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESY_AVG "ESO QC MODEL ANNEAL RESY_AVG"
#define XSH_QC_MODEL_ANNEAL_RESY_AVG_C "Mean Y Residual Y of data points to fit positions after model optimization."

#define XSH_QC_MODEL_ANNEAL_RESY_RMS "ESO QC MODEL ANNEAL RESY_RMS"



#define XSH_QC_MODEL_WAVECAL_DATE      "ESO QC MODEL WAVECAL DATE"
#define XSH_QC_MODEL_WAVECAL_DATE_C   "Date of the physical model used."

#define XSH_QC_MODEL_WAVECAL_DIFFXAVG  "ESO QC MODEL WAVECAL DIFFXAVG"
#define XSH_QC_MODEL_WAVECAL_DIFFXAVG_C "Average value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list)."


#define XSH_QC_MODEL_WAVECAL_DIFFXMED  "ESO QC MODEL WAVECAL DIFFXMED"
#define XSH_QC_MODEL_WAVECAL_DIFFXMED_C "Median value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list)."

#define XSH_QC_MODEL_WAVECAL_DIFFXSTD  "ESO QC MODEL WAVECAL DIFFXSTD"
#define XSH_QC_MODEL_WAVECAL_DIFFXSTD_C  "Standard Deviation value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list)."


/*
  Keyword
  List (comma separated) of implemented recipes
  List (comma separated) of NOT YET implemented recipes
  List (comma separated) of functions where the QC parameter is calculated
  Comment
  Parameter type
  ARM (if dpecific to one or two arms)
  PRO.CATG (regular expression)
*/
#include <xsh_qc_handling.h>

static const qc_description qc_table[] = {
  {"ESO QC AIRMASS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC CONAD",
   "xsh_linear",
   NULL,
   "xsh_gain",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "GAIN_INFO"
  },
  {"ESO QC GAIN",
   "xsh_linear",
   NULL,
   "xsh_gain",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "GAIN_INFO"
  },
  {"ESO QC GAIN MSE",
   "xsh_linear",
   NULL,
   "xsh_gain",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "GAIN_INFO"
  },
/*
  {"ESO QC RON",
   "xsh_linear",
   NULL,
   "xsh_gain",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "GAIN_INFO"
  },
*/
  {"ESO QC BP-MAP LINi MEAN",
   "xsh_linear",
   NULL,
   "xsh_compute_linearity",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "*BADPIXEL_MAP*"
  },
  {"ESO QC BP-MAP LINi MED",
   "xsh_linear",
   NULL,
   "xsh_compute_linearity",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "BP_MAP_HP"
  },
  {"ESO QC BP-MAP LINi RMS",
   "xsh_linear",
   NULL,
   "xsh_compute_linearity",
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   "BP_MAP_HP"  /* <any>BP_MAP_HP<any> */
  },
  {"ESO QC BP-MAP NBADPIX",
   "xsh_linear",
   NULL,
   "xsh_compute_linearity",
   "Number of bad pixels rejected for non linearity",
   CPL_TYPE_INT,
   NULL,
   "BP_MAP_HP"
  },
  {"ESO QC BP-MAP PICKUP NOISEPIX",
   "xsh_mdark",
   NULL,
   "xsh_compute_noise_map",
   "Number of pixels pixels detected in dark",
   CPL_TYPE_INT,
   "NIR",
   "MASTER_BP_MAP(.*)NIR"  /* <any>MASTER_BP_MAP<any>NIR<any> */
  },
  {XSH_QC_BP_MAP_NTOTAL,
   "xsh_mdark",
   NULL,
   "xsh_create_master_dark_bpmap",
   XSH_QC_BP_MAP_NTOTAL_C,
   CPL_TYPE_INT,
   "NIR",
   NULL
  },
  {XSH_QC_BP_MAP_NFLAGi,
   "xsh_mdark",
   NULL,
   "xsh_create_master_dark_bpmap",
   XSH_QC_BP_MAP_NFLAGi_C,
   CPL_TYPE_INT,
   "NIR",
   NULL
  },
  {XSH_QC_RON_REG1,
   "xsh_mbias,xsh_mdark,xsh_linear",
   NULL,
   "set_masterbias_qc,set_masterdark_qc",
   XSH_QC_RON_REG1_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_RON_REG1_ERR,
   "xsh_mdark",
   NULL,
   "set_masterdark_qc",
   XSH_QC_RON_REG1_ERR_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_RON_REG2,
   "xsh_mbias,xsh_mdark,xsh_linear",
   NULL,
   "set_masterbias_qc,set_masterdark_qc",
   XSH_QC_RON_REG2_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_RON_REG2_ERR,
   "xsh_mdark",
   NULL,
   "set_masterdark_qc",
   XSH_QC_RON_REG2_ERR_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_RON_MASTER,
   "xsh_mbias,xsh_mdark,xsh_linear",
   NULL,
   "set_masterbias_qc,set_masterdark_qc",
   XSH_QC_RON_MASTER_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC RON ERR MASTER",
   "xsh_mdark",
   NULL,
   "set_masterdark_qc",
   "Read Out Noise error on master farme (ADU)",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 
  {"ESO QC DIFFRON",
   "xsh_mbias",
   NULL,
   "set_masterbias_qc,set_masterdark_qc",
   "Read Out Noise value (ADU)",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC DIFFRON ERR",
   "xsh_mbias,xsh_mdark",
   NULL,
   "sset_masterbias_qc,et_masterdark_qc",
   "Read Out Noise error (ADU)",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC NORMRON",
   "xsh_mdark",
   NULL,
   "set_masterdark_qc",
   "Read Out Noise value (ADU) normalized to 1s exposure",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC NORMRON ERR",
   "xsh_mdark",
   NULL,
   "set_masterdark_qc",
   "Read Out Noise error (ADU) normalised to 1s exposure",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_FPN,
   "xsh_mbias,xsh_mdark",
   NULL,
   "xsh_mbias,xsh_mdark",
   XSH_QC_FPN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_FPN_ERR,
   "xsh_mbias,xsh_mdark",
   NULL,
   "xsh_mbias,xsh_mdark",
   XSH_QC_FPN_ERR_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_FPN_MASTER,
   "xsh_mbias,xsh_mdark",
   NULL,
   "xsh_mbias,xsh_mdark",
   XSH_QC_FPN_MASTER_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_FPN_ERR_MASTER,
   "xsh_mbias,xsh_mdark",
   NULL,
   "xsh_mbias,xsh_mdark",
   XSH_QC_FPN_ERR_MASTER_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_NORM_FPN,
   "xsh_mdark",
   NULL,
   "xsh_mdark",
   XSH_QC_NORM_FPN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_NORM_FPN_ERR,
   "xsh_mdark",
   NULL,
   "xsh_mdark",
   XSH_QC_NORM_FPN_ERR_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_CRH_RATE,
   "xsh_linear,xsh_mdark,xsh_mflat,xsh_scired_slit_stare",
   "xsh_absorp,xsh_respon_uvbvis,xsh_respon_visnir,xsh_scired_slit_nod,xsh_scired_slit_offset,xsh_scired_ifu_stare,xsh_scired_ifu_offset",
   "xsh_remove_crh_multiple,xsh_remove_crh_single",
   XSH_QC_CRH_RATE_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC DATAAVG",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC DETLINi MEAN",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC DETLINi MED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC DETLINi RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC DIFF RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
#if 0
  /* Mis de cote en attendant ... */
  {"ESO QC DISP COEFFi",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Dispersion coefficients",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
#endif
  {XSH_QC_FLAT_FPNi,
   NULL,
   NULL,
   NULL,
   XSH_QC_FLAT_FPNi_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_NLINE_FOUND,
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_NLINE_FOUND_C,
   CPL_TYPE_INT,
   NULL,
   "ARC_LINE_LIST|WAVE_TAB"
  },
  {XSH_QC_NLINE_CAT,
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_NLINE_CAT_C,
   CPL_TYPE_INT,
   NULL,
   "ARC_LINE_LIST|WAVE_TAB"
  },
  {XSH_QC_NLINE_CAT_CLEAN,
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_NLINE_CAT_CLEAN_C,
   CPL_TYPE_INT,
   NULL,
   "ARC_LINE_LIST"
  },
  {XSH_QC_NLINE_FOUND_CLEAN,
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_NLINE_FOUND_CLEAN_C,
   CPL_TYPE_INT,
   NULL,
   "ARC_LINE_LIST"
  },
  {"ESO QC WAVECAL FWHMRMS",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Mesured Standard Deviation of FWHM in Y of lines selected",
   CPL_TYPE_DOUBLE,
   NULL,
   "TILT_TAB"
  },
  {"ESO QC GAIN",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC INTAVG",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LAMP NAME",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Type of Lamp used",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC LAMP INTENSITY",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Lamp Intensity",
   CPL_TYPE_STRING,
   NULL,
   NULL
  },
  {"ESO QC LAMPOFF MAX",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LAMPOFF MED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LAMPOFF RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LAMPON MAX",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LAMPON MED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LAMPON RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC LINE RESIDRMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_BIAS_MEDIAN,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_MASTER_BIAS_MEDIAN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_BIAS_RMS,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_MASTER_BIAS_RMS_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_BIAS_MEAN,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_MASTER_BIAS_MEAN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_MEAN,
   "xsh_mbias",
   NULL,
   "xsh_mbias_get_hot_cold_maps",
   XSH_QC_MASTER_MEAN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {XSH_QC_MASTER_RMS,
   "xsh_mbias",
   NULL,
   "xsh_mbias_get_hot_cold_maps",
   XSH_QC_MASTER_RMS_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

 {XSH_QC_COLD_PIX_NUM,
   "xsh_mbias",
   NULL,
   "xsh_mbias_get_hot_cold_maps",
   XSH_QC_COLD_PIX_NUM_C,
   CPL_TYPE_INT,
   NULL,
   NULL
  },

 {XSH_QC_HOT_PIX_NUM,
   "xsh_mbias",
   NULL,
   "xsh_mbias_get_hot_cold_maps",
   XSH_QC_HOT_PIX_NUM_C,
   CPL_TYPE_INT,
   NULL,
   NULL
  },


 {"ESO QC BIAS RANDOM VAL",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_random",
   "Bias value",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS RANDOM RON",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_random",
   "Bias RON value",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

 {"ESO QC BIAS RANDOM PRESCAN MEAN",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias mean value on prescan region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS RANDOM PRESCAN MED",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias median value on prescan region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS RANDOM PRESCAN RON",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias RON value on prescan region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },


 {"ESO QC BIAS RANDOM OVERSCAN MEAN",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias mean value on overscan region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS RANDOM OVERSCAN MED",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias median value on overscan region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS RANDOM OVERSCAN RON",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias RON value on overscan region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

 {"ESO QC BIAS REGION MED",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias median value on region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS REGION VAL",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias mean value on region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS REGION RON",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias RON value on region",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

 {"ESO QC BIAS HISTO VAL",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias value from histogram",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
 {"ESO QC BIAS HISTO RON",
   "xsh_mbias",
   NULL,
   "xsh_my_detmon_ronbias_preoverscan",
   "Bias RON value from histogram",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_DARK_MEDIAN,
   "xsh_mdark",
   NULL,
   "xsh_create_masterdark",
   XSH_QC_MASTER_DARK_MEDIAN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_DARK_RMS,
   "xsh_mdark",
   NULL,
   "xsh_create_masterdark",
   XSH_QC_MASTER_DARK_RMS_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MASTER_DARK_MEAN,
   "xsh_mdark",
   NULL,
   "xsh_create_masterdark",
   XSH_QC_MASTER_DARK_MEAN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MFLATMAX",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC MFLATMIN",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC MFLATRMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK MODEL DATE",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Date of the physical model used.",
   CPL_TYPE_STRING,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK POLY DIFFXAVG",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Average value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK POLY DIFFXMED",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Median value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK POLY DIFFXSTD",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK POLY DIFFYAVG",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Average value of the differences between Y positions in the theoretical map (THE) and fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK POLY DIFFYMED",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Median value of the differences between Y positions in the theoretical map (THE) and fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC FMTCHK POLY DIFFYSTD",
   "xsh_predict,xsh_2dmap",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences between Y positions in the theoretical map (THE) and fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },


  {XSH_QC_MODEL_NDAT,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_NDAT_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {XSH_QC_MODEL_PREDICT_RESX_MIN,
   "xsh_predict",
   NULL,
   "xsh_model_pipe_anneal",
   XSH_QC_MODEL_PREDICT_RESX_MIN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_PREDICT_RESX_MAX,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_PREDICT_RESX_MAX_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL PREDICT RESX_STD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation X Residual X of data points to fit positions before model optimization.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {XSH_QC_MODEL_PREDICT_RESY_MIN,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_PREDICT_RESY_MIN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_PREDICT_RESY_MAX,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_PREDICT_RESY_MAX_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL PREDICT RESY_STD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation Y Residual Y of data points to fit positions before model optimization.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },



  {XSH_QC_MODEL_ANNEAL_RESX_MIN,
   "xsh_predict",
   NULL,
   "xsh_model_pipe_anneal",
   XSH_QC_MODEL_ANNEAL_RESX_MIN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_ANNEAL_RESX_MAX,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_ANNEAL_RESX_MAX_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ANNEAL RESX_STD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation X Residual X of data points to fit positions after model optimization.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {XSH_QC_MODEL_ANNEAL_RESY_MIN,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_ANNEAL_RESY_MIN_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_ANNEAL_RESY_MAX,
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   XSH_QC_MODEL_ANNEAL_RESY_MAX_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ANNEAL RESY_STD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation Y Residual Y of data points to fit positions after model optimization.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },




  {"ESO QC POLY RESX_MIN",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Minimum of the differences between X positions from poly fit and the fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY RESX_MAX",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Maximum of the differences between X positions from poly fit and the fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY RESX_STD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences between X positions from poly fit and the fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC POLY RESY_MIN",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Minimum of the differences between Y positions from poly fit and the fitted positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY RESY_MAX",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Maximum of the differences between Y positions from poly fit and the fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY RESY_STD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences between Y positions from poly fit and the fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },






  {"ESO QC MODEL DIFFXMIN",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Minimum of the differences between X positions from the model optimized on the frame and the fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL DIFFXMAX",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Maximum of the differences between X positions from the model optimized on the frame and the fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL DIFFXSTD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences between X positions from the model optimized on the frame and the fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC MODEL DIFFYMIN",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Minimum of the differences between Y positions from the model optimized on the frame and the fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL DIFFYMAX",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Maximum of the differences between Y positions from the model optimized on the frame and the fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL DIFFYSTD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences between Y positions from the model optimized on the frame and the fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },


  {"ESO QC POLY DIFFXMIN",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Minimum of the differences between the fitted X positions (from the clean arc line list) and the X positions from their polynomial fit",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY DIFFXMAX",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Maximum of the differences between the fitted X positions (from the clean arc line list) and the X positions from their polynomial fit.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY DIFFXSTD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences betweenthe fitted X positions (from the clean arc line list) and the X positions from their polynomial fit.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC POLY DIFFYMIN",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Minimum of the differences between the fitted Y positions (from the clean arc line list) and the Y positions from their polynomial fit",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY DIFFYMAX",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Maximum of the differences between the fitted Y positions (from the clean arc line list) and the Y positions from their polynomial fit.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC POLY DIFFYSTD",
   "xsh_predict",
   NULL,
   "xsh_detect_arclines",
   "Standard deviation of the differences betweenthe fitted Y positions (from the clean arc line list) and the Y positions from their polynomial fit.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC FLUXi MIN",
   "xsh_orderpos,xsh_mflat",
   NULL,
   "xsh_monitor_flux",
   "Minimum of flux on order i.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC FLUXi MAX",
   "xsh_orderpos,xsh_mflat",
   NULL,
   "xsh_monitor_flux",
   "Minimum of flux on order i.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },


  {"ESO QC FLUX16 MIN",
   "xsh_orderpos,xsh_mflat",
   NULL,
   "xsh_monitor_flux",
   "Minimum of flux on order i.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC FLUX16 MAX",
   "xsh_orderpos,xsh_mflat",
   NULL,
   "xsh_monitor_flux",
   "Minimum of flux on order i.",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC MODEL ORDERPOS DATE",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Date of the physical model used.",
   CPL_TYPE_STRING,
   NULL,
   NULL
  },
  {"ESO QC MODEL ORDERPOS DIFFXAVG",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Average value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ORDERPOS DIFFXMED",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Median value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ORDERPOS DIFFXSTD",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Standard Deviation value of the differences between X positions in the theoretical map (THE) and fitted X positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ORDERPOS DIFFYAVG",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Average value of the differences between Y positions in the theoretical map (THE) and fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ORDERPOS DIFFYMED",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Median value of the differences between Y positions in the theoretical map (THE) and fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC MODEL ORDERPOS DIFFYSTD",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum_",
   "Standard Deviation value of the differences between X positions in the theoretical map (THE) and fitted Y positions (from the clean arc line list).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_WAVECAL_DATE,
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   XSH_QC_MODEL_WAVECAL_DATE_C,
   CPL_TYPE_STRING,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_WAVECAL_DIFFXAVG,
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   XSH_QC_MODEL_WAVECAL_DIFFXAVG_C,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_WAVECAL_DIFFXMED,
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   XSH_QC_MODEL_WAVECAL_DIFFXMED_C,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {XSH_QC_MODEL_WAVECAL_DIFFXSTD,
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   XSH_QC_MODEL_WAVECAL_DIFFXSTD_C,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVECAL DIFFYAVG",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Average value of the differences between Y theoretical positions (model or polynomial solution) and fitted Y positions (from the linear fit of the tilt).",
   CPL_TYPE_DOUBLE,
   NULL,
   "SHIFT_TAB|TILT_TAB"
  },
  {"ESO QC WAVECAL DIFFYMED",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Median value of the differences between Y theoretical positions (model or polynomial solution) and fitted Y positions (from the linear fit of the tilt).",
   CPL_TYPE_DOUBLE,
   NULL,
   "SHIFT_TAB|TILT_TAB"
  },
  {"ESO QC WAVECAL DIFFYSTD",
   "xsh_wavecal",
   NULL,
   "xsh_follow_arclines",
   "Standard Deviation value of the differences between Y theoretical positions (model or polynomial solution) and fitted Y positions (from the linear fit of the tilt).",
   CPL_TYPE_DOUBLE,
   NULL,
   "SHIFT_TAB|TILT_TAB"
  },
  {XSH_QC_CRH_NUMBER,
   "xsh_linear,xsh_mdark,xsh_mflat",
   "xsh_absorp,xsh_respon_uvbvis,xsh_respon_visnir,xsh_scired_slit_stare,xsh_scired_slit_nod,xsh_scired_slit_offset,xsh_scired_ifu_stare,xsh_scired_ifu_offset",
   "xsh_remove_crh_multiple,xsh_remove_crh_single",
   XSH_QC_CRH_NUMBER_C,
   CPL_TYPE_INT,
   NULL,
   NULL
  },
  {XSH_QC_CRH_NUMBER_MEAN,
   "xsh_linear,xsh_mdark,xsh_mflat",
   "xsh_absorp,xsh_respon_uvbvis,xsh_respon_visnir,xsh_scired_slit_stare,xsh_scired_slit_nod,xsh_scired_slit_offset,xsh_scired_ifu_stare,xsh_scired_ifu_offset",
   "xsh_remove_crh_multiple",
   "Average number of cosmic ray hits per frame",
   CPL_TYPE_INT,
   NULL,
   NULL
  },
  {"ESO QC NHPIX",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC NLININT",
   "xsh_wavecal",
   NULL,
   NULL,
   "Average intensity of selected lines (at center)",
   CPL_TYPE_DOUBLE,
   NULL,
   "TILT_TAB|SHIFT_TAB"
  },
  {"ESO QC OBJi FWHM",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC ORDER COEFi",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESIDMIN",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Minimum of the residuals in order positions (calculated - guessed).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESIDMAX",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Maximum of the residuals in order positions (calculated - guessed).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESIDAVG",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Mean residual in order positions (calculated - guessed).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESIDRMS",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "RMS of the residuals in order positions (calculated - guessed).",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESELMIN",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Minimum of the residuals in order positions (calculated - guessed).after selection",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESELMAX",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Maximum of the residuals in order positions (calculated - guessed).after selection",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESELAVG",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Mean residual in order positions (calculated - guessed).after selection",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS RESELRMS",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "RMS of the residuals in order positions (calculated - guessed).after selection",
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },

  {"ESO QC ORD ORDERPOS MAX PRED",
   "xsh_orderpos",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS MIN PRED",
   "xsh_orderpos",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS NDET",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Number of detected orders.",
   CPL_TYPE_INT,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS NPOSALL",
   "xsh_orderpos",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS NPOSSEL",
   "xsh_orderpos",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC ORD ORDERPOS NPRED",
   "xsh_orderpos",
   NULL,
   "xsh_detect_continuum",
   "Number of orders predicted by the physical model.",
   CPL_TYPE_INT,
   NULL,
   NULL
  },
  {"ESO QC PIXLINi MEAN",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC PIXLINi MED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC PIXLINi RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC REC NS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC REFi DATAAVG",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC REFi DATAMED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC REFi DATARMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC REF TEMP",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC RESOLAVG",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC RESOLMED",
   "xsh_wavecal",
   NULL,
   NULL,
   "Measured median resolving power of lines selected",
   CPL_TYPE_DOUBLE,
   NULL,
   "TILT_TAB"
  },
  {"ESO QC RESOLRMS",
   "xsh_wavecal",
   NULL,
   NULL,
   "Measured RMS of resolving power of lines selected",
   CPL_TYPE_DOUBLE,
   NULL,
   "TILT_TAB"
  },
  {"ESO QC RONi",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {XSH_QC_RON_MASTER,
   NULL,
   NULL,
   NULL,
   XSH_QC_RON_MASTER_C,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC RON RAW",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC SHFTYAVG",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC SHIFTX RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC SHIFTY RMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC SKYMOD SUMS",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {XSH_QC_STRUCT_X_REG1,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_STRUCT_X_REG1_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_STRUCT_Y_REG1,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_STRUCT_Y_REG1_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_STRUCT_X_REG2,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_STRUCT_X_REG2_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {XSH_QC_STRUCT_Y_REG2,
   "xsh_mbias",
   NULL,
   "xsh_create_masterbias",
   XSH_QC_STRUCT_Y_REG2_C,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC WAVE",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVECAL CATLINE",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INT,
   NULL,
   "TILT_TAB|SHIFT_TAB"
  },
  {"ESO QC WAVECAL FOUNDLINE",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INT,
   NULL,
   "TILT_TAB,SHIFT_TAB"
  },
  {"ESO QC WAVECAL FWHMAVG",
   "xsh_wavecal",
   NULL,
   NULL,
   "Average FWHM in Y direction of detected lines",
   CPL_TYPE_DOUBLE,
   NULL,
   "TILT_TAB"
  },
  {"ESO QC WAVECAL MATCHLINE",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INT,
   NULL,
   "TILT_TAB,SHIFT_TAB"
  },
  {"ESO QC WAVECAL SPACEFIT",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVECAL WAVEFIT",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVE COEFi",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVE FMTCHK MAXPRED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC WAVE FMTCHK MINPRED",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC WAVEi",
   NULL,
   NULL,
   NULL,
   NULL,
   CPL_TYPE_DOUBLE,
   NULL,
   NULL
  },
  {"ESO QC WAVE INTAVG",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVE INTMAX",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVE INTRMS",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVEi SHIFT",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVE NALL",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {"ESO QC WAVE OFF",
   "xsh_wavecal",
   NULL,
   NULL,
   NULL,
   CPL_TYPE_INVALID,
   NULL,
   NULL
  },
  {NULL, NULL, NULL, NULL, NULL, CPL_TYPE_INVALID,
   NULL,
   NULL
  }
} ;

#endif
