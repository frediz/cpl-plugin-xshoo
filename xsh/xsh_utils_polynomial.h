/*
 * This file is part of the ESO X-Shooter Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-09 10:05:37 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifndef XSH_UTILS_POLYNOMIAL_H
#define XSH_UTILS_POLYNOMIAL_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

//#include <xsh_propertylist.h>
#include <cpl.h>
#include <xsh_cpl_size.h>
/*-----------------------------------------------------------------------------
                                   Typedefs
 -----------------------------------------------------------------------------*/

typedef struct _polynomial polynomial ;

/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/


polynomial *xsh_polynomial_new(const cpl_polynomial *pol);
polynomial *xsh_polynomial_new_zero(int dim);
polynomial *xsh_polynomial_duplicate(const polynomial *p);


polynomial *xsh_polynomial_convert_from_table(cpl_table *t);
polynomial *xsh_polynomial_collapse(const polynomial *p, int varno, double value);
polynomial * xsh_polynomial_fit_1d(const cpl_vector    *   x_pos,
                    const cpl_vector    *   values,
                    const cpl_vector    *   sigmas,
                    int                     poly_deg,
                    double              *   mse);
polynomial *xsh_polynomial_fit_2d(const cpl_bivector     *  xy_pos,
                   const cpl_vector       *  values,
                   const cpl_vector       *  sigmas,
                   int                       poly_deg1,
                   int                       poly_deg2,
                   double                 *  mse,
                   double                 *  red_chisq,
                   polynomial             ** variance);

polynomial *xsh_polynomial_add_2d(const polynomial *p1, const polynomial *p2);

int xsh_polynomial_get_degree(const polynomial *p);

void xsh_polynomial_delete(polynomial **p);
void xsh_polynomial_delete_const(const polynomial **p);

cpl_table     *xsh_polynomial_convert_to_table(const polynomial *p);
int            xsh_polynomial_get_dimension(const polynomial *p);
void           xsh_polynomial_dump(const polynomial *p, FILE *stream);
cpl_error_code xsh_polynomial_shift(polynomial *p, int varno, double shift);
cpl_error_code xsh_polynomial_rescale(polynomial *p, int varno, double scale);
double         xsh_polynomial_get_coeff_1d(const polynomial *p, int degree);
double         xsh_polynomial_get_coeff_2d(const polynomial *p,
                        int degree1, int degree2);
double         xsh_polynomial_evaluate_1d(const polynomial *p, double x);
double         xsh_polynomial_evaluate_2d(const polynomial *p, double x1, double x2);
double         xsh_polynomial_solve_1d(const polynomial *p, double value, 
                    double guess, int multiplicity);
double         xsh_polynomial_solve_2d(const polynomial *p, double value, 
                    double guess, int multiplicity, 
                    int varno, double x_value);
double         xsh_polynomial_derivative_1d(const polynomial *p, double x);
double         xsh_polynomial_derivative_2d(const polynomial *p, double x1,
                         double x2, int varno);
cpl_error_code xsh_polynomial_derivative(polynomial *p, int varno);

#endif
