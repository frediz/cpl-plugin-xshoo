/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is/ free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2009-12-30 15:03:39 $
 * $$
*/
#ifndef XSH_DATA_ORDER_RESID_TAB_H
#define XSH_DATA_ORDER_RESID_TAB_H

#include <cpl.h>
#include <xsh_data_order.h>

#define XSH_RESID_ORDER_TABLE_NB_COL 4

#define XSH_RESID_ORDER_TABLE_COLNAME_ORDER "Order"
#define XSH_RESID_ORDER_TABLE_UNIT_ORDER "none"

#define XSH_RESID_ORDER_TABLE_COLNAME_POSX "X"
#define XSH_RESID_ORDER_TABLE_UNIT_POSX "pixel"
#define XSH_RESID_ORDER_TABLE_COLNAME_POSY "Y"
#define XSH_RESID_ORDER_TABLE_UNIT_POSY "pixel"

#define XSH_RESID_ORDER_TABLE_COLNAME_RESX "RESX"
#define XSH_RESID_ORDER_TABLE_UNIT_RESX "pixel"

#define XSH_RESID_ORDER_TABLE_COLNAME_POLX "POLX"
#define XSH_RESID_ORDER_TABLE_UNIT_POLX "pixel"

/* get property  PROPERTY of type TYPE from OBJECT */
#define PROPERTY_GET( OBJECT, PROPERTY, TYPE, DEFAULT)\
TYPE OBJECT##_get_##PROPERTY( OBJECT* obj)\
{\
  TYPE result = DEFAULT;\
\
  XSH_ASSURE_NOT_NULL( obj);\
  result = obj->PROPERTY;\
\
  cleanup:\
    return result;\
}

/* get property  PROPERTY of type TYPE from resid_tab */
#define RESID_ORDER_PROPERTY_GET( PROPERTY, TYPE, DEFAULT)\
  PROPERTY_GET( xsh_resid_tab, PROPERTY, TYPE, DEFAULT)

typedef struct{
  int size ;
  double residmin ;
  double residmax ;
  double residavg ;
  double residrms ;

  double residmin_sel ;
  double residmax_sel ;
  double residavg_sel ;
  double residrms_sel ;

  /* order */
  int * order;
  double * pos_x;		/**< Fitted central X lines position*/
  double * pos_y;		/**< Y position */
  double * res_x;		/**< X diff found_center - fitted_polynomial */
  double * pol_x;		/**< fitted_polynomial X position */
  cpl_propertylist* header;
}xsh_resid_order_tab;

xsh_resid_order_tab * xsh_resid_order_create(int size, int * orders,
					     double * posx, double * posy,
					     double * resx, double* polx ) ;

xsh_resid_order_tab * xsh_resid_order_load( cpl_frame* resid_tab_frame);

void xsh_resid_order_free( xsh_resid_order_tab ** resid);


cpl_frame * xsh_resid_order_save( xsh_resid_order_tab * resid,
				  const char* filename,
				  xsh_instrument * instrument, 
                                  ORDERPOS_QC_PARAM* ord_qc_param,
				  const char* tag);
cpl_table * 
xsh_resid_order_2tab( xsh_resid_order_tab * resid,
				  xsh_instrument * instrument, 
		      ORDERPOS_QC_PARAM* ord_qc_param);

#endif  /* XSH_DATA_RESID_TAB_H */
