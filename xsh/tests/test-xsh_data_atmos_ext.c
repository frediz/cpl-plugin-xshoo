/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2009-09-27 10:35:23 $
 * $Revision: 1.2 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_data_atmos_ext     Test Loading atmosp extinction Frame
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_atmos_ext.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <xsh_utils_table.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_DATA_ATMOS_EXT"

#define SYNTAX "Test the atmos Extinction table\n"\
  "usage : test_xsh_data_atmos_ext atmos_ext \n"\
  "atmos_ext_table   => the Atmos Ext table FITS file\n"


/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief
    Unit test of xsh_data_atmos_ext
  @return
    0 if success

*/
/*--------------------------------------------------------------------------*/
int main(int argc, char** argv)
{
  int ret = 0;

  char * ext_tab_name = NULL;
  cpl_frame * ext_tab_frame = NULL;
  xsh_atmos_ext_list * ext_list = NULL ;
  double * plambda = NULL, *pK = NULL ;
  int ext_tab_size, i ;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if ( optind < argc ) {
    ext_tab_name = argv[optind] ;
  }
  else{
    printf(SYNTAX);
    return 0;
  }

  /* Create frames */
  XSH_ASSURE_NOT_NULL( ext_tab_name);
  ext_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( ext_tab_frame, ext_tab_name) ;
  cpl_frame_set_level( ext_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( ext_tab_frame, CPL_FRAME_GROUP_CALIB );

  check( ext_list = xsh_atmos_ext_list_load( ext_tab_frame ) ) ;
  ext_tab_size = ext_list->size ;
  xsh_msg( "Atmos Ext Table size: %d", ext_tab_size ) ;

  /* Dump star data */
  plambda = ext_list->lambda ;
  pK = ext_list->K ;

  for ( i = 0 ; i < ext_tab_size ; i++, plambda++, pK++ ) {
    xsh_msg( "   %3d: %lf %lf", i, *plambda, *pK ) ;
  } 
  /* dump to file */
  {
    FILE * fout ;

    fout = fopen( "atmos_ext.dat", "w" ) ;
    plambda = ext_list->lambda ;
    pK = ext_list->K ;
    for ( i = 0 ; i < ext_tab_size ; i++, plambda++, pK++ )
      fprintf( fout, "%lf %lf\n", *plambda, *pK ) ;
    fclose( fout ) ;
  }

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    xsh_atmos_ext_list_free( &ext_list);

    return ret ;
}

/**@}*/
