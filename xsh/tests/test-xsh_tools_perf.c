/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_TOOLS_PERF"

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
void test_tech_eval_perf(int indice, double val);

int main(void)
{
  int ret = 0;

  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;
  check( test_tech_eval_perf(500, 0.264879));
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    TEST_END();
    return ret;
}


void test_tech_eval_perf(int indice, double val){
  struct timeval start, end;
  double diff_time;
  cpl_vector* result_a = NULL;
  cpl_vector* result_b = NULL;
  int i=0;

  xsh_msg("Test Tchebitchev polynomial eval with indice %d and val %f", 
    indice, val);
  xsh_msg("Acos use");
  gettimeofday(&start, NULL);
  result_a = cpl_vector_new( indice+1);
  for(i=0; i<= indice; i++){
    double sval = cos(i*acos(val));
    cpl_vector_set( result_a, i, sval);
  }
  gettimeofday(&end, NULL);
  diff_time = (end.tv_sec - start.tv_sec)*1000000.0 + (end.tv_usec - start.tv_usec);
  xsh_msg("Time %.2lf micro seconds\n", diff_time);
  
  xsh_msg("Iterative use");
  gettimeofday(&start, NULL);
  check( result_b =  xsh_tools_tchebitchev_poly_eval( indice, val));
  gettimeofday(&end, NULL);
  diff_time = (end.tv_sec - start.tv_sec)*1000000.0 + (end.tv_usec - start.tv_usec);
  xsh_msg("Time %.2lf micro seconds\n", diff_time);
  for(i=0; i< indice+1; i++){
    double a, b;
    a = cpl_vector_get( result_a,i);
    b = cpl_vector_get( result_b,i);
    XSH_ASSURE_NOT_ILLEGAL( fabs(a-b) < 0.0000001 );
  }
  cleanup:
    xsh_free_vector( &result_a);
    xsh_free_vector( &result_b);
    return;
}
/**@}*/
