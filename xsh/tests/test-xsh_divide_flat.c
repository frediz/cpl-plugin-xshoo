/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: rhaigron $
 * $Date: 2010-04-20 11:48:53 $
 * $Revision: 1.8 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_divide_flat  Test the division by the flat frame 
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>
#include <xsh_badpixelmap.h>
#include <cpl.h>
#include <math.h>
#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_DIVIDE_FLAT"


enum {
DEBUG_OPT, HELP_OPT
};

static struct option long_options[] = {
  {"debug", required_argument, 0, DEBUG_OPT},
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};


static void Help( void )
{
  puts( "Unitary test of xsh_divide_flat");
  puts( "Usage: test_xsh_divide_flat [options] <input_files>");

  puts( "Options" ) ;
  puts( " --debug=<n>        : Level of debug LOW | MEDIUM | HIGH [MEDIUM]" );
  puts( " --help             : What you see" ) ;
  puts( "\nInput Files" ) ;
  puts( "The input files argument MUST be in this order:" ) ;
  puts( " 1. Science frame in PRE format" ) ;
  puts( " 2. SOF [MASTER_FLAT]");
  TEST_END();
  exit(0);
}

static void HandleOptions( int argc, char **argv)
{
  int opt ;
  int option_index = 0;
  while (( opt = getopt_long (argc, argv, "debug:help",
    long_options, &option_index)) != EOF ){

    switch ( opt ) {
    case DEBUG_OPT:
      if ( strcmp( optarg, "LOW")==0){
        xsh_debug_level_set( XSH_DEBUG_LEVEL_LOW);
      }
      else if ( strcmp( optarg, "HIGH")==0){
        xsh_debug_level_set( XSH_DEBUG_LEVEL_HIGH);
      }
      break;
    case HELP_OPT:
      Help();
      break;
    default:
      break;
    }
  }
  return;
}
/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_divide_flat
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  xsh_instrument* instrument = NULL;
  cpl_frame* result = NULL;
  char *sci_name = NULL;
  char *sof_name = NULL;
  cpl_frame *sci_frame = NULL;
  cpl_frame *flat_frame = NULL;
  cpl_frameset *set = NULL;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);


  HandleOptions( argc, argv);

  if ( (argc-optind) >= 2 ) {
    sci_name = argv[optind];
    sof_name = argv[optind+1];

    /* Create frameset from sof */
    check( set = sof_to_frameset( sof_name));

    /* Validate frame set */
    check( instrument = xsh_dfs_set_groups( set));
    check( flat_frame = xsh_find_master_flat( set, instrument));
    TESTS_XSH_FRAME_CREATE( sci_frame, "OBJECT_SLIT_STARE_arm", 
      sci_name);
  }
  else{
    Help();
  }

  /* Create instrument structure and fill */
  xsh_msg("PRE         : %s", cpl_frame_get_filename( sci_frame));
  xsh_msg("MASTER_FLAT : %s", cpl_frame_get_filename( flat_frame));

  xsh_instrument_set_recipe_id( instrument, "xsh_mdark");

  check( result = xsh_divide_flat( sci_frame, flat_frame, "DIVIDE_FLAT",
    instrument)); 
  xsh_msg("Save DIVIDE_FLAT.fits frame"); 

  cleanup:
    xsh_free_frame( &sci_frame);
    xsh_free_frameset( &set);
    xsh_free_frame( &result);
    xsh_instrument_free( &instrument);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    }
    TEST_END();
    return ret;
}

/**@}*/
