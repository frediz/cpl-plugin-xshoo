/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2012-04-27 17:35:03 $
 * $Revision: 1.3 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_remove_crh_single  Test Remove Crh Multi function(s)
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_spectrum.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>
#include <xsh_parameters.h>
#include <xsh_badpixelmap.h>
#include <xsh_utils_ifu.h>
#include <cpl.h>
#include <math.h>

#include <string.h>
#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_ATROUS"

enum {
  NSCALES_OPT, HELP_OPT
} ;

static struct option long_options[] = {
  {"nscales", required_argument, 0,  NSCALES_OPT},
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};

static void Help( void )
{
  puts( "Unitary test of xsh_atrous");
  puts( "Usage: test_xsh_atrous [options] DATA_FILE");

  puts( "Options" ) ;
  puts( " --help             : What you see" ) ;
  puts( " --nscales=         : Number os scales");
  puts( "\nInput Files" ) ;
  puts( "DATA_FILE           :ASCII x,y file y column is use for decomposition");
  TEST_END();
}


static void HandleOptions( int argc, char **argv, 
  int *nscales)
{
  int opt ;
  int option_index = 0;

  while (( opt = getopt_long (argc, argv, 
    "nscales",
    long_options, &option_index)) != EOF ){
  
    switch ( opt ) {
    case  NSCALES_OPT:
      *nscales = atoi(optarg);
      break ; 
    default: 
      Help(); exit(-1);
    }
  }
  return;
}


int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  int nscales = 5;
  const char *file_name = NULL;
  FILE* file = NULL;
  cpl_vector *data_vect = NULL;
  char line[200];
  double xpos[10000];
  double ypos[10000];
  int i, k, size=0;
  int max = 10000;
  cpl_matrix *result = NULL;
  char res_name[256];

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  /* Analyse parameters */
  HandleOptions( argc, argv, &nscales);

  if ( (argc - optind) > 0 ) {
    file_name = argv[optind];
  }
  else {
    Help();
    exit( 0);
  }
  
  xsh_msg("---Input Files");
  xsh_msg("File    : %s ", file_name);
  xsh_msg("---Options");
  xsh_msg("NSCALES : %d ", nscales);

  /* READ data */
  file = fopen( file_name, "r");
  if (file != NULL){
    size=0;
    while ( size < max && fgets( line, 200, file)){
      char col1[20];
      char col2[20];

      if ( line[0] != '#'){
        /* Note: update the string format (the %__s) if col1(2) changes size.
         * Remember: field width must equal 1 less than sizeof(col1(2)). */
        sscanf(line,"%19s %19s", col1, col2);
        xpos[size] = atof(col1);
        ypos[size] = atof(col2);
        size++; 
      }
    }
  }
  if (file != NULL){
    fclose(file);
  }  

  check( data_vect = cpl_vector_wrap( size, ypos));
 
  check ( result = xsh_atrous( data_vect, nscales));
  
  sprintf( res_name, "%s_ATROUS.dat", file_name);

  file = fopen( res_name, "w+");
  fprintf( file, "#x y");
  for(k=0; k< nscales+1; k++){
    fprintf( file, " n%d", k);
  }
  fprintf( file, "\n");
 
  for( i=0; i< size; i++){
    fprintf( file, "%f %f", xpos[i], ypos[i]);
    for(k=0; k< nscales+1; k++){
      double val;

      val = cpl_matrix_get( result, k, i);
      fprintf( file, " %f", val);
    }
    fprintf( file, "\n");
  }
  
  fclose(file);

  
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    }
    xsh_free_matrix( &result);
    xsh_unwrap_vector( &data_vect);
    TEST_END();
    return ret ;
}

/**@}*/
