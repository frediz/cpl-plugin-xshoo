/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: rhaigron $
 * $Date: 2010-04-20 13:20:09 $
 * $Revision: 1.5 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_pix_convention Test Flux Conservation 
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <cpl.h>
#include <xsh_data_instrument.h>
#include <xsh_pfits.h>
#include <xsh_msg.h>
#include <xsh_utils.h>
#include <xsh_data_order.h>
#include <tests.h>
#include <math.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_PIX_CONVENTION"
/*---------------------------------------------------------------------------
                         Functions prototypes
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of XSH_BIN_CONVENTION
  @return   0 if tests passed successfully

  Generate a small image to verify pixel conventions
 */
/*--------------------------------------------------------------------------*/


int main(void)
{
   cpl_image* small=NULL;
   int i=0;
   int sx=5;
   int sy=5;
   float* pi=NULL;
   cpl_propertylist* plist=NULL;
   int ret=0;

   TESTS_INIT_WORKSPACE(MODULE_ID);
   TESTS_INIT(MODULE_ID);
   xsh_msg("generate image");

   small=cpl_image_new(sx,sy,CPL_TYPE_FLOAT);
   pi=cpl_image_get_data_float(small);

   for(i=0;i<sx*sy;i++){
      pi[i]=i+1;
   }
   check(plist=cpl_propertylist_new());
   cpl_propertylist_append_double(plist,XSH_CRPIX1,1.);
   cpl_propertylist_append_double(plist,XSH_CRPIX2,1.);
   cpl_propertylist_append_double(plist,XSH_CRVAL1,1.);
   cpl_propertylist_append_double(plist,XSH_CRVAL2,1.);


   cpl_image_save(small,"small.fits", CPL_BPP_IEEE_FLOAT,plist,
                  CPL_IO_DEFAULT);
  cleanup:
   xsh_free_image(&small);
   xsh_free_propertylist(&plist);

   if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
   }
   TESTS_CLEAN_WORKSPACE(MODULE_ID);
   TEST_END();
   return ret;
}

/**@}*/
