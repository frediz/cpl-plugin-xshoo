/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-06-13 15:08:52 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_pre.h>
#include <xsh_fit.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <xsh_cpl_size.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_DETECT_LINE_POS2"

static cpl_error_code
xsh_add_fits_key_min_set(cpl_propertylist* plist)
{
  cpl_propertylist_append_string(plist,XSH_DPR_TYPE,"TEST");
  cpl_propertylist_append_double(plist,XSH_EXPTIME,10.);
  cpl_propertylist_append_double(plist,XSH_RON,1.);
  cpl_propertylist_append_double(plist,XSH_CONAD,1.);
  cpl_propertylist_append_double(plist,XSH_DET_GAIN,1.);  
  cpl_propertylist_append_int(plist,XSH_WIN_BINX,1);
  cpl_propertylist_append_int(plist,XSH_WIN_BINY,1);
  cpl_propertylist_append_double(plist,XSH_PSZX,15.);
  cpl_propertylist_append_double(plist,XSH_PSZY,15.);
  //Only for NIR:
  cpl_propertylist_append_double(plist,XSH_DET_PXSPACE,1.800e-05);
  cpl_propertylist_append_int(plist,XSH_CHIP_NY,2048);


   return cpl_error_get_code();

}

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main()
{
  int ret = 0;
  int sx=4096;
  int sy=4096;

  //char *image_name = NULL;
  cpl_image* image = NULL;
  cpl_image* gauss = NULL;
  cpl_image* img_raw = NULL;
  cpl_image* img_err = NULL;
  cpl_image* img_bias = NULL;
 


  double min_noise=-10;
  double max_noise=10;
  double mean_level=100;

  double gauss_a=1.e5;
  double gauss_sx=4;
  double gauss_sy=4;
  //const int dim=2;
  cpl_size xpos=sx/2;
  cpl_size ypos=sy/2;
  int size=1+2*(gauss_sx+gauss_sy);

  double norm=0;
  double cen_x=0;
  double cen_y=0;
  double sig_x=0;
  double sig_y=0;
  double fwhm_x=0;
  double fwhm_y=0;
  cpl_frame* frm_raw=NULL;
  cpl_frame* frm_bias=NULL;
  xsh_instrument* instrument=NULL;
  const char* name_bias="bias.fits";
  const char* name_raw="raw.fits";
  xsh_pre* pre = NULL;
  cpl_propertylist* plist=NULL;

  TESTS_INIT_WORKSPACE(MODULE_ID);
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  instrument=xsh_instrument_new();
  xsh_instrument_set_arm(instrument,XSH_ARM_UVB);

  check(img_bias=cpl_image_new(sx, sy,CPL_TYPE_FLOAT));
  check(cpl_image_fill_noise_uniform(img_bias,min_noise,max_noise));
  check(cpl_image_add_scalar(img_bias,mean_level));
  check(img_raw=cpl_image_duplicate(img_bias));

  plist=cpl_propertylist_new();
  xsh_add_fits_key_min_set(plist);
 

  check(cpl_image_save(img_bias,name_bias,
                       CPL_BPP_IEEE_FLOAT,plist,CPL_IO_DEFAULT));

  xsh_free_propertylist(&plist);

  check(frm_bias=xsh_frame_product(name_bias,"BIAS",
                                   CPL_FRAME_TYPE_IMAGE,
                                   CPL_FRAME_GROUP_RAW,
                                   CPL_FRAME_LEVEL_FINAL));

  
  check(gauss=cpl_image_new(sx,sy,CPL_TYPE_FLOAT));
  check(cpl_image_fill_gaussian(gauss,xpos,ypos,gauss_a,gauss_sx,gauss_sy));
  check(cpl_image_add(img_raw,gauss));

  plist=cpl_propertylist_new();
  xsh_add_fits_key_min_set(plist);

  check(cpl_image_save(img_raw,name_raw,
                       CPL_BPP_IEEE_FLOAT,plist,CPL_IO_DEFAULT));

  check(frm_raw=xsh_frame_product(name_raw,"RAW",
                                  CPL_FRAME_TYPE_IMAGE,
                                  CPL_FRAME_GROUP_RAW,
                                  CPL_FRAME_LEVEL_FINAL));

  xsh_free_propertylist(&plist);

  xsh_msg("Predicted Pos: [%" CPL_SIZE_FORMAT ",%" CPL_SIZE_FORMAT "], Amp: %f Sigma: [%f,%f]",
          xpos,ypos,gauss_a,gauss_sx,gauss_sy);
 
  xsh_msg("Fit box params: pos=[%" CPL_SIZE_FORMAT ",%" CPL_SIZE_FORMAT "] size=%d",xpos,ypos,size);
  img_err=cpl_image_duplicate(img_raw);
  cpl_image_power(img_err,0.5);
  int i=0;
  cpl_array* parameters=cpl_array_new(7, CPL_TYPE_DOUBLE);
  cpl_array* err_params=cpl_array_new(7, CPL_TYPE_DOUBLE);
  cpl_array* fit_params=cpl_array_new(7, CPL_TYPE_INT);

  /*All parameter should be fitted*/
  for (i = 0; i < 7; i++)
     cpl_array_set(fit_params, i, 1);

  double rms=0;
  double red_chisq=0;
  cpl_matrix* covariance=NULL;
  cpl_matrix* phys_cov=NULL;
  double major=0;
  double minor=0;
  double angle=0;
  const char  *p[7] = { /* Parameter names */
     "Background       ",
     "Normalisation    ",
     "Correlation      ",
     "Center position x",
     "Center position y",
     "Sigma x          ",
     "Sigma y          "};



  check(cpl_fit_image_gaussian(img_raw, img_err, xpos, ypos,size,size,
                               parameters,err_params,fit_params,
                               &rms,&red_chisq,&covariance,
                               &major,&minor,&angle,&phys_cov));
/*
                               &norm, &cen_x, &cen_y,
                               &sig_x, &sig_y, &fwhm_x, &fwhm_y));
*/


  for (i = 0; i < 7; i++){
     cpl_msg_info(cpl_func,"%s: %f",
                  p[i], cpl_array_get(parameters,i,NULL));
  }

  xsh_msg("G Results: rms: %f red_chisq: %f major: %f minor: %f angle: %f",
          rms,red_chisq,major,minor,angle);

  cen_x=cpl_array_get(parameters,3,NULL);
  cen_y=cpl_array_get(parameters,4,NULL);
  sig_x=cpl_array_get(parameters,5,NULL);
  sig_y=cpl_array_get(parameters,6,NULL);

  xsh_msg("G Results: [%f,%f], Amp: %f Sigma: [%f,%f] FWHM: [%f,%f]",
          cen_x,cen_y,norm,sig_x,sig_y,fwhm_x,fwhm_y);

  check(xsh_image_find_barycenter(img_raw,xpos,ypos,size,&norm,&cen_x,&cen_y, 
                                  &sig_x, &sig_y, &fwhm_x, &fwhm_y));

  xsh_msg("B Measured Pos: [%f,%f], Amp: %f Sigma: [%f,%f] FWHM: [%f,%f]",
          cen_x,cen_y,norm,sig_x,sig_y,fwhm_x,fwhm_y);


  check(pre=xsh_pre_create(frm_raw,NULL,img_bias,instrument,0,CPL_FALSE));

  check(cpl_image_save(pre->data,"pre_ima_raw.fits",
                       CPL_BPP_IEEE_FLOAT,plist,CPL_IO_DEFAULT));

  check(cpl_image_get_maxpos(pre->data,&xpos,&ypos));

  xsh_msg("Pos Max: [%" CPL_SIZE_FORMAT ",%" CPL_SIZE_FORMAT "]",xpos,ypos);

  check(cpl_image_fit_gaussian(pre->data, xpos, ypos,size,&norm, &cen_x, &cen_y, 
                               &sig_x, &sig_y, &fwhm_x, &fwhm_y));

  xsh_msg("G Measured Pos: [%f,%f], Amp: %f Sigma: [%f,%f] FWHM: [%f,%f]",
          cen_x,cen_y,norm,sig_x,sig_y,fwhm_x,fwhm_y);



  check(xsh_image_find_barycenter(pre->data,xpos,ypos,size,&norm,&cen_x,&cen_y, 
                                  &sig_x, &sig_y, &fwhm_x, &fwhm_y));

  xsh_msg("B Measured Pos: [%f,%f], Amp: %f Sigma: [%f,%f] FWHM: [%f,%f]",
          cen_x,cen_y,norm,sig_x,sig_y,fwhm_x,fwhm_y);
 
  cleanup:

  cpl_array_delete(parameters);
  cpl_array_delete(err_params);
  cpl_array_delete(fit_params);
  cpl_matrix_delete(covariance);
  cpl_matrix_delete(phys_cov);
  xsh_pre_free(&pre);
  xsh_free_image(&img_err);
  xsh_free_image(&image);
  xsh_free_image(&gauss);
  xsh_free_image(&img_raw);
  xsh_free_image(&img_bias);
  xsh_free_frame(&frm_raw);
  xsh_free_frame(&frm_bias);
  xsh_free_propertylist(&plist);
  xsh_instrument_free(&instrument);

  TESTS_CLEAN_WORKSPACE(MODULE_ID);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    return ret;
}

/**@}*/
