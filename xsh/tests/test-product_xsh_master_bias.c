/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup product_tests Tests the recipes products
 *
 * @defgroup test_product_xsh_master_bias Test the master bias
 * @ingroup product_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <string.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "PRODUCT_XSH_MASTER_BIAS"
#define SYNTAX "Test the MASTER_BIAS\n"\
  "use : ./test-product_xsh_master_bias MASTER_BIAS\n"\
  "MASTER_BIAS   => the master bias frame\n"
/*---------------------------------------------------------------------------
                         Functions prototypes
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
  @brief
    Test if the input file is a valid MASTER_BIAS product
  @return
    0 if success

 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int res = 0;
  char* master_bias_name = NULL;
  const char* master_bias_pcatg = NULL;
  cpl_propertylist* master_bias_header = NULL;
  cpl_frame* master_bias_frame = NULL;
  xsh_pre* master_bias_pre = NULL;
  xsh_instrument* instrument = NULL;
  double mean =0.0, median=0.0, stdev=0.0, ron=0.0;
  double structx=0.0, structy=0.0;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    master_bias_name = argv[1];
  }
  else{
    printf(SYNTAX);
    return 0;
  }

  /* Read the FITS header to define the arm */
  XSH_ASSURE_NOT_NULL( master_bias_name);
  check( master_bias_header = cpl_propertylist_load( master_bias_name, 0 ));
  check( master_bias_pcatg = xsh_pfits_get_pcatg( master_bias_header));

  /* Create valid instrument */
  instrument = xsh_instrument_new();
  master_bias_frame = cpl_frame_new();
  cpl_frame_set_filename( master_bias_frame, master_bias_name);  

  if ( strstr(master_bias_pcatg, "MASTER_BIAS_UVB") != NULL){
    xsh_instrument_set_arm( instrument, XSH_ARM_UVB);
    cpl_frame_set_tag( master_bias_frame, "MASTER_BIAS_UVB");
  }
  else if ( strstr(master_bias_pcatg, "MASTER_BIAS_VIS") != NULL){
    xsh_instrument_set_arm( instrument, XSH_ARM_VIS);
    cpl_frame_set_tag( master_bias_frame, "MASTER_BIAS_VIS");
  }
  else {
    xsh_msg( "Invalid pcatg %s", master_bias_pcatg);
    XSH_ASSURE_NOT_ILLEGAL(1==0);
  }

  /* Create a pre */
  check( master_bias_pre = xsh_pre_load( master_bias_frame, instrument));

  /* Verify header */  
  check( mean = xsh_pfits_get_qc_mbiasavg ( master_bias_pre->data_header));
  check( median = xsh_pfits_get_qc_mbiasmed( master_bias_pre->data_header));
  check( stdev = xsh_pfits_get_qc_mbiasrms ( master_bias_pre->data_header));
  check( ron = xsh_pfits_get_qc_ron( master_bias_pre->data_header));
  check( structx = xsh_pfits_get_qc_structx( master_bias_pre->data_header));
  check( structy = xsh_pfits_get_qc_structy( master_bias_pre->data_header));

  xsh_msg(" Given ron %f Compute ron %f",master_bias_pre->ron, ron);
  xsh_msg(" structx %f structy %f",structx, structy);
  xsh_msg(" stdev %f median %f mean %f",stdev, median, mean);
  xsh_msg(" All is OK"); 
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      res = 1;
    }
  xsh_pre_free( &master_bias_pre);  
  xsh_free_frame( &master_bias_frame);
  xsh_instrument_free(&instrument);
  return res;
}

/**@}*/
