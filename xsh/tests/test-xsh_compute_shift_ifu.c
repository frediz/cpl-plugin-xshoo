/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.3 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_localize_ifu
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_spectrum.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>
#include <xsh_parameters.h>
#include <xsh_badpixelmap.h>
#include <xsh_utils_ifu.h>
#include <cpl.h>
#include <math.h>

#include <string.h>
#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_COMPUTE_SHIFT_IFU"

enum {
  WAVEREF_OPT, WAVEREF_HSIZE_OPT, HELP_OPT
} ;

static struct option long_options[] = {
  {"wave-ref", required_argument, 0,  WAVEREF_OPT},
  {"wave-ref-hsize", required_argument, 0,  WAVEREF_HSIZE_OPT},
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};

static void Help( void )
{
  puts( "Unitary test of xsh_compute_shift_ifu");
  puts( "Usage: test_xsh_localize_ifu [options] OBJPOS_TAB SHIFTIFU_TAB");

  puts( "Options" ) ;
  puts( " --help             : What you see" ) ;
  puts( " --wave-ref=        : Wavelength reference");
  puts( " --wave-ref-hsize=  : Half size in nm for estimate wavelength reference [2.5]");
  puts( "\nInput Files" ) ;
  puts( "OBJPOS_TAB          : OBJPOS frame");
  puts( "SHIFTIFU_TAB        : [OPTIONAL] SHIFTIFU_TAB frame");
  TEST_END();
}


static void HandleOptions( int argc, char **argv, 
  double *waveref, double *waveref_hsize)
{
  int opt ;
  int option_index = 0;

  while (( opt = getopt_long (argc, argv, 
    "wave-ref",
    long_options, &option_index)) != EOF ){
  
    switch ( opt ) {
    case  WAVEREF_OPT:
      *waveref = atof(optarg);
      break;
    case  WAVEREF_HSIZE_OPT:
      *waveref_hsize = atof(optarg);
      break;
       
       
    default: 
      Help(); exit(-1);
    }
  }
  return;
}


int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  double waveref = 700;
  double waveref_hsize = 2.5;
  const char *objpos_name = NULL;
  const char *shiftifu_name = NULL;
  cpl_frame *objpos_frame = NULL;
  cpl_frame *shiftifu_frame = NULL;
  cpl_frame *result = NULL;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  /* Analyse parameters */
  HandleOptions( argc, argv, &waveref, &waveref_hsize);

  if ( (argc - optind) > 0 ) {
    objpos_name = argv[optind];
    if ( (argc - optind) > 1 ) {
      shiftifu_name = argv[optind+1];
    }
  }
  else {
    Help();
    exit( 0);
  }
  
  xsh_msg("---Input Files");
  xsh_msg("Objpos  : %s ", objpos_name);
  xsh_msg("Shiftifu: %s ", shiftifu_name);
  xsh_msg("---Options");
  xsh_msg("Waveref       : %f ", waveref);
  xsh_msg("Waveref_hsize : %f ", waveref_hsize);

  /* Create frames */
  TESTS_XSH_FRAME_CREATE( objpos_frame, "OBJPOS_arm", objpos_name);
  if ( shiftifu_name != NULL){
    TESTS_XSH_FRAME_CREATE( shiftifu_frame, "SHIFTIFU_arm", shiftifu_name);
  }

  check( result = xsh_compute_shift_ifu_slitlet( waveref, objpos_frame, 
    shiftifu_frame, waveref_hsize, "shift.fits"));

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    }
    xsh_free_frame( &result);
    xsh_free_frame( &objpos_frame);
    xsh_free_frame( &shiftifu_frame);
    TEST_END();
    return ret ;
}

/**@}*/
