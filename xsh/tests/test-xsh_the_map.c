/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2012-12-07 16:51:34 $
 * $Revision: 1.10 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_the_tab Test a THE tab
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_the_map.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <xsh_badpixelmap.h>

#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_THE_MAP"
#define SYNTAX "Test the theoretical map\n"\
  "use : ./the_xsh_themap THE_MAP PRE_FRAME\n"\
  "THE_MAP   => the theoretical map table\n"

  
/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_themap
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  char* the_name = NULL;
  cpl_frame* the_frame = NULL;
  xsh_the_map* themap = NULL;
  int themap_size = 0;
  int i = 0;
  FILE* themap_file = NULL;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    the_name = argv[1];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    return 0;  
  }
  
  /* Create frames */
  XSH_ASSURE_NOT_NULL( the_name);
  the_frame = cpl_frame_new();
  cpl_frame_set_filename( the_frame, the_name) ;
  cpl_frame_set_level( the_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( the_frame, CPL_FRAME_GROUP_RAW ) ;

  check( themap = xsh_the_map_load( the_frame));
  check(themap_size = xsh_the_map_get_size(themap));

  /* Create DS9 REGION FILE */
  themap_file = fopen( "THEMAP.reg", "w");
  fprintf( themap_file, "# Region file format: DS9 version 4.0\n\
    global color=red font=\"helvetica 4 normal\"\
    select=1 highlite=1 edit=1 move=1 delete=1 include=1 fixed=0 source \nimage\n");

  for( i=0; i<themap_size; i++){
    float lambdaTHE = 0.0;
    double xd, yd, slit; 

    check(lambdaTHE = xsh_the_map_get_wavelength(themap, i));
    check(xd = xsh_the_map_get_detx(themap, i));
    check(yd = xsh_the_map_get_dety(themap, i));

    check(slit = (double) xsh_the_map_get_slit_position( themap, i));
    if (slit == 0){
      fprintf( themap_file, "point(%f,%f) #point=cross color=yellow "\
        "font=\"helvetica 10 normal\"  text={THE %.3f}\n", xd, yd, lambdaTHE);
    }
    else{
      fprintf( themap_file, "point(%f,%f) #point=cross color=yellow "\
        "font=\"helvetica 10 normal\"  text={slit %f}\n", xd, yd, slit);
    }
  }


  cleanup:
    xsh_free_frame( &the_frame);
    xsh_the_map_free( &themap);
    if(themap_file != NULL) {
       fclose( themap_file);
    }
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    }
    TEST_END();
    return ret ;
}

/**@}*/
