/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: rhaigron $
 * $Date: 2011-04-08 13:26:00 $
 * $Revision: 1.3 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_remove_crh_single  Test Remove Crh Multi function(s)
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_drl.h>
#include <cpl.h>
#include <math.h>

#include <string.h>
#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_COMPUTE_ABSORP"

enum {
  FILTER_HSIZE_OPT, THRESH_OPT, HELP_OPT
} ;

static struct option long_options[] = {
  {"filter-hsize", required_argument, 0,  FILTER_HSIZE_OPT},
  {"threshold", required_argument, 0,  THRESH_OPT},
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};

static void Help( void )
{
  puts( "Unitary test of xsh_compute_absorp");
  puts( "Usage: test_xsh_compute_absorp [options] S1D TELLLIST");

  puts( "Options" ) ;
  puts( " --help             : What you see" ) ;
  puts( " --filter-hsize=    : Half size of filter");
  puts( " --threshold=       : Threshold");
  puts( "\nInput Files" ) ;
  puts( "S1D           : 1D spectrum file");
  puts( "TELLLIST      : tell line list");
  TEST_END();
}


static void HandleOptions( int argc, char **argv, 
  int *filter_hsize, double *threshold)
{
  int opt ;
  int option_index = 0;

  while (( opt = getopt_long (argc, argv, 
    "filter-hsize",
    long_options, &option_index)) != EOF ){
  
    switch ( opt ) {
    case  FILTER_HSIZE_OPT:
      *filter_hsize = atoi( optarg);
      break;
    case  THRESH_OPT:
      *threshold = atof( optarg);
      break;       
    default: 
      Help(); exit(-1);
    }
  }
  return;
}


int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  int filter_hsize = 5;
  double threshold = 0.0;
  const char *s1d_name = NULL;
  cpl_frame *s1d_frame = NULL;
  const char *tell_name = NULL;
  cpl_frame *tell_frame = NULL;
  xsh_instrument *instr = NULL;
  
  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  /* Analyse parameters */
  HandleOptions( argc, argv, &filter_hsize, &threshold);

  if ( (argc - optind) > 1 ) {
    s1d_name = argv[optind];
    tell_name = argv[optind+1];
  }
  else {
    Help();
    exit( 0);
  }
  
  xsh_msg("---Input Files");
  xsh_msg("S1D File         : %s ", s1d_name);
  xsh_msg("Telllist File    : %s ", tell_name);  
  xsh_msg("---Options");
  xsh_msg("Filter hsize     : %d ", filter_hsize);
  xsh_msg("Threshold        : %f ", threshold);
  
  instr = xsh_instrument_new();
  xsh_instrument_set_arm( instr, XSH_ARM_UVB);
  TESTS_XSH_FRAME_CREATE( s1d_frame, "S1D", s1d_name);
  TESTS_XSH_FRAME_CREATE( tell_frame, "TELL_LINE_LIST", tell_name);
  
  check( xsh_compute_absorp( s1d_frame, tell_frame, filter_hsize, threshold, instr));
  
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    }
    xsh_instrument_free( &instr);
    xsh_free_frame( &s1d_frame);
    xsh_free_frame( &tell_frame);
    TEST_END();
    return ret ;
}

/**@}*/
