/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-09-14 11:37:12 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.7.6.1  2011/04/15 13:31:26  rhaigron
 * add center_cube function
 *
 * Revision 1.7  2009/05/22 17:35:50  rhaigron
 * new version os slit nod work only in fast mode
 *
 * Revision 1.6  2009/04/22 12:55:07  rhaigron
 * correct sompe memory leaks
 *
 * Revision 1.5  2009/04/17 14:13:43  rhaigron
 * new version with some cosmics remove
 *
 * Revision 1.4  2009/04/16 08:55:19  rhaigron
 * add check for spectral format
 *
 * Revision 1.3  2008/07/11 13:26:45  lgugliel
 * Added functions to create fake rectified frames.
 *
 * Revision 1.2  2008/06/13 15:18:16  rhaigron
 * *** empty log message ***
 *
 * Revision 1.1  2007/11/13 12:08:16  amodigli
 * added to remository (from xshp/tests)
 *
 * Revision 1.12  2007/10/04 13:40:27  rhaigron
 * skip nan lines in the_map
 *
 * Revision 1.11  2007/10/04 09:47:04  rhaigron
 * cpl_init take an argument now
 *
 * Revision 1.10  2007/09/25 08:10:38  rhaigron
 * *** empty log message ***
 *
 * Revision 1.9  2007/07/03 13:47:38  rhaigron
 * introduce dbg level for log
 *
 * Revision 1.8  2007/06/29 11:54:28  rhaigron
 * *** empty log message ***
 *
 * Revision 1.7  2007/04/03 09:12:00  lgugliel
 * Added test-xsh_detect_continuum unitary test
 * Added functions to create order tables and images with orders (in test.c)
 *
 * Revision 1.6  2006/11/20 10:07:57  rhaigron
 * detect order edge chnages ny by ny
 *
 * Revision 1.5  2006/11/17 19:12:18  rhaigron
 * *** empty log message ***
 *
 * Revision 1.4  2006/10/26 10:50:01  lgugliel
 * Added creation and/or setting of fits header according to instrument
 * configuration:
 *    mkHeader: create and fill a new header
 *    setHeader: just fill an existing header
 *
 * Revision 1.3  2006/10/19 14:42:22  rhaigron
 * *** empty log message ***
 *
 * Revision 1.2  2006/10/19 09:08:43  rhaigron
 * some cosmetical changes need by staticcheck
 *
 * Revision 1.1  2006/03/03 13:15:16  jmlarsen
 * Imported sources
 *
 * Revision 1.2  2005/12/19 16:17:56  jmlarsen
 * Replaced bool -> int
 *
 */
#ifndef TESTS_H
#define TESTS_H

#include <cpl.h>
#include <xsh_drl.h>
#include <stdbool.h>
#include <xsh_data_order.h>
#include <config.h>
#include <stdlib.h>
#include <unistd.h>

#define TESTS_INIT(DRL_ID) do {         \
    cpl_init(CPL_INIT_DEFAULT);         \
    xsh_init();                         \
    cpl_msg_set_domain("Test-" DRL_ID); \
} while(false)

#define TEST_END()\
  xsh_free_temporary_files(); \
  cpl_end()

#define TESTS_XSH_INSTRUMENT_CREATE(instr, mode, arm, lamp, recipe)\
  instr = xsh_instrument_new() ;\
  xsh_instrument_set_mode( instr, mode) ;\
  xsh_instrument_set_arm( instr, arm) ;\
  xsh_instrument_set_lamp( instr, lamp) ;\
  xsh_instrument_set_recipe_id( instr, recipe) ;\
  xsh_msg( "   recipe_id: %s", instr->recipe_id )

#define TESTS_XSH_FRAME_CREATE( frame, tag, name)\
  frame = cpl_frame_new();\
  cpl_frame_set_filename( frame, name);\
  cpl_frame_set_level( frame, CPL_FRAME_LEVEL_TEMPORARY);\
  cpl_frame_set_group( frame, CPL_FRAME_GROUP_RAW);\
  cpl_frame_set_tag( frame, tag)


#define TESTS_DATA(file)  XSH_TEST_DATA_PATH "/" file 

#define TESTS_INIT_WORKSPACE(DRL_ID)                            \
  /* Create an new workspace and change to that directory. */   \
  (void) system("test -d workspace_Test-"DRL_ID                 \
                " || mkdir workspace_Test-"DRL_ID);             \
  (void) chdir("workspace_Test-"DRL_ID);

#define TESTS_CLEAN_WORKSPACE(DRL_ID)                            \
  /* Delete the test sub-directory if it exists and no errors   \
   * were found when running the unit tests. */                 \
  (void) chdir("..");                                           \
  if (cpl_error_get_code() == CPL_ERROR_NONE) {                 \
    (void) system("test -d workspace_Test-"DRL_ID               \
                  " && rm -r -f workspace_Test-"DRL_ID);        \
  }                                                             \


/* Define accuracies here */
#define XSH_DRL_FUNC_RMS 0.01
#define XSH_FLOAT_PRECISION 0.000003

void
tests_set_defaults(cpl_parameterlist *parlist);

cpl_image* xsh_test_create_bias_image(const char* name, int nx, int ny,
  xsh_instrument* instrument);

cpl_propertylist * mkHeader( XSH_INSTRCONFIG *iconfig,
			     int nx, int ny, double exptime ) ;
void setHeader( cpl_propertylist *header,
		XSH_INSTRCONFIG *iconfig,
		int nx, int ny, double exptime ) ;
cpl_frame* xsh_test_create_frame( const char* name,int nx, int ny,
  const char* tag, cpl_frame_group group, xsh_instrument* instrument);
void add_to_order_list( xsh_order_list * list, int order,
			int absorder,
			cpl_polynomial *poly, int xdelta,
			int starty, int endy ) ;
xsh_order_list * create_order_list( int norder, xsh_instrument* instrument ) ;

cpl_image * create_order_image( xsh_order_list* list, 
  int nx, int ny ) ;

cpl_frame * create_rectify_nod_list( int dual, const char * fname,
				     xsh_instrument * instr ) ;

cpl_frameset* sof_to_frameset( const char* sof_name);

xsh_instrument * create_instrument( const char *filename);

#endif  /* XSH_TESTS_H */
