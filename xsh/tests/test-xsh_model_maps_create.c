/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2012-12-07 16:51:34 $
 * $Revision: 1.9 $
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_model  Test model functionality
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_resid_tab.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <xsh_model_io.h>
#include <xsh_model_kernel.h>
#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_MODEL_MAPS_CREATE"
#define SYNTAX "Create a wave and a slit map from model\n"\
  "use : ./test_xsh_model_maps_create SOF\n"\
  "SOF => model config and arclist\n"\
  "Options:\n"\
  " --binx=<n>     : binning in X (default 1)\n"\
  " --biny=<n>     : binning in Y (default 1)\n"

/*
  " --mode=<str>   : observing mode ([SLIT]/IFU\n"
*/
  
/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_model_maps_create
  @return
    0 if success

*/
static const char * Options = "?" ;

enum {
   BINX_OPT, BINY_OPT /*, MODE_OPT */
} ;

static struct option LongOptions[] = {
  {"binx", required_argument, 0, BINX_OPT},
  {"biny", required_argument, 0, BINY_OPT},
/*  {"mode", required_argument, 0, MODE_OPT}, */
  {NULL, 0, 0, 0}
} ;
static char mode[32] ;
int binx =1;
int biny =1;

static void HandleOptions( int argc, char ** argv )
{
  int opt ;
  int option_index = 0;

  /* Set default values */
  strcpy( mode, "SLIT" ) ;

  while( (opt = getopt_long( argc, argv, Options,
			     LongOptions, &option_index )) != EOF )
    switch( opt ) {
    case  BINX_OPT:
      binx = atoi(optarg);
    case BINY_OPT:
      biny = atoi(optarg);
      break ;
/*
    case MODE_OPT:
      strcpy( mode, optarg ) ;
*/
    default:
      printf( SYNTAX ) ;
      TEST_END();
      exit( 0 ) ;
    }

}


int main( int argc, char **argv)
{
  int ret=0;
  xsh_instrument *instrument = NULL ;
  const char *sof_name = NULL;
  const char *model_config_name = NULL;
  const char *lines_list_name = NULL;
  cpl_frame *model_config_frame = NULL;
  cpl_frame *lines_list_frame = NULL;
  cpl_frameset *set = NULL;
  cpl_frameset *raws = NULL;
  cpl_frameset *calib = NULL;

  FILE* sof_file = NULL;
  char sof_line[200];
  xsh_xs_3 config_model;
  cpl_frame* wmap_frame=NULL;
  cpl_frame* smap_frame=NULL;
 
  char *pre_name = NULL;

  /* Initialize libraries */

  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  HandleOptions( argc, argv );


  /* Analyse parameters */
  if ((argc-optind) <= 2){
     pre_name = argv[optind];
    sof_name = argv[optind];
  }
  else{
    printf(SYNTAX);
    return 0;
  }


  XSH_ASSURE_NOT_NULL( sof_name);

  /* Create frameset from sof */
  check( set = cpl_frameset_new());
  sof_file = fopen( sof_name, "r");

  while ( fgets( sof_line, 200, sof_file)){
    char raw_name[200];
    char raw_tag[200];
    cpl_frame *raw_frame = NULL;
    /* Note: update the string format (the %__s) if raw_name(tag) changes size.
     * Remember: field width must equal 1 less than sizeof(raw_name(tag)). */
    sscanf( sof_line, "%199s %199s", raw_name, raw_tag);
    check( raw_frame = cpl_frame_new());
    check( cpl_frame_set_filename( raw_frame, raw_name));
    check( cpl_frame_set_tag( raw_frame, raw_tag));
    check( cpl_frameset_insert(set, raw_frame));
  }


  /* Validate frame set */
  check( instrument = xsh_dfs_set_groups( set));
  XSH_NEW_FRAMESET( raws);
  XSH_NEW_FRAMESET( calib);
  check( xsh_dfs_split_in_group( set, raws, calib));

  check( lines_list_frame = xsh_find_arc_line_list( calib, instrument));
  check( model_config_frame = xsh_find_model_config_tab( calib, instrument));

  check( lines_list_name = cpl_frame_get_filename( lines_list_frame));
  check( model_config_name = cpl_frame_get_filename( model_config_frame));

  xsh_msg(" Find model %s\n", model_config_name);
  xsh_msg(" Find lines list %s\n", lines_list_name);

  check( xsh_model_config_load_best( model_config_frame, &config_model));

  xsh_model_binxy(&config_model,binx,biny);
  check(xsh_model_maps_create( &config_model, instrument,
			       "WAVE_MAP_TEST","SLIT_MAP_TEST", 
			       &wmap_frame,&smap_frame,0));


 
  cleanup:
  if(sof_file!=NULL) {
     fclose( sof_file);
  }
  xsh_free_frame(&wmap_frame);
  xsh_free_frame(&smap_frame);
  xsh_free_frameset(&set);
  xsh_free_frameset(&raws);
  xsh_free_frameset(&calib);
  xsh_instrument_free(&instrument);
 
   if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else return ret ;
}

/**@}*/
