/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2012-12-07 16:51:34 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_wavecal_fwhm
 *   Test the dispersol list
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <xsh_data_order.h>
#include <xsh_data_dispersol.h>
#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <tests.h>
#include <xsh_utils_table.h>
#include <cpl.h>
#include <math.h>

/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_WAVECAL_FWHM"

#define SYNTAX "Add evaluation of wavelength ans slit by dispersion solution to file fwhm.dat produce by wavecal\n"\
  "usage : ./the_xsh_wavecal_fwhm DATA_FILE DISP_TAB\n"\
  "DATA_FILE     => File fwhm.dat produce by wavecal\n"\
  "DISP_TAB      => dispersion solution to apply\n"

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/**
 *  @brief
      Unit test of xsh_data_dispersol
  @return
    0 if success
*/
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;
  xsh_instrument * instrument = NULL ;
  XSH_ARM arm;
  const char *fwhm_name = NULL;
  const char* disp_tab_name = NULL;
  cpl_frame *disp_tab_frame = NULL;
  cpl_propertylist *header = NULL;
  xsh_dispersol_list *displist = NULL; 
  FILE *fwhm_file = NULL;
  FILE *res_file = NULL;
  cpl_vector *pos = NULL;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if ( argc > 2) {
    fwhm_name = argv[1];
    disp_tab_name = argv[2];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    return 0;
  }
  xsh_msg( "Fwhm data file:    %s", fwhm_name);
  xsh_msg( "Disp tab file:     %s", disp_tab_name);

  /* Create frames */
  check( header = cpl_propertylist_load( disp_tab_name, 0));
  instrument = xsh_instrument_new();
  check( arm = xsh_pfits_get_arm( header));
  xsh_instrument_set_arm( instrument, arm); 

  disp_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( disp_tab_frame, disp_tab_name) ;
  cpl_frame_set_level( disp_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( disp_tab_frame, CPL_FRAME_GROUP_RAW);

  check( displist = xsh_dispersol_list_load( disp_tab_frame, instrument));
  check( pos = cpl_vector_new(2));
    

  fwhm_file = fopen( fwhm_name, "r");
  res_file = fopen( "fwhm_full.dat", "w+");

  if ( fwhm_file != NULL && res_file!=NULL){
    char line[200];
    char *read; 
    /* read first line */
    read = fgets( line, 200, fwhm_file);
    fputs( "# wavelength order xcen xcen-x0 ygauss ytilt fwhm area good lambdaG slitG lambdaT slitT\n", res_file);
    while ( fgets( line, 200, fwhm_file)){
      double wavelength, xcen, xcen_x0, ygauss, ytilt, fwhm, area;
      int order, good;
      double lambdaG, slitG, lambdaT, slitT;
      int iorder=0;

      sscanf( line, "%64lf %64d %64lf %64lf %64lf %64lf %64lf %64lf %64d\n",
        &wavelength, &order, &xcen, &xcen_x0, &ygauss, &ytilt, &fwhm, &area, &good);

      while( order != displist->list[iorder].absorder){
        iorder++;
      }

      cpl_vector_set( pos, 0, xcen);
      cpl_vector_set( pos, 1, ygauss);
      check( lambdaG = xsh_dispersol_list_eval( displist,
        displist->list[iorder].lambda_poly, pos));
      check( slitG = xsh_dispersol_list_eval( displist,
        displist->list[iorder].slit_poly, pos));

      cpl_vector_set( pos, 0, xcen);
      cpl_vector_set( pos, 1, ytilt);
      check( lambdaT = xsh_dispersol_list_eval( displist,
        displist->list[iorder].lambda_poly, pos));
      check( slitT = xsh_dispersol_list_eval( displist,
        displist->list[iorder].slit_poly, pos));
      fprintf( res_file, "%f %d %f %f %f %f %f %f %d %f %f %f %f\n", 
        wavelength, order, xcen, xcen_x0, ygauss, ytilt, fwhm, area, 
        good, lambdaG, slitG, lambdaT, slitT);
    }
  }


  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    if ( fwhm_file != NULL && res_file!=NULL){
       fclose( fwhm_file);
       fclose( res_file);
    }
    xsh_dispersol_list_free( &displist);
    xsh_instrument_free( &instrument);
    xsh_free_frame( &disp_tab_frame);
    TEST_END();
    return ret ;
}

/**@}*/
