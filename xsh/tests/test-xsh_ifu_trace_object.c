/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup xsh_test_pre     Test Data Type PRE functions
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/


#include <math.h>

#include <cpl.h>
#include <tests.h>

#include <xsh_error.h>
#include <xsh_utils_ifu.h>
#include <xsh_msg.h>
#include <xsh_dfs.h>
#include <xsh_utils.h>
#include <xsh_utils_image.h>
#include <xsh_pfits.h>
#include <xsh_data_instrument.h>
#include <xsh_utils_wrappers.h>


/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_IFU_TRACE_OBJECT"

#define SYNTAX "Computes the ifu object position for each slice \n"\
  "use : ./test_xsh_ifu_trace_object IFU_OBJECT_FLATFIELDED.fits ORDER_TAB_EDGES_IFU.fits SLIT_MAP WAVE_MAP \n"

/**@{*/

/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of PRE module
  @return   0 if tests passed successfully

  Test the PRE module.
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  char *ifu_object_ff_name = NULL;
  char *order_tab_edges_ifu_name = NULL;
  char *slit_map_name = NULL;
  char *wave_map_name = NULL;

  int fit_method=0;
  int rad_x=0;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    ifu_object_ff_name = argv[1];
    order_tab_edges_ifu_name = argv[2];
    slit_map_name = argv[3];
    wave_map_name = argv[4];
    if(argc > 5) {
      fit_method = atoi(argv[5]);
      rad_x = atoi(argv[6]);
    }
  }
  else{
    /* to remove a compiler warning */
    rad_x=0; 
    fit_method = 0;
    printf(SYNTAX);
    return 0;
  }
  
  XSH_ASSURE_NOT_NULL( ifu_object_ff_name);
  XSH_ASSURE_NOT_NULL( order_tab_edges_ifu_name);
  XSH_ASSURE_NOT_NULL( slit_map_name);
  XSH_ASSURE_NOT_NULL( wave_map_name);

  check(xsh_ifu_trace_object_calibrate(ifu_object_ff_name,
                                       order_tab_edges_ifu_name,
                                       slit_map_name,wave_map_name));

  cleanup:
  if (cpl_error_get_code() != CPL_ERROR_NONE) {
    xsh_error_dump(CPL_MSG_ERROR);
    return 1;
  } else {
    return 0;
  }

}

/**@}*/
