/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2011-09-14 11:37:12 $
 * $Revision: 1.3 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_remove_crh_single  Test Remove Crh Multi function(s)
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_drl.h>
#include <cpl.h>
#include <math.h>
#include <xsh_data_spectrum.h>
#include <string.h>
#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_MARK_TELL"

enum {
  HELP_OPT
} ;

static struct option long_options[] = {
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};

static void Help( void )
{
  puts( "Unitary test of xsh_mark_tell");
  puts( "Usage: test_xsh_mark_tell [options] S1D TELLLIST");

  puts( "Options" ) ;
  puts( " --help             : What you see" ) ;
  puts( "\nInput Files" ) ;
  puts( "S1D           : 1D spectrum file");
  puts( "TELL_MASK     : telluric mask");
  TEST_END();
}


static void HandleOptions( int argc, char **argv)
{
  int opt ;
  int option_index = 0;

  while (( opt = getopt_long (argc, argv, 
    "help",
    long_options, &option_index)) != EOF ){
  
    switch ( opt ) {
    default: 
      Help(); exit(-1);
    }
  }
  return;
}

static void analyse_spectrum( cpl_frame* spectrum_frame);

/*--------------------------------------------------------------------------
 *   Implementation
 *--------------------------------------------------------------------------*/
static void analyse_spectrum( cpl_frame* spectrum_frame)
{
  const char* spectrum_name = NULL;
  xsh_spectrum *spectrum = NULL;
  int i;
  FILE* fulldatfile = NULL;
  double* flux = NULL;
  int *qual = NULL;

  XSH_ASSURE_NOT_NULL( spectrum_frame);

  check (spectrum_name = cpl_frame_get_filename( spectrum_frame));

  xsh_msg("Spectrum frame : %s", spectrum_name);

  check( spectrum = xsh_spectrum_load( spectrum_frame));
  check( flux = xsh_spectrum_get_flux( spectrum));
  check( qual = xsh_spectrum_get_qual( spectrum));

  fulldatfile = fopen("s1d_with_tell.dat","w");
  for(i=0; i< spectrum->size; i++){
    fprintf( fulldatfile, "%f %f %d\n", spectrum->lambda_min+i*spectrum->lambda_step, flux[i],
      qual[i]);
  }
  xsh_msg("Save file s1d_with_tell.dat");
  fclose( fulldatfile);

  cleanup:
    xsh_spectrum_free( &spectrum);
    return;  
}

int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  const char *s1d_name = NULL;
  cpl_frame *s1d_frame = NULL;
  const char *mask_name = NULL;
  cpl_frame *mask_frame = NULL;
  
  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM);

  /* Analyse parameters */
  HandleOptions( argc, argv);

  if ( (argc - optind) > 1 ) {
    s1d_name = argv[optind];
    mask_name = argv[optind+1];
  }
  else {
    Help();
    exit( 0);
  }
  
  xsh_msg("---Input Files");
  xsh_msg("S1D File         : %s ", s1d_name);
  xsh_msg("Tell mask File   : %s ", mask_name);  
  
  TESTS_XSH_FRAME_CREATE( s1d_frame, "S1D", s1d_name);
  TESTS_XSH_FRAME_CREATE( mask_frame, "TELL_MASK", mask_name);
  
  check( xsh_mark_tell( s1d_frame, mask_frame));
  
  analyse_spectrum( s1d_frame);
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    }
    xsh_free_frame( &s1d_frame);
    xsh_free_frame( &mask_frame);
    TEST_END();
    return ret ;
}


/**@}*/
