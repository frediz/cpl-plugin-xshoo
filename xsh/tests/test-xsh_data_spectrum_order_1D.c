/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: rhaigron $
 * $Date: 2010-04-21 13:37:05 $
 * $Revision: 1.4 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_data Visualize a spectrum order 1D
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_rec.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <xsh_badpixelmap.h>

#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_DATA_SPECTRUM_ORDER_1D"

#define SYNTAX "Test a spectrum order 1D file\n"\
  "use : ./test_xsh_data_spectrum_order_1D FRAME\n"\
  "FRAME => the spectrum order 1D frame\n"

static void analyse_extraction( cpl_frame* rec_frame,  xsh_instrument* instr);

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/
static void analyse_extraction( cpl_frame* rec_frame, xsh_instrument* instr)
{
  const char* rec_name = NULL;
  xsh_rec_list* rec_list = NULL;
  int iorder;

  XSH_ASSURE_NOT_NULL( rec_frame);

  check( rec_name = cpl_frame_get_filename( rec_frame));

  printf("RECTIFY frame : %s\n", rec_name);
  check( rec_list = xsh_rec_list_load( rec_frame, instr));

  for(iorder=0; iorder< rec_list->size; iorder++){
    int order = 0, ilambda = 0;
    int nlambda = 0;
    float *flux = NULL;
    float *err = NULL; 
    double *lambda = NULL;
    char name[256];
    FILE* datfile = NULL;

    check( order = xsh_rec_list_get_order(rec_list, iorder)); 
    check( nlambda = xsh_rec_list_get_nlambda(rec_list, iorder));
    check( flux = xsh_rec_list_get_data1( rec_list, iorder));
    check( err = xsh_rec_list_get_errs1( rec_list, iorder));
    check( lambda = xsh_rec_list_get_lambda( rec_list, iorder));

    sprintf( name, "spectrum1D_order%d.dat",order);
    xsh_msg("Save file %s",name);
    datfile = fopen( name, "w");

    fprintf( datfile, "#lambda flux err\n");

    for(ilambda=0; ilambda < nlambda; ilambda++){
      fprintf( datfile,"%f %f %f\n",lambda[ilambda],flux[ilambda], err[ilambda]);
    }
    fclose(datfile);

  }
  cleanup:
    xsh_rec_list_free( &rec_list); 
    return;
}

/**
  @brief
    Unit test of xsh_extract
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  xsh_instrument* instrument = NULL;
  char* rec_name = NULL;
  cpl_frame* rec_frame = NULL;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;


  /* Analyse parameters */
  if ( argc  == 2 ) {
      rec_name = argv[1];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    exit(0);
  }
  rec_frame = cpl_frame_new();
  XSH_ASSURE_NOT_NULL (rec_frame);
  cpl_frame_set_filename( rec_frame, rec_name) ;
  cpl_frame_set_level( rec_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( rec_frame, CPL_FRAME_GROUP_RAW ) ;


  /* Create instrument structure and fill */
  instrument = xsh_instrument_new() ;
  check( analyse_extraction( rec_frame, instrument));

  cleanup:
    xsh_instrument_free( &instrument);
    xsh_free_frame( &rec_frame);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    }
    TEST_END();
    return ret ;
}

/**@}*/
