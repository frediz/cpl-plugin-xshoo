/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_subtract_bkg Test Subtract Background 
 * @ingroup unit_tests
 *
 */
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/

#include <xsh_data_instrument.h>
#include <xsh_pfits.h>
#include <xsh_msg.h>
#include <xsh_utils.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>

#include <getopt.h>

#include <xsh_data_spectrum.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "XSH_COMPUTE_RESPONSE"

XSH_ARM TheArm = XSH_ARM_VIS ;
char DbgLevel[8] ;

enum {
  ARM_OPT, DBG_OPT
} ;

static struct option LongOptions[] = {
  {"arm", required_argument, 0, ARM_OPT},
  {"debug-level", required_argument, 0, DBG_OPT},
  {NULL, 0, 0, 0}
} ;
static const char * Options = "?" ;

static void Help( void )
{
  puts( "Unitary test of xsh_compute_response" ) ;
  puts( "Usage: test_xsh_compute_response [options] <spectrum> <star_table> [<atmos_ext>] " ) ;
  puts( "Options:" ) ;
  puts( " --arm=<arm>           : Set the arm (NIR/UVB/VIS). Default 'VIS'" ) ;
  puts( " --debug-level=<level> : Set the debug level (none/low/medium/high)." ) ;
  puts( "                         Default 'low'" ) ;
  TEST_END();
  exit( 0 ) ;
}

static void HandleOptions( int argc, char **argv )
{
  int opt ;
  int option_index = 0;

  while (( opt = getopt_long (argc, argv, Options, LongOptions,
			      &option_index )) != EOF )
    switch( opt ) {
    case ARM_OPT:
      TheArm = xsh_arm_get( optarg );
      break ;
    case DBG_OPT:
      strcpy( DbgLevel, optarg ) ;
      break ;
    default:
      Help() ;
    }
}

/*--------------------------------------------------------------------------*/
/**
  @brief    Unit test of XSH_COMPUTE_RESPONSE
  @return   0 if tests passed successfully

  Test the Data Reduction Library function XSH_COMPUTE_RESPONSE
 */
/*--------------------------------------------------------------------------*/

int main( int argc, char **argv)
{
  int ret = 1;

  xsh_instrument* instrument = NULL;
  cpl_frame * spectrum_frame = NULL ;
  cpl_frame * star_frame = NULL ;
  cpl_frame * atmos_frame = NULL ;
  cpl_frame * result = NULL ;
  const char * spectrum_name = NULL ;
  const char * star_name = NULL ;
  const char * atmos_name = NULL ;
  const char * tag ;

  char arm_str[8] ;
  double exptime = 1. ;
  int nbframes ;

  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  if ( strcmp( DbgLevel, "low" ) == 0 )
    xsh_debug_level_set(XSH_DEBUG_LEVEL_LOW);
  else if ( strcmp( DbgLevel, "medium" ) == 0 )
    xsh_debug_level_set(XSH_DEBUG_LEVEL_LOW);
  else if ( strcmp( DbgLevel, "high" ) == 0 )
    xsh_debug_level_set(XSH_DEBUG_LEVEL_LOW);
  else xsh_debug_level_set(XSH_DEBUG_LEVEL_NONE);

  /* Analyse parameters */
  HandleOptions( argc, argv );

  nbframes = argc - optind ;
  if ( nbframes >= 2 ) {
    spectrum_name = argv[optind] ;
    star_name = argv[optind+1] ;
    if ( nbframes == 3 ) atmos_name = argv[optind+2] ;
  }
  else Help() ;

  TESTS_XSH_INSTRUMENT_CREATE( instrument, XSH_MODE_SLIT, TheArm,
			        XSH_LAMP_UNDEFINED, "xsh_respon_stare");
  sprintf( arm_str, "%s",  xsh_instrument_arm_tostring(instrument) ) ;

  tag = XSH_GET_TAG_FROM_ARM( XSH_MERGE1D, instrument ) ;
  xsh_msg_dbg_medium( "%s tag: %s", spectrum_name, tag ) ;
  TESTS_XSH_FRAME_CREATE( spectrum_frame, tag, spectrum_name ) ;

  tag = XSH_GET_TAG_FROM_ARM( XSH_STD_STAR_FLUX, instrument ) ;
  xsh_msg_dbg_medium( "%s tag: %s", star_name, tag ) ;
  TESTS_XSH_FRAME_CREATE( star_frame, tag, star_name ) ;

  if ( atmos_name != NULL ) {
    tag = XSH_GET_TAG_FROM_ARM( XSH_ATMOS_EXT, instrument ) ;
    xsh_msg_dbg_medium( "%s tag: %s", atmos_name, tag ) ;
    TESTS_XSH_FRAME_CREATE( atmos_frame, tag, atmos_name ) ;
  }

  /* Get Exptime */
  {
    xsh_spectrum * xspec = NULL ;

    check( xspec = xsh_spectrum_load( spectrum_frame) ) ;
    check( exptime = xsh_pfits_get_exptime( xspec->flux_header ) ) ;
    xsh_msg( "EXPTIME: %lf", exptime ) ;

    xsh_spectrum_free( &xspec ) ;
  }


  check( result = xsh_compute_response( spectrum_frame,
					star_frame,
					atmos_frame,
                                        NULL,
					instrument,
					exptime ) ) ;
  ret = 0 ;

 cleanup:
  xsh_free_frame(&result);
  xsh_msg( "Finished %d", ret ) ;
  return ret ;

}
