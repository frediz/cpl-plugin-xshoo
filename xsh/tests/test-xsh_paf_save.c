/*                                                                              *
 *   This file is part of the ESO IRPLIB package                                *
 *   Copyright (C) 2004,2005 European Southern Observatory                      *
 *                                                                              *
 *   This library is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by       *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                        *
 *                                                                              *
 *   This program is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *   GNU General Public License for more details.                               *
 *                                                                              *
 *   You should have received a copy of the GNU General Public License          *
 *   along with this program; if not, write to the Free Software                *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA       *
 *                                                                              */
 
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/
#include <cpl.h>
#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_dfs.h>
#include <xsh_pfits.h>
#include <math.h>
#include <string.h>
#include <xsh_paf_save.h>
#include <xsh_qc_handling.h>
#include <tests.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
#define MODULE_ID "XSH_PAF_SAVE"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_paf_save Test PAF Save functions
 * @ingroup unit_tests
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
   @brief   Unit tests of PAF saving
**/
/*----------------------------------------------------------------------------*/

static void with_dots( char *to, const char *from, char *filename )
{
  int k ;

  k = sprintf( to, "grep --quiet '" ) ;
  to += k ;

  if ( strncmp( from, "ESO ", 4 ) == 0 )
    from += 4 ;

  for( ; *from != '\0' ; from++, to++ )
    if ( *from == ' ' ) *to = '.' ;
    else *to = *from ;
  *to++ = '\'' ;
  *to++ = ' ' ;
  *to = '\0' ;
  strcat( to, filename ) ;
}

int main(void)
{
    cpl_propertylist *qc = NULL;
    char * filename = NULL;
    qc_description *pqc ;
    int ret=0;
    xsh_instrument * iiinstrument = NULL;

    TESTS_INIT_WORKSPACE(MODULE_ID);
    TESTS_INIT(MODULE_ID);

    /* Create PAF file */
    {
      const char * const rrrecipe = "xsh_mbias";
      const char * pro_catg = NULL ;


      iiinstrument = xsh_instrument_new() ;
      xsh_instrument_set_arm( iiinstrument, XSH_ARM_UVB ) ;
      xsh_instrument_set_mode( iiinstrument, XSH_MODE_UNDEFINED ) ;
      pro_catg = "MASTER_BIAS_UVB" ;

      filename = xsh_stringcat_any(rrrecipe, XSH_PAF_SAVE_EXTENSION,
                                   (void*)NULL) ;

      assure( xsh_paf_save(iiinstrument, rrrecipe,
			   NULL,
			   filename, pro_catg)
	      == CPL_ERROR_NULL_INPUT, CPL_ERROR_ILLEGAL_OUTPUT,
	      "Failed to fail on NULL input");
      xsh_error_reset();

      qc = cpl_propertylist_new();

      /* Add QC parameters of xsh_mbias recipe */
      pqc = NULL ;
      while ( (pqc = xsh_get_qc_desc_by_recipe( rrrecipe, pqc )) != NULL ) {
	//xsh_msg( "QC Parameter: %s", pqc->kw_name ) ;
	switch( pqc->kw_type ) {
	case CPL_TYPE_INT:
	  cpl_propertylist_append_int (qc, pqc->kw_name, 0 ) ;
	  break ;
	case CPL_TYPE_DOUBLE:
	  cpl_propertylist_append_double(qc, pqc->kw_name, 1.0 ) ;
	  break ;
	case CPL_TYPE_CHAR:
	  cpl_propertylist_append_char(qc, pqc->kw_name, 'x' ) ;
	  break ;
	case CPL_TYPE_FLOAT:
	  cpl_propertylist_append_float(qc, pqc->kw_name, 0.1 ) ;
	  break ;
	case CPL_TYPE_STRING:
	  cpl_propertylist_append_string(qc, pqc->kw_name, "ABCDEF" ) ;
	  break ;
	default:
	  xsh_msg( "Other Types not handled" ) ;
	  break ;
	}
	if ( pqc->kw_help != NULL )
	  cpl_propertylist_set_comment  (qc, pqc->kw_name, pqc->kw_help ) ;
      }

      assure( xsh_paf_save(iiinstrument, rrrecipe,
			   qc,
			   filename, pro_catg)
	      == CPL_ERROR_NONE, cpl_error_get_code(),
	      "PAF file creation failed");
      /* Check that PAF contains all the relevant QC */
      pqc = NULL ;
      while ( (pqc = xsh_get_qc_desc_by_recipe( rrrecipe, pqc )) != NULL ) {
	char cmd[128] ;

	memset( cmd, 0, 128 ) ;
	with_dots( cmd, pqc->kw_name, filename ) ;
	assure( system( cmd ) == 0, CPL_ERROR_ILLEGAL_INPUT,
		"QC parameter '%s' NOT in PAF",
		pqc->kw_name ) ;
      }
    }
    
  cleanup:
    xsh_instrument_free( &iiinstrument);
    xsh_free_propertylist(&qc);
    cpl_free(filename);
    TESTS_CLEAN_WORKSPACE(MODULE_ID);
    TEST_END();

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
	xsh_error_dump(CPL_MSG_ERROR);
	ret =  1;
    }
    return ret;
}
