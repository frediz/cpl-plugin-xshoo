/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: rhaigron $
 * $Date: 2010-10-20 12:30:38 $
 * $Revision: 1.1 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_data Visualize a spectrum order 1D
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_rec.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <xsh_badpixelmap.h>

#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_DATA_ORDER_2D"

#define SYNTAX "Invert an order 2D file\n"\
  "use : ./test_xsh_data_order_2D FRAME\n"\
  "FRAME => the order 2D frame\n"

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_extract
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  xsh_instrument* instrument = NULL;
  char* rec_name = NULL;
  cpl_frame* rec_frame = NULL;
  cpl_frame *result = NULL;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);

  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;


  /* Analyse parameters */
  if ( argc  == 2 ) {
      rec_name = argv[1];
  }
  else{
    printf(SYNTAX);
    TEST_END();
    exit(0);
  }
  rec_frame = cpl_frame_new();
  XSH_ASSURE_NOT_NULL (rec_frame);
  cpl_frame_set_filename( rec_frame, rec_name) ;
  cpl_frame_set_level( rec_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( rec_frame, CPL_FRAME_GROUP_RAW ) ;


  /* Create instrument structure and fill */
  instrument = xsh_instrument_new() ;
  check( result = xsh_rec_list_frame_invert( rec_frame,
    "INV_TEST", instrument));

  cleanup:
    xsh_instrument_free( &instrument);
    xsh_free_frame( &rec_frame);
    xsh_free_frame( &result);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret=1;
    }
    TEST_END();
    return ret ;
}

/**@}*/
