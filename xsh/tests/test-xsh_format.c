/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2012-01-16 21:21:52 $
 * $Revision: 1.5 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
  @defgroup test_xsh_format  Test a cube build
  @ingroup unit_tests 
*/
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>
#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_rec.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>
#include <xsh_data_pre_3d.h>
#include <xsh_badpixelmap.h>

#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_FORMAT"

enum {
  OVERSAMPLE_OPT, BOX_HSIZE_OPT, CHUNK_SIZE_OPT, LAMBDA_STEP_OPT, 
  KAPPA_OPT, METHOD_OPT, MIN_ORDER_OPT, MAX_ORDER_OPT, HELP_OPT
};
/*
static struct option long_options[] = {
  {"oversample", required_argument, 0, OVERSAMPLE_OPT},
  {"box-hsize", required_argument, 0, BOX_HSIZE_OPT},
  {"chunk-size", required_argument, 0, CHUNK_SIZE_OPT},
  {"lambda-step", required_argument, 0, LAMBDA_STEP_OPT},
  {"kappa", required_argument, 0, KAPPA_OPT},
  {"method", required_argument, 0, METHOD_OPT},
  {"order-min", required_argument, 0, MIN_ORDER_OPT},
  {"order-max", required_argument, 0, MAX_ORDER_OPT},
  {"help", 0, 0, HELP_OPT},
  {0, 0, 0, 0}
};
*/
static void Help( void )
{
  puts( "Unitary test of a cube build");
  puts( "Usage: test_format [options] <input_files>");

  puts( "Options" ) ;
  puts( "\nInput Files" ) ;
  puts( "The input files argument MUST be in this order:" ) ;
  puts( " 1. ORDER2D_DOWN, ORDER2D_CEN, ORDER2D_UP" ) ;
  TEST_END();
}

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_opt_extract
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  int ret;
  /* Declarations */
  xsh_instrument* instrument = NULL;
  const char *sof_name = NULL;
  cpl_frameset *set = NULL;
  cpl_frame *cube = NULL;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM);

  /* Analyse parameters */
  if (argc >= 2 ) {
    sof_name = argv[1];
  }
  else{
    Help();
    exit(0);
  }

  /* Create frameset from sof */
  check( set = sof_to_frameset( sof_name));
  check( instrument = xsh_dfs_set_groups( set));

  check( cube = xsh_format( set, "CUBE.fits", instrument,"test"));

  /* USE load function */

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret = 1;
    }
    xsh_free_frameset( &set);
    xsh_instrument_free( &instrument); 
    xsh_free_frame( &cube);

    TEST_END();
    return ret;
}

/**@}*/
