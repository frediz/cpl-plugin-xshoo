/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2012-12-07 16:51:34 $
 * $Revision: 1.4 $
*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_resid_tab  Test a Residual tab
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_resid_tab.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <xsh_model_io.h>
#include <xsh_model_kernel.h>
#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_MODEL"
#define SYNTAX "Create a the tab from model\n"\
  "use : ./test_xsh_model SOF\n"\
  "SOF => model config and arclist\n"

  
/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_resid_tab
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  int ret=0;
  xsh_instrument *instrument = NULL ;
  const char *sof_name = NULL;
  const char *model_config_name = NULL;
  const char *lines_list_name = NULL;
  cpl_frame *model_config_frame = NULL;
  cpl_frame *lines_list_frame = NULL;
  cpl_frame *the_frame = NULL;
  cpl_frameset *set = NULL;
  cpl_frameset *raws = NULL;
  cpl_frameset *calib = NULL;

  FILE* sof_file = NULL;
  char sof_line[200];
  xsh_xs_3 config_model;

  /* Initialize libraries */
  TESTS_INIT(MODULE_ID);
  cpl_msg_set_level(CPL_MSG_DEBUG);
  xsh_debug_level_set(XSH_DEBUG_LEVEL_MEDIUM) ;

  /* Analyse parameters */
  if (argc > 1){
    sof_name = argv[1];
  }
  else{
    printf(SYNTAX);
    return 0;
  }

  XSH_ASSURE_NOT_NULL( sof_name);

  /* Create frameset from sof */
  check( set = cpl_frameset_new());
  sof_file = fopen( sof_name, "r");
  while ( fgets( sof_line, 200, sof_file)){
    char raw_name[200];
    char raw_tag[200];
    cpl_frame *raw_frame = NULL;
    /* Note: update the string format (the %__s) if raw_name(tag) changes size.
     * Remember: field width must equal 1 less than sizeof(raw_name(tag)). */
    sscanf( sof_line, "%199s %199s", raw_name, raw_tag);
    check( raw_frame = cpl_frame_new());
    check( cpl_frame_set_filename( raw_frame, raw_name));
    check( cpl_frame_set_tag( raw_frame, raw_tag));
    check( cpl_frameset_insert(set, raw_frame));
  }


  /* Validate frame set */
  check( instrument = xsh_dfs_set_groups( set));
  XSH_NEW_FRAMESET( raws);
  XSH_NEW_FRAMESET( calib);
  check( xsh_dfs_split_in_group( set, raws, calib));
  
  check( lines_list_frame = xsh_find_arc_line_list( calib, instrument));
  check( model_config_frame = xsh_find_model_config_tab( calib, instrument));

  check( lines_list_name = cpl_frame_get_filename( lines_list_frame));
  check( model_config_name = cpl_frame_get_filename( model_config_frame));

  xsh_msg(" Find model %s\n", model_config_name);
  xsh_msg(" Find lines list %s\n", lines_list_name);

  check( xsh_model_config_load_best( model_config_frame, &config_model));
  the_frame = xsh_model_THE_create( &config_model, instrument, 
    lines_list_name, 9, 1.4, "THE.fits");
  cleanup:
  xsh_free_frame(&the_frame);
  if(sof_file!=NULL) {
      fclose( sof_file);
   }
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else return ret ;
}

/**@}*/
