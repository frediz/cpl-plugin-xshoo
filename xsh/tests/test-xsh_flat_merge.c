/*                                                                          *
 *   This file is part of the ESO X-shooter Pipeline                        *
 *   Copyright (C) 2006 European Southern Observatory                       *
 *                                                                          *
 *   This library is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   You should have received a copy of the GNU General Public License      *
 *   along with this program; if not, write to the Free Software            *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA   *
 *                                                                          */
 
/*
 * $Author: amodigli $
 * $Date: 2009-12-30 14:51:26 $
 * $Revision: 1.5 $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*-------------------------------------------------------------------------*/
/**
 * @defgroup test_xsh_extract  Test Object extraction
 * @ingroup unit_tests
 * 
 */
/*-------------------------------------------------------------------------*/
/**@{*/

/*--------------------------------------------------------------------------
  Includes
  --------------------------------------------------------------------------*/

#include <tests.h>

#include <xsh_data_pre.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <xsh_data_instrument.h>
#include <xsh_data_rec.h>
#include <xsh_data_localization.h>
#include <xsh_drl.h>
#include <xsh_pfits.h>

#include <xsh_badpixelmap.h>

#include <cpl.h>
#include <math.h>

#include <getopt.h>

/*--------------------------------------------------------------------------
  Defines
  --------------------------------------------------------------------------*/

#define MODULE_ID "XSH_FLAT_MERGE"

#define SYNTAX "Test the flat merge function\n"\
  "use : ./test_xsh_flat_merge QTH_FRAME QTH_ORDER_TAB D2_FRAME D2_ORDER_TAB"\
  " SPECTRAL_TAB\n"\
  "QTH_FRAME => the qth flat frame\n"\
  "QTH_ORDER_TAB => the qth order table\n"\
  "D2_FRAME => the d2 flat frame\n"\
  "D2_ORDER_TAB => the d2 order table\n"\

/*--------------------------------------------------------------------------
  Implementation
  --------------------------------------------------------------------------*/

/**
  @brief
    Unit test of xsh_flat_merge
  @return
    0 if success

*/
int main( int argc, char **argv)
{
  /* Declarations */
  int ret = 0 ;
  xsh_instrument* instrument = NULL;
 
  char* qth_name = NULL;
  cpl_frame* qth_frame = NULL;
  char* qth_order_tab_name = NULL; 
  cpl_frame* qth_order_tab_frame = NULL;
  char* d2_name = NULL; 
  char* d2_bkg_name = NULL; 
  char* qth_bkg_name = NULL; 
  cpl_frame* d2_frame = NULL;
  cpl_frame* d2_bkg_frame = NULL;
  cpl_frame* qth_bkg_frame = NULL;

  char* d2_order_tab_name = NULL;
  cpl_frame* d2_order_tab_frame = NULL;

  cpl_frame *qth_d2_flat_frame = NULL;
  cpl_frame *qth_d2_bkg_frame = NULL;
  cpl_frame *qth_d2_order_tab_frame = NULL;

  /* Initialize libraries */
  TESTS_INIT( MODULE_ID);

  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;


  /* Analyse parameters */
  if ( argc > 4 ) {
    qth_name = argv[1];
    qth_bkg_name = argv[2];
    qth_order_tab_name = argv[3];
    d2_name = argv[4];
    d2_bkg_name = argv[5];
    d2_order_tab_name = argv[6];
    xsh_msg(" %s %s %s %s %s %s",
            qth_name,qth_bkg_name,qth_order_tab_name, 
            d2_name, d2_bkg_name, d2_order_tab_name);
  }
  else{
    printf(SYNTAX);
    exit(0);
  }

  qth_frame = cpl_frame_new();
  cpl_frame_set_filename( qth_frame, qth_name) ;
  cpl_frame_set_level( qth_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( qth_frame, CPL_FRAME_GROUP_RAW ) ;
  cpl_frame_set_tag( qth_frame, "MASTER_FLAT_SLIT_UVB_QTH");



  qth_bkg_frame = cpl_frame_new();
  cpl_frame_set_filename( qth_bkg_frame, qth_name) ;
  cpl_frame_set_level( qth_bkg_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( qth_bkg_frame, CPL_FRAME_GROUP_RAW ) ;
  cpl_frame_set_tag( qth_bkg_frame, "BKG_FLAT_SLIT_UVB_QTH");

  qth_order_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( qth_order_tab_frame, qth_order_tab_name) ;
  cpl_frame_set_level( qth_order_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( qth_order_tab_frame, CPL_FRAME_GROUP_RAW ) ;

  d2_frame = cpl_frame_new();
  cpl_frame_set_filename( d2_frame, d2_name) ;
  cpl_frame_set_level( d2_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( d2_frame, CPL_FRAME_GROUP_RAW ) ;
  cpl_frame_set_tag( d2_frame, "MASTER_FLAT_SLIT_UVB_D2");


  d2_bkg_frame = cpl_frame_new();
  cpl_frame_set_filename( d2_bkg_frame, qth_name) ;
  cpl_frame_set_level( d2_bkg_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( d2_bkg_frame, CPL_FRAME_GROUP_RAW ) ;
  cpl_frame_set_tag( d2_bkg_frame, "BKG_FLAT_SLIT_UVB_D2");


  d2_order_tab_frame = cpl_frame_new();
  cpl_frame_set_filename( d2_order_tab_frame, d2_order_tab_name) ;
  cpl_frame_set_level( d2_order_tab_frame, CPL_FRAME_LEVEL_TEMPORARY);
  cpl_frame_set_group( d2_order_tab_frame, CPL_FRAME_GROUP_RAW ) ;

  /* Create instrument structure and fill */
  instrument = xsh_instrument_new() ;

  instrument = xsh_instrument_new();
  xsh_instrument_set_arm( instrument, XSH_ARM_UVB); 

  xsh_instrument_set_mode( instrument, XSH_MODE_SLIT ) ;
  xsh_instrument_set_lamp( instrument, XSH_LAMP_QTH_D2) ;

  check( xsh_flat_merge_qth_d2( qth_frame,qth_order_tab_frame,  
                                d2_frame,d2_order_tab_frame,
                                qth_bkg_frame,d2_bkg_frame,
                                &qth_d2_flat_frame,&qth_d2_bkg_frame,
                                &qth_d2_order_tab_frame, instrument));

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      return 1;
    }
    else return ret ;
}

/**@}*/
