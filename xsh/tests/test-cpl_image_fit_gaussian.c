/*                                                                            *
 *   This file is part of the ESO X-shooter Pipeline                          *
 *   Copyright (C) 2006 European Southern Observatory                         *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                            */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:13:14 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*--------------------------------------------------------------------------*/
/**
  @defgroup xsh_test_tools     
    Test some tools functions for performances check
  @ingroup unit_tests
*/
/*--------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <xsh_utils.h>
#include <xsh_error.h>
#include <xsh_msg.h>
#include <tests.h>
#include <cpl.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
/*---------------------------------------------------------------------------
                            Defines
 ---------------------------------------------------------------------------*/
#define MODULE_ID "CPL_IMAGE_FIT_GAUSSIAN"

/*--------------------------------------------------------------------------*/
/**
  @brief
    Some test about performances
  @return
 */
/*--------------------------------------------------------------------------*/
int main( int argc, char** argv)
{
  int ret = 0;
  char *image_name = NULL;
  cpl_image* image = NULL;
  double norm=0.0, xcen=0.0, ycen=0.0, sig_x=0.0, sig_y=0.0; 
  int nx,ny;

  TESTS_INIT( MODULE_ID);
  cpl_msg_set_level( CPL_MSG_DEBUG);
  xsh_debug_level_set( XSH_DEBUG_LEVEL_MEDIUM) ;

  if (argc > 1){
    image_name = argv[1];
  }
  else{
    return 0;
  }
  check( image = cpl_image_load( image_name, CPL_TYPE_FLOAT, 0, 0));
  check( nx = cpl_image_get_size_x( image));
  check( ny = cpl_image_get_size_y( image));

  xsh_msg( "nx %d ny %d", nx, ny);

  cpl_image_fit_gaussian( image, 4, 4, 6, &norm, &xcen, &ycen, &sig_x, &sig_y, NULL, NULL);

  if ( cpl_error_get_code() != CPL_ERROR_NONE){
    xsh_msg("Can not fit gaussian the image");
  }
  else{
    xsh_msg(" find center at %f %f", xcen, ycen);
  }
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      xsh_error_dump(CPL_MSG_ERROR);
      ret  = 1;
    }
    return ret;
}

/**@}*/
