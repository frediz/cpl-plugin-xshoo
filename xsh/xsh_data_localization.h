/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-09 10:04:57 $
 * $Revision: 1.8 $
 */
#ifndef XSH_DATA_LOCALIZATION_H
#define XSH_DATA_LOCALIZATION_H
#include <xsh_cpl_size.h>
#include <cpl.h>
#include <xsh_data_instrument.h>


#define XSH_LOCALIZATION_TABLE_COLNAME_SLICE "SLICE"
#define XSH_LOCALIZATION_TABLE_COLNAME_CENTER "CENCOEF"
#define XSH_LOCALIZATION_TABLE_COLNAME_EDGUP "EDGUPCOEF"
#define XSH_LOCALIZATION_TABLE_COLNAME_EDGLO "EDGLOCOEF"
#define XSH_LOCALIZATION_TABLE_DEGPOL "DEGPOL"

typedef struct{
  /* Slice Identification */
  float slice ;
  /* polynomial fitting the center points of the order */
  cpl_polynomial * cenpoly;
  /* polynomial fitting the upper edge of the order */
  cpl_polynomial * edguppoly;
  /* polynomial fitting the lower edge of the order */ 
  cpl_polynomial * edglopoly; 
  /* Polynomial degree (same for the 3 polynomes) */
  int pol_degree ;
  cpl_propertylist *header;
} xsh_localization ;


xsh_localization* xsh_localization_create(void);

xsh_localization* xsh_localization_load( cpl_frame* frame);

cpl_propertylist* xsh_localization_list_get_header( xsh_localization *list);

cpl_propertylist * xsh_localization_get_header(xsh_localization *list);


void xsh_localization_free( xsh_localization **list);

cpl_frame* xsh_localization_save( xsh_localization *list,
  const char* filename, xsh_instrument *instrument);

#endif  /* XSH_ORDER_H */
