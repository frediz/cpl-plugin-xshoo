/*                                                                           a
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */



/*
 * $Author: amodigli $
 * $Date: 2012-12-11 08:44:13 $
 * $Revision: 1.29 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_UTILS_SCIRED_SLIT_H
#define XSH_UTILS_SCIRED_SLIT_H

#include <cpl.h>
#include <xsh_data_instrument.h>
#include <xsh_utils_scired_slit.h>
#include <xsh_parameters.h>

cpl_error_code
xsh_scired_get_proper_maps(cpl_frameset* raws, cpl_frameset* calib,
                           cpl_frame* order_tab_edges,cpl_frame* master_flat,
                           cpl_frame* model_config_frame,cpl_frame* disp_tab,
                           xsh_instrument* instrument,const int do_computemap,
                           const char* rec_prefix, cpl_frame** wavemap,
                           cpl_frame** slitmap);
cpl_error_code 
xsh_stare_params_bin_scale(cpl_frameset* raws,
                     xsh_background_param* backg,
                     xsh_opt_extract_param *opt_extract_par,
                     int* sub_sky_nbkpts1,
			   int* sub_sky_nbkpts2);

cpl_frameset*
xsh_scired_slit_nod_fast(
			 cpl_frameset *nod_set,
			 cpl_frame* spectral_format,
			 cpl_frame* master_flat,
			 cpl_frame* order_tab_edges,
                         cpl_frame* wave_tab,
                         cpl_frame* model_config_frame,
                         cpl_frame* disp_tab_frame,
                         cpl_frame* wavemap,
			 xsh_instrument* instrument,
                         xsh_remove_crh_single_param *crh_single_par,
                         xsh_rectify_param *rectify_par,
                         const int do_flatfield,
                         const int corr_sky,
                         const int compute_eff,
                         const char* rec_prefix,
                         cpl_frameset **comb_eff_set
   );


cpl_frameset*
xsh_scired_slit_nod_accurate(
			     cpl_frameset *nod_set,
			     cpl_frame* spectral_format,
                             cpl_frame* master_flat,
                             cpl_frame* order_tab_edges,
                             cpl_frame* wave_tab,
                             cpl_frame* model_config_frame,
                             cpl_frame* disp_tab_frame,
                             cpl_frame* wavemap,
                             cpl_frame *skymask_frame,
                             xsh_instrument* instrument,
                             xsh_remove_crh_single_param *crh_single_par,
                             xsh_rectify_param *rectify_par,
                             xsh_localize_obj_param *loc_obj_par,
                             const char *throw_name,
                             const int do_flatfield,
                             const char* rec_prefix
   );

cpl_error_code
xsh_scired_slit_nod_get_calibs(cpl_frameset* raws,
                               cpl_frameset* calib,
                   xsh_instrument* instrument,
                   cpl_frame** bpmap,
                   cpl_frame** master_bias,
                   cpl_frame** master_flat,
                   cpl_frame** order_tab_edges,
                   cpl_frame** wave_tab,
                   cpl_frame** model_config_frame,
                   cpl_frame** wavemap,
                   cpl_frame** slitmap,
                   cpl_frame** disp_tab_frame,
                   cpl_frame** spectral_format,
                               cpl_frame** skymask_frame,
                               cpl_frame** response_ord_frame,
                               cpl_frame** frm_atmext,
                               int do_computemap,
                               int use_skymask,
                               int pscan,
                   const char* rec_prefix,const char* rec_id);

cpl_error_code
xsh_respon_slit_nod_get_calibs(cpl_frameset* calib,
			       xsh_instrument* instrument,
			       cpl_frame** bpmap,
			       cpl_frame** master_bias,
			       cpl_frame** master_flat,
			       cpl_frame** order_tab_edges,
			       cpl_frame** wave_tab,
			       cpl_frame** model_config_frame,
			       cpl_frame** wavemap,
			       cpl_frame** slitmap,
			       cpl_frame** disp_tab_frame,
			       cpl_frame** spectral_format,
                               cpl_frame** skymask_frame,
                               cpl_frame** response_ord_frame,
                               cpl_frame** frm_atmext,
                               int do_computemap,
                               int use_skymask,
                               int pscan,
			       const char* rec_prefix,const char* rec_id);


cpl_error_code
xsh_scired_nod_get_parameters(cpl_parameterlist* parameters,
			      xsh_instrument* instrument,
			      xsh_remove_crh_single_param** crh_single_par,
			      xsh_rectify_param** rectify_par,
			      xsh_extract_param** extract_par,
			      xsh_combine_nod_param** combine_nod_par,
			      xsh_slit_limit_param** slit_limit_par,
			      xsh_localize_obj_param** loc_obj_par,
			      int* rectify_fast,int* pscan,
                              int* generate_sdp_format,
                              const char* rec_id);



cpl_frameset* 
xsh_nod_group_by_reloff( cpl_frameset *ord_set,xsh_instrument *instrument,xsh_stack_param* stack_par);


cpl_frameset*
xsh_nod_group_by_reloff2( cpl_frameset *ord_set,xsh_instrument *instrument,xsh_stack_param* stack_par);

cpl_error_code
xsh_flux_calibrate(cpl_frame* rect2D,
                   cpl_frame* rect1D,
                   cpl_frame* atmext,
                   cpl_frame* response,
                   int mpar,
                   xsh_instrument* inst,
                   const char* rec_prefix,
                   cpl_frame** fcal_rect_2D,
                   cpl_frame** fcal_rect_1D,
                   cpl_frame** fcal_2D,
                   cpl_frame** fcal_1D);

cpl_error_code
xsh_flux_calibrate2D(cpl_frame* rect2D,
                   cpl_frame* atmext,
                   cpl_frame* response,
                   int mpar,
                   xsh_instrument* inst,
                   const char* rec_prefix,
                   cpl_frame** fcal_rect_2D,
		     cpl_frame** fcal_2D);

cpl_error_code
xsh_flux_calibrate1D(cpl_frame* rect1D,
                   cpl_frame* atmext,
                   cpl_frame* response,
                   int mpar,
                   xsh_instrument* inst,
                   const char* rec_prefix,
                   cpl_frame** fcal_rect_1D,
		     cpl_frame** fcal_1D);



cpl_error_code
xsh_ifu_stare_get_calibs(cpl_frameset* calib,
                          xsh_instrument* inst,
                          cpl_frame** spectral_format,
                          cpl_frame** mbias,
                          cpl_frame** mdark,
                          cpl_frame** mflat,
                          cpl_frame** otab_edges,
                          cpl_frame** model_cfg,
                          cpl_frame** bpmap,
                          cpl_frame** wmap,
                          cpl_frame** smap,
                          cpl_frame** ifu_cfg_tab,
                          cpl_frame** ifu_cfg_cor,
                          cpl_frame** wavesol,
                          const char* rec_id,
                          int * recipe_use_model,
                          int pscan);

cpl_error_code
xsh_slit_stare_get_calibs(cpl_frameset* calib, 
                          xsh_instrument* instrument,
                          cpl_frame** spectralformat,
                          cpl_frame** mbias,
                          cpl_frame** mdark,
                          cpl_frame** mflat,
                          cpl_frame** otab_edges,
                          cpl_frame** model_cfg,
                          cpl_frame** wave_tab,
                          cpl_frame** sky_list,
                          cpl_frame** sky_orders_chunks,
                          cpl_frame** qc_sky,
                          cpl_frame** bpmap,
                          cpl_frame** sframe_sky_sub_tab,
                          cpl_frame** wmap,
                          cpl_frame** smap,
			  const char* rec_id,
                          int * recipe_use_model,
                          int  pscan);

cpl_error_code
xsh_slit_offset_get_calibs(cpl_frameset* calib,xsh_instrument* instrument,
                           cpl_frame** bpmap,cpl_frame** mbias,
                           cpl_frame** mdark, cpl_frame** otab_edges,
                           cpl_frame** model_cfg, cpl_frame** wave_tab,
                           cpl_frame** mflat, cpl_frame** wmap, cpl_frame** smap,
                           cpl_frame** spectral_format,const char* rec_id);


cpl_error_code
xsh_slit_stare_get_params(cpl_parameterlist* parameters,
                          const char* rec_id,
                          int* pre_overscan_corr,
                          xsh_background_param** backg_par,
                          xsh_localize_obj_param** loc_obj_par,
                          xsh_rectify_param** rectify_par,
                          xsh_remove_crh_single_param** crh_single_par,
                          int* sub_sky_nbkpts1,
                          int* do_flatfield,
                          int* sub_sky_nbkpts2,
                          xsh_subtract_sky_single_param** sky_par,
                          int* do_optextract,
                          xsh_opt_extract_param** opt_extract_par,
                          int* generate_sdp_format);

cpl_error_code
xsh_slit_offset_get_params(cpl_parameterlist* parameters,
                          const char* rec_id,
                          xsh_localize_obj_param** loc_obj_par,
                          xsh_rectify_param** rectify_par,
                          xsh_remove_crh_single_param** crh_single_par,
                          xsh_extract_param**extract_par,
                          xsh_combine_nod_param** combine_nod_param,
                          int* do_flatfield,
                          int* gen_sky,
                          int* generate_sdp_format);

cpl_error_code
xsh_slit_stare_correct_crh_and_sky(xsh_localize_obj_param * loc_obj_par,
                                   xsh_remove_crh_single_param* crh_single_par,
                                   xsh_rectify_param* rectify_par,
                                   int do_sub_sky,
                                   const char* rec_prefix,
                                   cpl_frame* rmbkg,
                                   cpl_frame* order_tab_edges, 
                                   cpl_frame* slitmap, 
                                   cpl_frame* wavemap,
                                   cpl_frame* sky_map,
                                   cpl_frame* model_config,
                                   cpl_frame* single_frame_sky_sub_tab,
                                   xsh_instrument* instrument,
                                   int sub_sky_nbkpts1, 
                                   xsh_subtract_sky_single_param* sky_par,
                                   cpl_frame* qc_sky_frame,
                                   cpl_frame* sky_orders_chunks,
                                   cpl_frame** sky,
                                   cpl_frame** sky_eso,
                                   cpl_frame** sky_ima,
                                   cpl_frame* wave_tab,
                                   cpl_frame* disp_tab,
                                   cpl_frame* spectral_format,
                                   int nb_raw_frames,
                                   cpl_frame** loc_table,
                                   cpl_frame** clean,
                                   cpl_frame** clean_obj,const int clean_tmp);

cpl_error_code
xsh_slit_stare_get_maps(cpl_frameset* calib,
                        int do_compute_map,int recipe_use_model,
                        const char* rec_prefix,xsh_instrument* instrument,
                        cpl_frame* model_config_frame,cpl_frame* crhm_frame,
                        cpl_frame* disp_tab_frame, cpl_frame* order_tab_edges,
                        cpl_frame** wavemap_frame, cpl_frame** slitmap_frame);

cpl_error_code
xsh_scired_util_spectra_flux_calibrate(cpl_frame* res2D,cpl_frame* res1D,
                                       cpl_frame* response,cpl_frame* atmext,
                                       xsh_instrument* inst,
                                       const char* prefix,
                                       cpl_frame** fluxcal_2D,
                                       cpl_frame** fluxcal_1D);

cpl_frame* xsh_compute_efficiency(cpl_frame* mer1D, cpl_frame* std_cat, 
				       cpl_frame* atm_ext, cpl_frame* high_abs_win, 
				       xsh_instrument* instr);


cpl_error_code
xsh_compute_resampling_accuracy(cpl_frame* wavemap,
				cpl_frame* slitmap,
				cpl_frame* order_tab_edges,
				cpl_frame* model_config,
				cpl_frame* merged2D,
				xsh_instrument* instrument);

cpl_error_code
xsh_compute_wavelength_resampling_accuracy(cpl_frame* wavemap,
        cpl_frame* order_tab_edges,
        cpl_frame* model_config,
        cpl_frame* merged1D,
        xsh_instrument* instrument);
cpl_frameset*
xsh_frameset_crh_single(cpl_frameset* raws,
                        xsh_remove_crh_single_param * crh_single_par,
                        cpl_frame* sky_map_frm,
    xsh_instrument* instrument,const char* prefix,const char* spec);

cpl_frameset*
xsh_frameset_mflat_divide(cpl_frameset* input, cpl_frame* mflat,
    xsh_instrument* instrument);




#endif
