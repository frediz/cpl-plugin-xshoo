/* $Id: xsh_detmon.h,v 1.3 2013-01-25 16:08:41 jtaylor Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002, 2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02111-1307 USA
 */

/*
 * $Author: jtaylor $
 * $Date: 2013-01-25 16:08:41 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_DETMON_H
#define XSH_DETMON_H

/*----------------------------------------------------------------------------
                                   Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_cpl_size.h>

/*----------------------------------------------------------------------------
                                   Prototypes
 ----------------------------------------------------------------------------*/


#define NIR TRUE
#define OPT FALSE


#undef REGEXP
#define REGEXP "ARCFILE|MJD-OBS|ESO TPL ID|DATE-OBS|ESO DET DIT|ESO DET NDIT"

cpl_propertylist *
xsh_detmon_fill_prolist(const char *,
			   const char *,
			   const char *,
			   cpl_boolean);




double                  irplib_pfits_get_exptime(const cpl_propertylist *);

cpl_error_code
xsh_detmon_fill_parlist(cpl_parameterlist *,
                           const char *, const char *, int, ...);

int
xsh_detmon_retrieve_par_int(const char *,
                            const char *,
                            const char *, const cpl_parameterlist *);

double
xsh_detmon_retrieve_par_double(const char *,
                            const char *,
                            const char *, const cpl_parameterlist *);

int xsh_detmon_lg_mr(cpl_frameset            * frameset,
                 const cpl_parameterlist * parlist_,
                 const char * recipe,
                 cpl_boolean opt);

#endif
