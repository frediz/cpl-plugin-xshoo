/*
 * This file is part of the ESO X-shooter Pipeline
 * Copyright (C) 2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_MSG_H
#define XSH_MSG_H

#include <xsh_utils.h>

#include <stdbool.h>

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup xsh_msg
 *
 */
/*----------------------------------------------------------------------------*/
/**@{*/
#define xsh_msg_dbg_high(...) { \
    if (xsh_debug_level_get() >= XSH_DEBUG_LEVEL_HIGH)\
      cpl_msg_debug(__func__, __VA_ARGS__) ; \
    }
#define xsh_msg_dbg_medium(...) { \
    if (xsh_debug_level_get() >= XSH_DEBUG_LEVEL_MEDIUM)\
      cpl_msg_debug(__func__, __VA_ARGS__)  ; \
    }
#define xsh_msg_dbg_low(...) { \
    if (xsh_debug_level_get() >= XSH_DEBUG_LEVEL_LOW)\
      cpl_msg_debug(__func__, __VA_ARGS__) ; \
    }

/*----------------------------------------------------------------------------*/
/**
   @brief    Print an error message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_error(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define xsh_msg_error(...) cpl_msg_error(__func__, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a progress message
   @param    i           See @c cpl_msg_progress()
   @param    iter        See @c cpl_msg_progress()
   @param    ...         Message to print

   This function is used instead of @c cpl_msg_progress(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define xsh_msg_progress(i, iter, ...) cpl_msg_progress(__func__, (i), (iter),\
	       	__VA_ARGS__)
/*----------------------------------------------------------------------------*/
/**
   @brief    Print an warning message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_warning(), and saves
   the user from typing the calling function name.

   Additionally the number of warnings printed so far is 
*/
/*----------------------------------------------------------------------------*/
#define xsh_msg_warning(...) xsh_msg_warning_macro(__func__, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a debug message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_debug(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define xsh_msg_debug(...) cpl_msg_debug(__func__, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print an indented message
   @param    ...             Message to print
*/
/*----------------------------------------------------------------------------*/
#define xsh_msg_indented(...)  do {                \
                           cpl_msg_indent_more();  \
                           xsh_msg(__VA_ARGS__);   \
                           cpl_msg_indent_less();  \
                           } while (false)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a message on info level
   @param    ...             Message to print

   See also @c xsh_msg_macro().
*/
/*----------------------------------------------------------------------------*/
#define xsh_msg(...) cpl_msg_info("", __VA_ARGS__)

void xsh_msg_init(void);

void xsh_msg_warning_macro(const char *fct, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 2, 3)))
#endif
;

int xsh_msg_get_warnings(void);

#endif /* XSH_MSG_H */

/**@}*/
