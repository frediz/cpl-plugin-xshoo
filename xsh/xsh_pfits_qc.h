/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2013-04-26 10:43:32 $
 * $Revision: 1.35 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_PFITS_QC_H
#define XSH_PFITS_QC_H

/*----------------------------------------------------------------------------
 Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_data_instrument.h>
#include <xsh_qc_definition.h>

/*----------------------------------------------------------------------------
 Defines
 ----------------------------------------------------------------------------*/
#define XSH_QC_LAMP_NAME "ESO QC LAMP NAME"
#define XSH_QC_LAMP_INTENSITY "ESO QC LAMP INTENSITY"

#define XSH_QC_VRAD_BARYCOR "ESO QC VRAD BARYCOR"
#define XSH_QC_VRAD_BARYCOR_C "Barycentric radial velocity correction"
#define XSH_QC_VRAD_HELICOR "ESO QC VRAD HELICOR"
#define XSH_QC_VRAD_HELICOR_C "Heliocentric radial velocity correction"

#define XSH_QC_EFF_MED_ORD "ESO QC EFF MED ORD"
#define XSH_QC_EFF_PEAK_ORD "ESO QC EFF PEAK ORD"


#define XSH_QC_TRACE_FIT_C0 "ESO QC TRACE FIT C0"
#define XSH_QC_TRACE_FIT_C1 "ESO QC TRACE FIT C1"
#define XSH_QC_TRACE_FIT_C2 "ESO QC TRACE FIT C2"

#define XSH_QC_AFC_XSHIFT "ESO QC AFC XSHIFT"
#define XSH_QC_AFC_YSHIFT "ESO QC AFC YSHIFT"

#define XSH_QC_AFC_XSHIFT_C "Measured shift in X"
#define XSH_QC_AFC_YSHIFT_C "Measured shift in Y"

#define XSH_QC_TELLCORR_RATAVG "ESO QC TELLCORR RATAVG"
#define XSH_QC_TELLCORR_RATRMS "ESO QC TELLCORR RATRMS"
#define XSH_QC_TELLCORR_OPTEXTID "ESO QC TELLCORR OPTEXTID"

#define XSH_QC_TRACE_FIT_DIFF_C0 "ESO QC TRACE FIT DIFF C0"
#define XSH_QC_TRACE_FIT_DIFF_C1 "ESO QC TRACE FIT DIFF C1"
#define XSH_QC_TRACE_FIT_DIFF_C2 "ESO QC TRACE FIT DIFF C2"

#define XSH_QC_TRACE_FIT_DIFF_POS "ESO QC TRACE FIT DIFF POS"


#define XSH_QC_TRACE12_MIN "ESO QC TRACE12 MIN"
#define XSH_QC_TRACE12_MAX "ESO QC TRACE12 MAX"
#define XSH_QC_TRACE12_AVG "ESO QC TRACE12 AVG"
#define XSH_QC_TRACE12_MED "ESO QC TRACE12 MED"
#define XSH_QC_TRACE12_RMS "ESO QC TRACE12 RMS"

#define XSH_QC_TRACE32_MIN "ESO QC TRACE32 MIN"
#define XSH_QC_TRACE32_MAX "ESO QC TRACE32 MAX"
#define XSH_QC_TRACE32_AVG "ESO QC TRACE32 AVG"
#define XSH_QC_TRACE32_MED "ESO QC TRACE32 MED"
#define XSH_QC_TRACE32_RMS "ESO QC TRACE32 RMS"

#define XSH_QC_TRACE13_MIN "ESO QC TRACE13 MIN"
#define XSH_QC_TRACE13_MAX "ESO QC TRACE13 MAX"
#define XSH_QC_TRACE13_AVG "ESO QC TRACE13 AVG"
#define XSH_QC_TRACE13_MED "ESO QC TRACE13 MED"
#define XSH_QC_TRACE13_RMS "ESO QC TRACE13 RMS"

#define XSH_QC_WMAP_WAVEC "ESO QC WMAP WAVEC"

#define XSH_QC_LINE_DIFMIN_ORD "ESO QC LINE DIFMIN ORD"
#define XSH_QC_LINE_DIFMAX_ORD "ESO QC LINE DIFMAX ORD"

#define XSH_QC_LINE_DIFMIN "ESO QC LINE DIFMIN"
#define XSH_QC_LINE_DIFMIN_C "Min difference between adjiacent lines"
#define XSH_QC_LINE_DIFMAX "ESO QC LINE DIFMAX"
#define XSH_QC_LINE_DIFMAX_C "Max difference between adjiacent lines"
#define XSH_QC_LINE_DIFMED "ESO QC LINE DIFMED"
#define XSH_QC_LINE_DIFMED_C "Median difference between adjiacent lines"
#define XSH_QC_LINE_DIFAVG "ESO QC LINE DIFAVG"
#define XSH_QC_LINE_DIFAVG_C "Mean difference between adjiacent lines"




#define XSH_QC_FLUX "ESO QC FLUX"
#define XSH_QC_FLUX_MIN "ESO QC FLUX MIN"
#define XSH_QC_FLUX_MIN_C "Min of flux"
#define XSH_QC_FLUX_MAX "ESO QC FLUX MAX"
#define XSH_QC_FLUX_MAX_C "Max of flux"
#define XSH_QC_FLUX_SN "ESO QC FLUX SN"


#define XSH_QC_MBIASSLOPE "ESO QC MBIASSLOP"

#define XSH_QC_NHPIX "ESO QC NHPIX"
#define XSH_QC_ORD_EDGE_NDET "ESO QC ORD EDGE NDET"


#define XSH_QC_MASTER_RON "ESO QC RON MASTER"
#define XSH_QC_MASTER_RON_C "Read out noise on master frame"
#define XSH_QC_MASTER_RON_ERR "ESO QC RON ERR MASTER"
#define XSH_QC_MASTER_RON_ERR_C "Read out noise error on master frame"

#define XSH_QC_DIFFRON "ESO QC DIFFRON"
#define XSH_QC_DIFFRON_ERR "ESO QC DIFFRON ERR"

#define XSH_QC_NORMRON "ESO QC NORMRON"
#define XSH_QC_NORMRON_ERR "ESO QC NORMRON ERR"

#define XSH_QC_MASTER_FPN "ESO QC FPN MASTER"
#define XSH_QC_MASTER_FPN_C "Fixed pattern noise on master frame"
#define XSH_QC_MASTER_FPN_ERR "ESO QC FPN ERR MASTER"
#define XSH_QC_MASTER_FPN_ERR_C "Fixed patter noise error on master frame"

#define  XSH_QC_EFF_FCLIP "ESO QC FCLIP"
#define  XSH_QC_EFF_NCLIP "ESO QC NCLIP"

#define XSH_QC_FPN "ESO QC FPN"
#define XSH_QC_FPN_ERR "ESO QC FPN ERR"

#define XSH_QC_NORMFPN "ESO QC NORMFPN"
#define XSH_QC_NORMFPN_ERR "ESO QC NORMFPN ERR"

#define XSH_QC_MDARKSLOPE "ESO QC MDARKSLOP"
#define XSH_QC_CONTAM "ESO QC MDARK CONTAM"
#define XSH_QC_CONTAM_C "Master dark contamination"



#define XSH_QC_AFC_RESX_MED "ESO QC AFC RESX MED"
#define XSH_QC_AFC_RESY_MED "ESO QC AFC RESY MED"

#define XSH_QC_AFC_RESX_AVG "ESO QC AFC RESX AVG"
#define XSH_QC_AFC_RESY_AVG "ESO QC AFC RESY AVG"


#define XSH_QC_BP_MAP_PICKUP_NOISE_PIX "ESO QC BP-MAP PICKUP NOISEPIX"
#define XSH_QC_BP_MAP_NBADPIX "ESO QC BP-MAP NBADPIX"
#define XSH_QC_BP_MAP_LINi_MEAN "ESO QC BP-MAP LINi MEAN"
#define XSH_QC_BP_MAP_LINi_MED "ESO QC BP-MAP LINi MED"
#define XSH_QC_BP_MAP_LINi_RMS "ESO QC BP-MAP LINi RMS"


#define XSH_QC_DARKMED_AVE "ESO QC DARKMED AVE"
#define XSH_QC_DARKMED_STDEV "ESO QC DARKMED STDEV"

#define QC_ORD_ORDERPOS_RESIDMIN "ESO QC ORD ORDERPOS RESIDMIN"
#define QC_ORD_ORDERPOS_RESIDMAX "ESO QC ORD ORDERPOS RESIDMAX"
#define QC_ORD_ORDERPOS_RESIDAVG "ESO QC ORD ORDERPOS RESIDAVG"
#define QC_ORD_ORDERPOS_RESIDRMS "ESO QC ORD ORDERPOS RESIDRMS"


#define QC_ORD_ORDERPOS_RESIDMIN_SEL "ESO QC ORD ORDERPOS RESELMIN"
#define QC_ORD_ORDERPOS_RESIDMAX_SEL "ESO QC ORD ORDERPOS RESELMAX"
#define QC_ORD_ORDERPOS_RESIDAVG_SEL "ESO QC ORD ORDERPOS RESELAVG"
#define QC_ORD_ORDERPOS_RESIDRMS_SEL "ESO QC ORD ORDERPOS RESELRMS"

#define QC_ORD_ORDERPOS_MAX_PRED "ESO QC ORD ORDERPOS MAX PRED"
#define QC_ORD_ORDERPOS_MIN_PRED "ESO QC ORD ORDERPOS MIN PRED"
#define QC_ORD_ORDERPOS_NDET "ESO QC ORD ORDERPOS NDET"
#define QC_ORD_ORDERPOS_NPOSALL "ESO QC ORD ORDERPOS NPOSALL"
#define QC_ORD_ORDERPOS_NPOSSEL "ESO QC ORD ORDERPOS NPOSSEL"
#define QC_ORD_ORDERPOS_NPRED "ESO QC ORD ORDERPOS NPRED"
#define QC_WAVECAL_DIFFYAVG        "ESO QC WAVECAL DIFFYAVG"
#define QC_WAVECAL_DIFFYMED        "ESO QC WAVECAL DIFFYMED"
#define QC_WAVECAL_DIFFYSTD        "ESO QC WAVECAL DIFFYSTD"


#define QC_WAVECAL_FWHMRMS         "ESO QC WAVECAL FWHMRMS"
#define QC_WAVECAL_FWHMAVG         "ESO QC WAVECAL FWHMAVG"
#define QC_WAVECAL_NLININT         "ESO QC NLININT"
#define QC_WAVECAL_NLININT_C       "No of lines to measure INTAVG [ADU]"
#define QC_WAVECAL_INTAVG          "ESO QC INTAVG"
#define QC_WAVECAL_INTAVG_C         "average intensity of line list [ADU]"
#define QC_WAVECAL_CATLINE         "ESO QC WAVECAL CATLINE"
#define QC_WAVECAL_FOUNDLINE       "ESO QC WAVECAL FOUNDLINE"
#define QC_WAVECAL_MATCHLINE       "ESO QC WAVECAL MATCHLINE"
#define QC_WAVECAL_SPACEFIT        "ESO QC WAVECAL SPACEFIT"
#define QC_WAVECAL_WAVEFIT         "ESO QC WAVECAL WAVEFIT"
#define QC_RESOLMED                "ESO QC RESOLMED"
#define QC_RESOLRMS                "ESO QC RESOLRMS"
/*----------------------------------------------------------------------------
 * Macros
 ----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Functions prototypes
 ----------------------------------------------------------------------------*/

double xsh_pfits_get_qc_mbiasavg( const cpl_propertylist * plist);
double xsh_pfits_get_qc_mbiasmed( const cpl_propertylist * plist);
double xsh_pfits_get_qc_mbiasrms( const cpl_propertylist * plist);
int xsh_pfits_get_qc_nhpix( const cpl_propertylist * plist);
double xsh_pfits_get_qc_mbiasslope( const cpl_propertylist * plist);
int xsh_pfits_get_qc_ncrh( const cpl_propertylist * plist);
double xsh_pfits_get_qc_ncrh_mean( const cpl_propertylist * plist);
double xsh_pfits_get_qc_structx( const cpl_propertylist * plist);
double xsh_pfits_get_qc_structy( const cpl_propertylist * plist);
double xsh_pfits_get_qc_ron( const cpl_propertylist * plist);
double xsh_pfits_get_qc_mdarkmed (cpl_propertylist * plist);

void xsh_pfits_set_qc_eff_nclip( cpl_propertylist * plist,int value );
void xsh_pfits_set_qc_eff_fclip( cpl_propertylist * plist,double value );

void xsh_pfits_set_qc_mbiasavg(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_mbiasmed(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_mbiasrms(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_nhpix(cpl_propertylist * plist, int value);
void xsh_pfits_set_qc_noisepix(cpl_propertylist * plist, int value);
void xsh_pfits_set_qc_mbiasslope(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_structx(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_structy(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_fpn(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_fpn_err(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_norm_fpn(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_norm_fpn_err(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_fpn_master (cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_ron_master (cpl_propertylist * plist, double value);

void  xsh_pfits_set_qc_ron( cpl_propertylist * plist, double value);
void  xsh_pfits_set_qc_ron_err( cpl_propertylist * plist, double value);
void  xsh_pfits_set_qc_ron1( cpl_propertylist * plist, double value);
void  xsh_pfits_set_qc_ron2( cpl_propertylist * plist, double value);
void  xsh_pfits_set_qc_ron1_err( cpl_propertylist * plist, double value);
void  xsh_pfits_set_qc_ron2_err( cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_ncrh(cpl_propertylist * plist, int value) ;
void xsh_pfits_set_qc_ncrh_mean(cpl_propertylist * plist, const double value) ;
void xsh_pfits_set_qc_ncrh_tot(cpl_propertylist * plist, const int value) ;
void xsh_pfits_set_qc_crrate(cpl_propertylist * plist, double value) ;
void xsh_pfits_set_qc_mdarkavg(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_mdarkmed(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_mdarkrms(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_mdarkslope(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_contamination(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_bp_map_ntotal(cpl_propertylist * plist, int value);
void xsh_pfits_set_qc_darkmed_ave(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_darkmed_stdev(cpl_propertylist * plist, double value);

void xsh_pfits_set_qc_nlinecat(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_nlinecat_clean(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_nlinefound(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_nlinefound_clean(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_nlinefound_fib4(cpl_propertylist * plist, double value);
void xsh_pfits_set_qc_nlinefound_clean_fib4(cpl_propertylist * plist, double value);

void xsh_pfits_set_qc_ord_orderpos_residmin( cpl_propertylist * plist,
					     double value );
void xsh_pfits_set_qc_ord_orderpos_residmax( cpl_propertylist * plist,
					     double value );
void xsh_pfits_set_qc_ord_orderpos_residavg( cpl_propertylist * plist,
					     double value );
void xsh_pfits_set_qc_ord_orderpos_residrms( cpl_propertylist * plist,
					     double value );
void xsh_pfits_set_qc_ord_orderpos_max_pred( cpl_propertylist * plist,
					     int value );
void xsh_pfits_set_qc_ord_orderpos_min_pred( cpl_propertylist * plist,
					     int value );
void xsh_pfits_set_qc_ord_orderpos_ndet( cpl_propertylist * plist,
					 int value );
void xsh_pfits_set_qc_ord_orderpos_nposall( cpl_propertylist * plist,
					    int value );
void xsh_pfits_set_qc_ord_orderpos_npossel( cpl_propertylist * plist,
					    int value );
void xsh_pfits_set_qc_ord_orderpos_npred( cpl_propertylist * plist,
					  int value );

void xsh_pfits_set_qc( cpl_propertylist *plist, void *value,
		       const char * kw, xsh_instrument * instrument ) ;
void xsh_pfits_set_qc_multi( cpl_propertylist *plist, void *value,
			     const char * kw, xsh_instrument * instrument,
			     int idx ) ;
void
xsh_pfits_set_qc_reg1_structx (cpl_propertylist * plist, double value);
void
xsh_pfits_set_qc_reg1_structy (cpl_propertylist * plist, double value);

void
xsh_pfits_set_qc_reg2_structx (cpl_propertylist * plist, double value);
void
xsh_pfits_set_qc_reg2_structy (cpl_propertylist * plist, double value);

#endif
