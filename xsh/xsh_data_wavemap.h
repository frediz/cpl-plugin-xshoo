/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2009-11-13 11:12:50 $
 * $Revision: 1.19 $
 */
#ifndef XSH_DATA_WAVEMAP_H
#define XSH_DATA_WAVEMAP_H

#include <cpl.h>
#include <xsh_data_instrument.h>

#define XSH_WAVEMAP_TABLE_NB_UVB_ORDERS 11
#define XSH_WAVEMAP_TABLE_NB_VIS_ORDERS 14
#define XSH_WAVEMAP_TABLE_NB_NIR_ORDERS 16

typedef struct {
  double lambda;
  double slit;
  float flux ;			/**< Average flux for this lambda */
  float sigma ;
	int   ix;
	int   iy;
	int qual;
  double fitted ;		/**< Fitted flux */
  double fit_err ;		/**< Error on the fit */
} wavemap_item ;

typedef struct{
  int order ;
  int sky_size;
  int all_size;
  int object_size;
  int max_size;
  double lambda_min, lambda_max ;
  wavemap_item *sky;
  wavemap_item *object;
  wavemap_item *all;
  cpl_polynomial *pol_lambda;
  cpl_polynomial *pol_slit;
  cpl_polynomial *tcheb_pol_lambda;
  double xmin, xmax, ymin, ymax;
} xsh_wavemap;

typedef struct{
    double sky_slit_min;
    double sky_slit_max;
    double obj_slit_min;
    double obj_slit_max;
  int size;
  int degx, degy ;
  xsh_wavemap * list;
  xsh_instrument * instrument; 
  cpl_propertylist * header;
} xsh_wavemap_list;

cpl_error_code
xsh_wavemap_list_object_image_save( xsh_wavemap_list * omap,
                             xsh_instrument * instr,
                             const int iter);

cpl_error_code
xsh_wavemap_list_sky_image_save( xsh_wavemap_list * smap,
                             xsh_instrument * instr,
                             const int abs_ord, const int sid,const int iter);


cpl_error_code  xsh_wavemap_list_save4debug( xsh_wavemap_list * wmap,
                                              xsh_instrument * instr,
                                              const char * prefix);

xsh_wavemap_list * xsh_wavemap_list_create(xsh_instrument* instr);

void xsh_wavemap_list_free(xsh_wavemap_list ** list);

void xsh_wavemap_list_dump( xsh_wavemap_list * list, const char * fname ) ;

void xsh_wavemap_list_set_max_size( xsh_wavemap_list * list, int idx,
				     int ordnum, int ndata ) ;

void xsh_wavemap_list_compute( double * vlambda, double * xpos, double * ypos,
			       int nitems, double * orders,
			       xsh_dispersol_param * dispsol_param,
			       xsh_wavemap_list * wmap ) ;

void xsh_wavemap_list_compute_poly( double * vlambda, double *vslit,
  double * xpos, double * ypos,
                               int nitems, double * orders,
                               xsh_dispersol_param * dispsol_param,
                               xsh_wavemap_list * wmap );

cpl_frame * xsh_wavemap_list_save_poly( xsh_wavemap_list * wmap,
                                   cpl_frame * order_frame,
                                   xsh_pre * pre,
                                   xsh_instrument * instr, const char * fname,
  cpl_frame **dispersol_frame, cpl_frame **slitmap_frame);

cpl_frame * xsh_wavemap_list_save( xsh_wavemap_list * wmap,
				   cpl_frame * order_frame,
				   xsh_pre * pre,
				   xsh_instrument * instr, const char * fname);

cpl_frame * xsh_wavemap_list_save2( xsh_wavemap_list * wmap,
				   xsh_order_list * order_list,
				   xsh_pre * pre,
                                   xsh_instrument * instr, const char * prefix);
#endif  /* XSH_DATA_WAVEMAP_H */
