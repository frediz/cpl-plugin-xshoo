/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2013-02-08 16:48:12 $
 * $Revision: 1.17 $
 *
 */

#ifndef XSH_UTILS_EFFICIENCY_H
#define XSH_UTILS_EFFICIENCY_H
#include <cpl.h>


typedef enum {
  XSH_GD71,
  XSH_Feige110,
  XSH_GD153,
  XSH_LTT3218,
  XSH_LTT7987,
  XSH_BD17,
  XSH_EG274
}  xsh_std_star_id;

typedef struct {
  double wguess ;    /* Reference line wavelength position */
  double range_wmin; /* minimum of wavelength box for line fit */
  double range_wmax; /* maximum of wavelength box for line fit */
  double ipol_wmin_max; /* maximum lower sub-range wavelength value used to fit line slope */
  double ipol_wmax_min; /* minimum upper sub-range wavelength value used to fit line slope */
  double ipol_hbox;     /* half box where polynomial  fit is performed */

} xsh_rv_ref_wave_param ;


double xsh_get_std_star_vel(xsh_std_star_id std_star_id ,XSH_ARM arm);


void 
xsh_load_ref_table(cpl_frameset* frames, 
                   double dRA, 
                   double dDEC, 
                   double EPSILON, 
                   xsh_instrument* instrument, 
                   cpl_table** pptable);



cpl_frame*
xsh_utils_efficiency(
		     cpl_frameset * frames,
		     double dGain,
		     double dEpsilon,
		     double aimprim,
                     xsh_instrument* inst,
                     const char* col_name_atm_wave,
                     const char* col_name_atm_abs,
                     const char* col_name_ref_wave,
                     const char* col_name_ref_flux,
                     const char* col_name_ref_bin,
                     const char* col_name_obj_wave,
                     const char* col_name_obj_flux
   );

cpl_table* 
xsh_utils_efficiency_internal(
			      cpl_table* tbl_obj_spectrum,
			      cpl_table* tbl_atmext,
			      cpl_table* tbl_ref,
			      double exptime,
			      double airmass,
			      double aimprim,
			      double gain,
			      int    biny,
                              double src2ref_wave_sampling,
                              const char* col_name_atm_wave,
                              const char* col_name_atm_abs,
                              const char* col_name_ref_wave,
                              const char* col_name_ref_flux,
                              const char* col_name_ref_bin,
                              const char* col_name_obj_wave,
                              const char* col_name_obj_flux,
                              int* ntot, int* nclip
   );



cpl_frame*
xsh_efficiency_compute(cpl_frame* frm_sci, 
                       cpl_frame* frm_cat,
                       cpl_frame* frm_atmext,
                      cpl_frame* high_abs_win,
                       xsh_instrument* instrument);

cpl_frame*
xsh_catalog_extract_spectrum_frame(cpl_frame* frm_cat,
                                   cpl_frame* frm_sci);


cpl_error_code
xsh_parse_catalog_std_stars(cpl_frame* cat, 
			    double dRA, 
			    double dDEC, 
			    double EPSILON, 
			    cpl_table** pptable,
			    xsh_std_star_id* std_star_id);

cpl_error_code
xsh_rv_ref_wave_init(xsh_std_star_id std_star_id ,XSH_ARM arm,xsh_rv_ref_wave_param* rv_ref_wave);

void
xsh_frame_sci_get_ra_dec_airmass(cpl_frame* frm_sci,double* ra, double* dec, double* airmass);

double
xsh_utils_compute_airm(cpl_frameset* raws);
double
xsh_utils_compute_airm_eff(cpl_frameset* raws);

xsh_rv_ref_wave_param * xsh_rv_ref_wave_param_create(void);
void xsh_rv_ref_wave_param_destroy(xsh_rv_ref_wave_param * rv_ref_wave);
#endif
