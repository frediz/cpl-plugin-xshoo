/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_SPECTRUM_H
#define XSH_DATA_SPECTRUM_H

#include <cpl.h>
#include <xsh_data_instrument.h>

#define XSH_SPECTRUM_DATA_TYPE CPL_TYPE_FLOAT
#define XSH_SPECTRUM_DATA_BPP CPL_BPP_IEEE_FLOAT
#define XSH_SPECTRUM_ERRS_TYPE CPL_TYPE_FLOAT
#define XSH_SPECTRUM_ERRS_BPP CPL_BPP_IEEE_FLOAT
#define XSH_SPECTRUM_QUAL_TYPE CPL_TYPE_INT
#define XSH_SPECTRUM_QUAL_BPP CPL_BPP_32_SIGNED

typedef struct{
  /* size of the order */
  int size;
  /* Minimal spectrum lambda */
  double lambda_min;
  /* Maximal spectrum lambda */
  double lambda_max;
  /* step in lambda */
  double lambda_step;
  /* Minimal slit position */
  double slit_min;
  /* Maximal slit position */
  double slit_max;
  /* step in slit */
  double slit_step;
  /* size in lambda */
  int size_lambda;
  /* size in slit */
  int size_slit;
  /* flux data */
  cpl_propertylist* flux_header;
  cpl_image *flux;
  /* errs */
  cpl_propertylist* errs_header;
  cpl_image *errs;
  /* qual */
  cpl_propertylist* qual_header;
  cpl_image *qual;
}xsh_spectrum;

xsh_spectrum* xsh_spectrum_1D_create( double lambda_min, double lambda_max, 
  double lambda_step);
xsh_spectrum* xsh_spectrum_2D_create( double lambda_min, double lambda_max,
  double lambda_step, double slit_min, double slit_max, double slit_step);

xsh_spectrum* xsh_spectrum_load( cpl_frame* s1d_frame);
xsh_spectrum* xsh_spectrum_load_order( cpl_frame* s1d_frame, 
                                       xsh_instrument* instr,
                                       const int order);

int xsh_spectrum_get_size( xsh_spectrum* s);
int xsh_spectrum_get_size_lambda( xsh_spectrum* s);
int xsh_spectrum_get_size_slit( xsh_spectrum* s);
double xsh_spectrum_get_lambda_min( xsh_spectrum* s);
double xsh_spectrum_get_lambda_max( xsh_spectrum* s);
double xsh_spectrum_get_lambda_step( xsh_spectrum* s);
double* xsh_spectrum_get_flux( xsh_spectrum* s);
double* xsh_spectrum_get_errs( xsh_spectrum* s);
int* xsh_spectrum_get_qual( xsh_spectrum* s);
cpl_frame* xsh_spectrum_save( xsh_spectrum* s, const char* filename,
			      const char* tag);
cpl_frame* xsh_spectrum_save_order( xsh_spectrum* s, const char* filename, 
                                    const char* tag,const int order);

cpl_image* xsh_spectrum_get_flux_ima( xsh_spectrum* s);

cpl_image* xsh_spectrum_get_errs_ima( xsh_spectrum* s);

cpl_image* xsh_spectrum_get_qual_ima( xsh_spectrum* s);

void xsh_spectrum_free( xsh_spectrum** list);

cpl_frame * xsh_phys_spectrum_save( xsh_spectrum* s, const char* filename, 
				   xsh_instrument* instr) ;
xsh_spectrum * xsh_spectrum_duplicate( xsh_spectrum * org ) ;
xsh_spectrum *
xsh_spectrum_extract_range( xsh_spectrum * org, const double wmin,const double wmax );
cpl_error_code
xsh_spectrum_orders_cut_dichroic_uvb(cpl_frame* frame1d,xsh_instrument* instr);
cpl_error_code
xsh_spectrum_cut_dichroic_uvb(cpl_frame* frame1d);
#endif  /* XSH_DATA_SPECTRUM_H */
