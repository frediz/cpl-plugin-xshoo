/* $Id: xsh_drl_check.h,v 1.16 2012-11-06 16:08:26 amodigli Exp $
 *
 * This file is part of the X-shooter Pipeline
 * Copyright (C) 2006 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-11-06 16:08:26 $
 * $Revision: 1.16 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_DRL_CHECK_H
#define XSH_DRL_CHECK_H

/*-----------------------------------------------------------------------------
                                    Includes
 -----------------------------------------------------------------------------*/
#include <cpl.h>
#include <xsh_drl.h>
#include <xsh_error.h>
#include <xsh_dfs.h>
#include <xsh_utils.h>
#include <xsh_data_pre.h>
#include <xsh_data_rec.h>
#include <xsh_data_instrument.h>
#include <xsh_data_order.h>
#include <xsh_data_dispersol.h>
#include <xsh_data_slice_offset.h>
#include <xsh_parameters.h>
#include <xsh_qc_handling.h>
#include <xsh_utils_ifu.h>
/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
                             Typedefs
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                    Prototypes
 -----------------------------------------------------------------------------*/
cpl_frame* 
xsh_check_load_master_bpmap(cpl_frameset* calib,xsh_instrument* inst,
const char* rec_id);


void xsh_check_get_map( cpl_frame *disp_tab_frame, cpl_frame *order_tab_edges,
  cpl_frame *crhm_frame, cpl_frame *model_config_frame, cpl_frameset *calib,
  xsh_instrument *instrument, int do_computemap, int recipe_use_model,
  const char *rec_prefix,
  cpl_frame **wavemap_frame, cpl_frame **slitmap_frame);

cpl_frame* xsh_check_remove_crh_multiple( cpl_frameset* raws,
  const char *ftag, xsh_stack_param* stack_par,
  xsh_clipping_param *crh_clipping, xsh_instrument* instr,
  cpl_imagelist ** lista, cpl_image** listb);

void xsh_check_afc( int check_flag, cpl_frame *model_frame, 
  cpl_frame *sci_frame, cpl_frameset *wave_frameset, 
  cpl_frame *order_tab_frame, cpl_frame *disp_tab_frame, 
  xsh_instrument *instrument);
  
cpl_frame* xsh_check_subtract_bias( cpl_frame *crhm_frame,
				     cpl_frame *master_bias, 
				     xsh_instrument *instrument, 
				     const char* prefix,
				     const int pre_overscan_corr,const int save_tmp);

cpl_frame* xsh_check_subtract_dark( cpl_frame *rmbias_frame,
  cpl_frame *master_dark, xsh_instrument *instrument, const char* prefix);

cpl_frame* xsh_check_divide_flat( int do_flatfield, cpl_frame *clean_frame,
  cpl_frame *master_flat, xsh_instrument *instrument, const char* prefix);

cpl_frame* xsh_check_remove_crh_single( int nb_raws_frame, 
  cpl_frame *subsky_frame,xsh_remove_crh_single_param *crh_single_par, 
  xsh_instrument *instrument, const char* prefix);

cpl_frame* xsh_check_subtract_sky_single( int do_subsky,
                                          cpl_frame *src_frame,
                                          cpl_frame *ordertabedges_frame,
                                          cpl_frame *slitmap_frame,
                                          cpl_frame *wavemap_frame,
                                          cpl_frame *loctab_frame,
                                          cpl_frame *definedbreakpoints_frame,
                                          xsh_instrument *instrument,
                                          int nbkpts,
                                          xsh_subtract_sky_single_param *sky_par,
                                          cpl_frame* ref_sky_list,
                                          cpl_frame* sky_orders_chhunks,
                                          cpl_frame **sky_spectrum,
                                          cpl_frame **sky_spectrum_eso,
                                          cpl_frame **sky_img,
                                          const char *prefix,
                                          const int clean_tmp);


cpl_frame* 
xsh_save_sky_model( cpl_frame* obj_frame, cpl_frame* sub_sky_frame,
		    const char* sky_tag,xsh_instrument* instrument);

#endif
