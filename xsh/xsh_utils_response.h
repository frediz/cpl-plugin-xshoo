/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */
/*
 * $Author: amodigli $
 * $Date: 2013-04-01 06:27:01 $
 * $Revision: 1.6 $
 */
#ifndef XSH_RESPONSE_H
#define XSH_RESPONSE_H
#include <cpl.h>
#include <xsh_data_spectrum.h>
#include <xsh_data_instrument.h>
#include <xsh_utils.h>
cpl_table*
xsh_telluric_model_eval(cpl_frame* frame_m,xsh_spectrum* s,xsh_instrument* instrument,cpl_size* model_idx);

cpl_table*
xsh_table_resample_uniform(cpl_table* tobs,const char* cwobs, const char* cfobs,
			   const double wstp);
cpl_error_code
xsh_correl_spectra(double* flux_s, double* flux_m, const int size,const int hsearch,const double wlogstp,const int norm_xcorr,const double range,const int ext,XSH_GAUSSIAN_FIT* gfit);


#endif
