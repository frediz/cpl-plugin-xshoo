/* $Id: xsh_model_cputime.c,v 1.4 2011-12-02 14:15:28 amodigli Exp $
 *
 *Not sure about the copyright stuff here!
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */
/* for cpu timing uses getrusage() unless TIMES is defined */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
/*---------------------------------------------------------------------------*/
/**
 * @addtogroup xsh_model    xsh_model_cputime
 *
 * Physical model random CPU time computation functions
 * See man-page for details.
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "xsh_model_kernel.h"

/*----------------------------------------------------------------------------*/

#ifdef TIMES
#include <sys/tupes.h>
#include <sys/times.h>
#include <sys/param.h>

#ifndef HZ
#define HZ       60.0
#endif

#else

#include <sys/time.h>
#include <sys/resource.h>

#endif

#include "xsh_model_cputime.h"

#ifdef NO_PROTO
void xsh_report_cpu( fp, str )
FILE *fp;
char *str;
#else
void xsh_report_cpu(FILE *fp, char *str)
#endif
{
      static int call_no = 0;
      double utime, s_time;

      get_cpu_time(&utime, &s_time);
  
      if ( str == NULL )
	{
	  if ( call_no++ == 0 )
                fprintf(fp,"Preprocessing");
	  else
	        fprintf(fp,"Total CPU");
	}
      else
	        fprintf(fp,"%s", str);

      fprintf(fp," time:\t%2.2fu  %2.2fs\t%2.2f (sec)\n", utime, s_time,
	                                                   utime + s_time);

     
}

#ifdef NO_PROTO
void get_cpu_time(usertime, systime)
double *usertime;
double *systime;
#else
void get_cpu_time(double *usertime, double *systime)
#endif
{
#ifdef TIMES
     struct tms time;

     times( &time );

     *usertime = (double)time.tms_utime / (double)HZ;
     *systime  = (double)time.tms_stime / (double)HZ;

#else

     struct rusage usage;

     getrusage( RUSAGE_SELF, &usage );

        *usertime = (double)usage.ru_utime.tv_sec +
                        (double)usage.ru_utime.tv_usec / 1000000.;
        *systime = (double)usage.ru_stime.tv_sec +
                        (double)usage.ru_stime.tv_usec / 1000000.;

#endif
     
}

/**@}*/
