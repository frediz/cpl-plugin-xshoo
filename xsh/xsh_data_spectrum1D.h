/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2011-12-02 14:15:28 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_SPECTRUM1D_H
#define XSH_DATA_SPECTRUM1D_H

#include <cpl.h>
#include <xsh_data_instrument.h>

#define XSH_SPECTRUM1D_DATA_TYPE CPL_TYPE_FLOAT
#define XSH_SPECTRUM1D_DATA_BPP CPL_BPP_IEEE_FLOAT
#define XSH_SPECTRUM1D_ERRS_TYPE CPL_TYPE_FLOAT
#define XSH_SPECTRUM1D_ERRS_BPP CPL_BPP_IEEE_FLOAT
#define XSH_SPECTRUM1D_QUAL_TYPE CPL_TYPE_INT
#define XSH_SPECTRUM1D_QUAL_BPP CPL_BPP_32_SIGNED

typedef struct{
  /* size of the order */
  int size;
  /* Minimal spectrum lambda */
  double lambda_min;
  /* Maximal spectrum lambda */
  double lambda_max;
  /* step in lambda */
  double lambda_step;
  /* flux data */
  cpl_propertylist* flux_header;
  cpl_image *flux;
  /* errs */
  cpl_propertylist* errs_header;
  cpl_image *errs;
  /* qual */
  cpl_propertylist* qual_header;
  cpl_image *qual;
}xsh_spectrum1D;

xsh_spectrum1D* xsh_spectrum1D_create( double lambda_min, double lambda_max, 
  double lambda_step);
xsh_spectrum1D* xsh_spectrum1D_load( cpl_frame* s1d_frame, 
  xsh_instrument* instr);
int xsh_spectrum1D_get_size( xsh_spectrum1D* s);
double xsh_spectrum1D_get_lambda_min( xsh_spectrum1D* s);
double xsh_spectrum1D_get_lambda_max( xsh_spectrum1D* s);
double xsh_spectrum1D_get_lambda_step( xsh_spectrum1D* s);
double* xsh_spectrum1D_get_flux( xsh_spectrum1D* s);
double* xsh_spectrum1D_get_errs( xsh_spectrum1D* s);
int* xsh_spectrum1D_get_qual( xsh_spectrum1D* s);
cpl_frame* xsh_spectrum1D_save( xsh_spectrum1D* s, const char* filename); 
void xsh_spectrum1D_free( xsh_spectrum1D** list);
cpl_error_code
xsh_monitor_spectrum1D_flux(cpl_frame* in_frm,xsh_instrument* instrument);


#endif  /* XSH_DATA_SPECTRUM1D_H */
