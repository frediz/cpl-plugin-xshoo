/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-06-12 12:22:16 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifndef XSH_UTILS_IMAGELIST_H
#define XSH_UTILS_IMAGELIST_H

/*----------------------------------------------------------------------------
                                    Includes
 ----------------------------------------------------------------------------*/

#include <cpl.h>
#include <xsh_error.h>
cpl_image*
xsh_imagelist_collapse_median_create(cpl_imagelist* iml);
cpl_image*
xsh_imagelist_collapse_mean_create(cpl_imagelist* iml);
cpl_error_code
xsh_imagelist_cut_dichroic_uvb(cpl_imagelist* org_data_iml,
                               cpl_imagelist* org_errs_iml,
                               cpl_imagelist* org_qual_iml,
                               cpl_propertylist* phead);
#endif
