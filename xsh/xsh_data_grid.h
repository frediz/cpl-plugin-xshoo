/*                                                                           *
 *   This file is part of the ESO X-shooter Pipeline                         *
 *   Copyright (C) 2006 European Southern Observatory                        *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-10-01 16:15:31 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */
#ifndef XSH_DATA_GRID_H
#define XSH_DATA_GRID_H

#include <cpl.h>
#include <xsh_data_instrument.h>


/* a grid point */
typedef struct {
  /* x coordinates */
  int x;
  /* y coordinates */
  int y;
  /* value */
  double v;
  double errs;
  int qual;

} xsh_grid_point;

typedef struct{
  /* size of the grid */
  int size;
  /* index of the grid */
  int idx;
  /* list */
  xsh_grid_point** list;
}xsh_grid;

xsh_grid* xsh_grid_create(int size);
void xsh_grid_sort( xsh_grid* grid);
void xsh_grid_free(xsh_grid** grid);
void xsh_grid_add(xsh_grid* grid, int x, int y, double v, double errs, int qual);
xsh_grid_point* xsh_grid_point_get(xsh_grid* grid, int i);
int xsh_grid_get_index(xsh_grid* grid);
void xsh_grid_dump(xsh_grid* grid);
cpl_table* xsh_grid2table( xsh_grid* grid);

#endif  /* XSH_DATA_GRID_H */
